<div class="shop-cart" data-shopcart="2">
    <div class="shop-cart-head"><b>Корзина</b></div>
    <div class="empty">
        <div class="shop-cart-empty">Пусто</div>
    </div>
</div>
<!--tpl_separator-->
<h3 class="uk-panel-title">Корзина</h3>
      <table class="bh-offcanvas-shopping-cart uk-table uk-table-middle uk-margin-small-top uk-margin-bottom-remove">
        <tbody>
          <tr>
            <td>
              <a href="shop-singleproduct.html">
                <img src="assets/templates/uikit/img/shop_product-01-1.jpg" width="100" height="75" alt="Product Image">
              </a>
            </td>
            <td>
              <a class="bh-text-uppercase" href="shop-singleproduct.html">Workboots</a>
              <br> $119.90
            </td>
            <td class="uk-text-center"><a href="#"><i class="uk-icon-times"></i></a></td>
          </tr>
          <tr>
            <td>
              <a href="shop-singleproduct.html">
                <img src="assets/templates/uikit/img/shop_product-07-1.jpg" width="100" height="75" alt="Product Image">
              </a>
            </td>
            <td>
              <a class="bh-text-uppercase" href="shop-singleproduct.html">Wayfarer Glasses</a>
              <br> $89.90
            </td>
            <td class="uk-text-center"><a href="#"><i class="uk-icon-times"></i></a></td>
          </tr>
        </tbody>
      </table>
      <a href="shop-checkout.html" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-top">Перейти к корзине</a>
<div class="shop-cart" data-shopcart="2">
    <div class="shop-cart-head"><a name="shopCart"></a><b>Корзина</b></div>
    <div class="full">
        <form action="[[+this_page_url]]#shopCart" method="post">
        <fieldset>
            <div  style="text-align:right;">
                <a href="[[+empty_url]]" id="shk_butEmptyCart">Очистить корзину</a>
            </div>
            <table width="100%">
                <colgroup>
                    <col width="40%" />
                    <col width="25%" />
                    <col width="25%" />
                    <col width="10%" />
                </colgroup>
                <tbody>
                    [[+inner]]
                </tbody>
            </table>
            <div  style="text-align:right;">
                Доставка: [[+delivery_name]] ([[+delivery_price]] [[+currency]])
            </div>
            <div  style="text-align:right;">
                Общая сумма: <b>[[+price_total]]</b> [[+currency]]
            </div>
            <noscript>
                <div><input type="submit" name="shk_recount" value="Пересчитать" /></div>
            </noscript>
            <div class="cart-order">
                <a href="[[+order_page_url]]" id="shk_butOrder">Оформить заказ</a>
            </div>
        </fieldset>
        </form>
    </div>
</div>