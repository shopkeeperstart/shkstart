  <form action="" class="uk-form">
    <div class="filter-block">
      <div class="filter-head">
        <h4>Тип одежды</h4>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Блузки" />
        <label for="">Блузки</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Брюки" />
        <label for="">Брюки</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Джемпер" />
        <label for="">Джемпер</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Жакеты" />
        <label for="">Жакеты</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Жилеты" />
        <label for="">Жилеты</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Кардиганы" />
        <label for="">Кардиганы</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Комбинезон" />
        <label for="">Комбинезон</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Комплект" />
        <label for="">Комплект</label>
      </div>
    </div>
    <div class="filter_block uk-margin-top">
      <div class="filter_head">
        <h4>Статус</h4>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Новинка" />
        <label for="">Новинка</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Нарядное" />
        <label for="">Нарядное</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Распродажа" />
        <label for="">Распродажа</label>
      </div>
      <div>
        <input type="checkbox" id="" name="" value="Хит продаж" />
        <label for="">Хит продаж</label>
      </div>
    </div>
    <button class="uk-button uk-button-large uk-width-1-1 uk-button-success uk-margin-top">Подобрать</button>
  </form>