<h3>Корзина</h3>
            <div id="shop-cart" class="uk-overflow-container">
              <form action="" class="uk-form uk-table-hover uk-table-striped">
                <table class="uk-table">
                  <tbody>
                    <tr>
                      <td class="uk-vertical-align"><img class="uk-vertical-align-middle uk-thumbnail" src="img/catalog/356340_crop.jpg" alt="" width="100"></td>
                      <td class="uk-vertical-align">
                        <div class="uk-vertical-align-middle"><a href="#">467</a>
                          <br><span class="params">Серый</span></div>
                      </td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">54</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><input type="number" class="uk-form-width-mini" name="quantity" value="1"></span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">42$</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><a href="#" class="uk-icon-trash uk-text-center uk-text-large"></a></span></td>
                    </tr>
                    <tr>
                      <td class="uk-vertical-align"><img class="uk-vertical-align-middle uk-thumbnail" src="img/catalog/358140_crop.jpg" alt="" width="100"></td>
                      <td class="uk-vertical-align">
                        <div class="uk-vertical-align-middle"><a href="#">467</a>
                          <br><span class="params">Красный</span></div>
                      </td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">54</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><input type="number" class="uk-form-width-mini" name="quantity" value="1"></span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">42$</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><a href="#" class="uk-icon-trash uk-text-center uk-text-large"></a></span></td>
                    </tr>
                    <tr>
                      <td class="uk-vertical-align"><img class="uk-vertical-align-middle uk-thumbnail" src="img/catalog/358720_crop.jpg" alt="" width="100"></td>
                      <td class="uk-vertical-align">
                        <div class="uk-vertical-align-middle"><a href="#">467</a>
                          <br><span class="params">Белый</span></div>
                      </td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">54</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><input type="number" class="uk-form-width-mini" name="quantity" value="1"></span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle">42$</span></td>
                      <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><a href="#" class="uk-icon-trash uk-text-center uk-text-large"></a></span></td>
                    </tr>
                  </tbody>
                </table>
                <div class="uk-grid uk-margin-bottom">
                  <div class="uk-width-medium-1-1">
                    <span class="uk-float-left">Общая сумма:</span>
                    <span class="uk-float-right">209.80$</span>
                    <br>
                    <span class="uk-float-left">Доставка:</span>
                    <span class="uk-float-right">-</span>
                    <br>
                    <hr>
                    <span class="uk-float-left uk-text-bold">ИТОГО:</span>
                    <span class="uk-float-right">209.80$</span>
                  </div>
                </div>
                <div class="uk-button uk-button-large uk-button-success uk-width-1-1"><i class="uk-icon-paper-plane"></i> &nbsp;Отправить заказ</div>
              </form>
            </div>
          </div>