<p>Consequatur odio amet repudiandae quaerat nemo similique architecto, laudantium obcaecati distinctio.</p>
                <form class="uk-form uk-form-stacked">
                  <div data-uk-margin="{cls: 'uk-margin-top'}">
                    <div class="uk-grid uk-grid-small" uk-text-center">
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Email</label>
                        <input type="text" placeholder="user@gmail.com" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Пароль</label>
                        <input type="password" placeholder="******" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-text-center uk-margin-bottom">
                        <label>
                          <input type="checkbox"> Запомнить меня</label>
                        <a class="uk-margin-left" href="#">Потеряли пароль?</a>
                      </div>
                      <div class="uk-width-1-1 uk-text-center">
                        <button class="uk-button uk-width-1-2 uk-align-center uk-text-center uk-button-large"><i class="uk-icon-sign-in"></i> Войти</button>
                      </div>
                      <div class="uk-width-1-1 uk-text-center uk-margin-top">ИЛИ</div>
                      <div class="uk-width-1-1 uk-margin">
                        <p class="uk-text-center">Авторизироватся через социальные сети</p>
                      </div>
                      <div class="uk-width-1-1 uk-text-center">
                        <a href="#"><i class="uk-icon-vk uk-icon-button"></i></a>
                        <a href="#"><i class="uk-icon-facebook uk-icon-button"></i></a>
                        <a href="#"><i class="uk-icon-twitter uk-icon-button"></i></a>
                        <a href="#"><i class="uk-icon-odnoklassniki uk-icon-button"></i></a>
                      </div>
                    </div>
                  </div>
                </form>