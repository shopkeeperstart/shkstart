<div class="uk-panel uk-panel-box uk-panel-box-secondary uk-margin-bottom">
  <h3 class="uk-panel-title">Фильтры</h3>
    <div id="filters">
    <a onclick="tmFilters.resetFilters(); return false;">Сбросить фильтры</a>
      [[tmFilters?
        &filterOuterTpl=`tm2_filterOuterTpl`
        &filterTpl=`tm2_filterTpl`
        &filterNumericOuterTpl=`tm2_filterOuterTpl`
        &filterNumericTpl=`tm2_filterNumericTpl`
        &jsMap=`1`
        &toPlaceholder=`filters`
      ]]
      <form action="[[~[[*id]]]]" method="get" class="uk-form" >
          <input type="hidden" name="page_id" value="[[*id]]" disabled="disabled" />
          [[+filters]]
      </form>
  </div>
</div>