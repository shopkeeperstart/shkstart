<p>Consequatur odio amet repudiandae quaerat nemo similique architecto, laudantium obcaecati distinctio.</p>
                <form class="uk-form uk-form-stacked">
                  <div data-uk-margin="{cls:'uk-margin-top'}">
                    <div class="uk-grid uk-grid-small" uk-text-center">
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Имя <span class="required uk-text-danger">*</span></label>
                        <input type="text" placeholder="Владимир" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Фамилия <span class="required uk-text-danger">*</span></label>
                        <input type="text" placeholder="Пукин" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Телефон</label>
                        <input type="text" placeholder="+38 (099) 061-74-40" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Email <span class="required uk-text-danger">*</span></label>
                        <input type="text" placeholder="user@gmail.com" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Пароль <span class="required uk-text-danger">*</span></label>
                        <input type="text" placeholder="******" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Адрес</label>
                        <input type="text" placeholder="Украина, 35000, г. Киев, ул. Ленина, 50, кв. 71" class="uk-width-1-1">
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Вариант доставки</label>
                        <select class="uk-width-1-1">
                          <option>Курьер</option>
                          <option>Самовывоз</option>
                        </select>
                      </div>
                      <div class="uk-width-1-1 uk-margin-bottom">
                        <label class="uk-form-label" for="">Вариант оплаты</label>
                        <select class="uk-width-1-1">
                          <option>Наличными</option>
                          <option>Банковской картой (онлайн)</option>
                        </select>
                      </div>
                      <div class="uk-width-1-1 uk-text-center uk-margin-bottom">
                        <label><input type="checkbox" checked> Зарегистрироваться на сайте</label>
                      </div>
                    </div>
                  </div>
                </form>