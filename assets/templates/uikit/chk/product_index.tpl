<div class="shk-item product uk-panel uk-active">
  <div class="uk-thumbnail uk-visible-hover">
    [[+tv.status:contains=`Популярное`:then=`<div class="uk-badge uk-panel-badge">ХИТ</div>`]]
    [[+tv.status:contains=`Новинка`:then=`<div class="uk-badge uk-panel-badge uk-badge-success">НОВОЕ</div>`]]
    [[+tv.status:contains=`Акция`:then=`<div class="uk-badge uk-panel-badge uk-badge-danger">АКЦИЯ</div>`]]
    <a href="[[~[[+id]]? &scheme=`abs`]]"><img class="uk-margin-small-bottom" src="[[pthumb? &input=`assets/resourceimages/[[+id]]/[[+tv.image]]` &options=`w=250&h=350&zc=TC`]]" alt=""></a>
    <div class="name"><strong>Модель:</strong> <a href="#">[[+pagetitle]]</a></div>
    <div class="name"><strong>Бренд:</strong> <a href="[[~[[+id:pdofield=`{"top":2,"field":"id"}`]]]]">[[+id:pdofield=`{"top":2,"field":"longtitle"}`]]</a></div>
    <div class="name"><strong>Размеры:</strong> [[sizes? &par=`[[+tv.size]]`]]</div>
    <div class="old-price">[[+tv.old_price:notempty=`<span>[[!+tv.old_price:num_format:shk_curr_rate]]</span> [[!+shk_currency]]`]]</div>
    <form action="[[~[[*id]]? &scheme=`abs`]]" class="uk-form" method="post">
      <div class="price uk-margin-small-bottom"><span class="shk-price">[[!+tv.price:num_format:shk_curr_rate]]</span> [[!+shk_currency]]</div>
      <input type="hidden" name="shk-id" value="[[+id]]" />
      <input type="hidden" name="shk-name" value="[[+pagetitle]]" />
      <input type="hidden" name="shk-count" value="1" />
      <button type="submit" class="shk-but uk-button uk-width-1-1 uk-button-success uk-margin-small-bottom">В корзину</button>
      <a class="uk-button uk-width-1-1 uk-button-primary uk-margin-small-bottom" href="[[~[[+id]]? &scheme=`abs`]]">Подробнее</a>
      <input type="checkbox" name="compare" id="compare[[+id]]" value="1" onclick="return shkCompare.toCompare([[+id]],[[+parent]],this)" [[+id:in_compare=`checked="checked"`]] />
      <label for="compare[[+id]]">Добавить к сравнению</label>
    </form>
  </div>
</div>