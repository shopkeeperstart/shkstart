<form class="uk-form">
  <div class="uk-grid" data-uk-margin="{cls:'uk-margin-top'}">
    <div class="uk-width-1-1 uk-text-center">
      <h3>Новый пользователь</h3>
      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr
        <br> sed diam nonumy eirmod tempor invidunt.</p>
    </div>
    <div class="uk-width-1-1">
      <input type="text" placeholder="Логин" class="uk-form-large uk-width-1-1">
    </div>
    <div class="uk-width-1-1">
      <input type="text" placeholder="Пароль" class="uk-form-large uk-width-1-1">
    </div>
    <div class="uk-width-1-1 uk-text-center">
      <button class="uk-button uk-text-center uk-button-large">Регистрация</button>
    </div>
  </div>
</form>