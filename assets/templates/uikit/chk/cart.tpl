<div id="offcanvas-cart" class="uk-offcanvas">
  <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
    <div class="uk-panel">
      [[!Shopkeeper3? &cartTpl=`shopCartOffset`]]
    </div>
  </div>
</div>