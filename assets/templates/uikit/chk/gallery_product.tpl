<div class="uk-width-medium-1-3">
  <div data-uk-slideshow="{animation: 'scroll', duration: 700}">
    <div class="uk-grid uk-grid-small" data-uk-grid-margin>
      <div class="uk-width-1-1 uk-margin-bottom">
        <div class="uk-slidenav-position">
          <ul class="uk-slideshow">
          [[getImageList? 
              &tvname=`resourcealbum`
              &tpl=`gallery_big_image`
            ]]
          </ul>
          <a data-uk-slideshow-item="previous" href="#" class="uk-slidenav uk-slidenav-previous"></a>
          <a data-uk-slideshow-item="next" href="#" class="uk-slidenav uk-slidenav-next"></a>
        </div>
      </div>
        [[getImageList? 
          &tvname=`resourcealbum` 
          &tpl=`gallery_small_image` 
        ]]
    </div>
  </div>
</div>