<div class="uk-grid" id="office-auth-form">
  <div class="uk-width-4-10">
    <h3>[[%office_auth_login]]</h3>
      <form method="post" class="uk-form uk-form-stacked" id="office-auth-login">
      <div >
        <label for="office-auth-login-email" class="uk-form-label">[[%office_auth_login_username]]&nbsp;<span class="red">*</span></label>
        <input type="text" name="username" placeholder="user@gmail.com" class="uk-width-1-1" id="office-auth-login-username" value="" />
        <p class="help-block"><small>[[%office_auth_login_username_desc]]</small></p>
        <label for="office-auth-login-password" class="uk-form-label">[[%office_auth_login_password]]</label>
        <input type="password" name="password" placeholder="*****" class="uk-width-1-1" id="office-login-form-password" value="" />
        <p class="help-block"><small>[[%office_auth_login_password_desc]]</small></p>
        <input type="hidden" name="action" value="auth/formLogin" />
        <input type="hidden" name="return" value="" />
        <div class="col-sm-offset-3 col-sm-8">
        <button type="submit" class="uk-margin-top uk-button uk-align-center uk-text-center uk-button-large uk-width-1-1"><i class="uk-icon-sign-in"></i> [[%office_auth_login_btn]]</button>
        </div>
      </div>
    </form>
    <label>[[%office_auth_login_ha]]</label>
    <div>[[+providers]]</div>
    <p class="help-block"><small>[[%office_auth_login_ha_desc]]</small></p>
    [[+error:notempty=`<div class="alert alert-block alert-danger alert-error">[[+error]]</div>`]]
  </div>
  <div class="uk-width-6-10">
    <div class="uk-panel uk-panel-box">
      <h3>[[%office_auth_register]]</h3>
      <form method="post" class="uk-form uk-form-stacked" id="office-auth-register">
        <div class="uk-grid uk-grid-small" uk-text-center">
          <div class="uk-width-1-1 uk-margin-bottom">
            <label for="office-auth-register-email" class="uk-form-label">[[%office_auth_register_email]]&nbsp;<span class="red">*</span></label>
            <input type="email" name="email" placeholder="user@gmail.com" class="uk-width-1-1" id="office-auth-register-email" value="" />
            <p class="help-block"><small>[[%office_auth_register_email_desc]]</small></p>
          </div>
          <div class="uk-width-1-1 uk-margin-bottom">
            <label for="office-auth-register-password" class="uk-form-label">[[%office_auth_register_password]]</label>
            <input type="password" name="password" placeholder="******" class="uk-width-1-1" id="office-register-form-password" value="" />
            <p class="help-block"><small>[[%office_auth_register_password_desc]]</small></p>
          </div>
          <div class="uk-width-1-1 uk-margin-bottom">
            <label for="office-auth-register-username" class="uk-form-label">[[%office_auth_register_username]]</label>
            <input type="text" name="username" placeholder="" class="uk-width-1-1" id="office-register-form-username" value="" />
            <p class="help-block"><small>[[%office_auth_register_username_desc]]</small></p>
          </div>
          <div class="uk-width-1-1 uk-margin-bottom">
            <label for="office-auth-register-fullname" class="uk-form-label">[[%office_auth_register_fullname]]</label>
            <input type="text" name="fullname" placeholder="" class="uk-width-1-1" id="office-register-form-fullname" value="" />
            <p class="help-block"><small>[[%office_auth_register_fullname_desc]]</small></p>
          </div>
          <div class="uk-width-1-1 uk-margin-bottom">
            <input type="hidden" name="action" value="auth/formRegister" />
            <button type="submit" class="uk-button uk-button-large uk-button-danger uk-width-1-1 uk-align-center"><i class="uk-icon-paper-plane-o"></i> &nbsp;[[%office_auth_register_btn]]</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>