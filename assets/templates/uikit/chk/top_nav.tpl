<nav id="main-nav" class="uk-navbar">
  <div class="uk-container uk-container-center">
    [[Wayfinder?
      &startId=`0`
      &level=`1`
      &outerTpl=`@CODE: <ul [[+wf.classes]]>[[+wf.wrapper]]</ul>`
      &outerClass=`uk-navbar-nav uk-hidden-small`
      &hereClass=`uk-active`
      &parentClass=`uk-parent`
      &innerClass=`uk-nav uk-nav-navbar`
      &parentRowTpl=`@CODE: <li [[+wf.classes]] data-uk-dropdown><a href="[[+wf.link]]">[[+wf.linktext]]</a><div class="uk-dropdown uk-dropdown-navbar">[[+wf.wrapper]]</div></li>`
    ]]
    <a href="#offcanvas-menu" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
    <div class="uk-navbar-flip uk-visible-small">
      <div class="uk-navbar-content">
        <form class="uk-search">
          <input class="uk-search-field" type="search" placeholder="Поиск...">
        </form>
      </div>
    </div>
  </div>
</nav>