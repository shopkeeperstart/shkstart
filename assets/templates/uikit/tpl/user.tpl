<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div id="content" class="product-page uk-container uk-container-center">
    <div id="content" class="uk-grid uk-margin-bottom">
      [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
      <div class="uk-width-1-1">
        <h1>[[*pagetitle]]</h1>
        <div class="uk-grid">
          <div class="uk-width-medium-1-4">
            <div class="uk-panel uk-panel-box uk-margin-bottom">
              <h3 class="uk-panel-title">Профиль</h3>
              <ul class="uk-nav uk-nav-side uk-nav-parent-icon">
                [[Wayfinder?
                  &startId=`8`
                  &outerTpl=`@CODE: [[+wf.wrapper]]`
                  &outerClass=``
                  &hereClass=`uk-active`
                ]]
                <li class="uk-nav-divider"></li>
                <li><a href="[[~[[*id]]]]?action=auth/logout">Выйти</a></li>
              </ul>
            </div>
          </div>
          <div class="uk-width-medium-3-4">
            [[*content]]
          </div>
        </div>
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$table_sizes]]
  [[$order-one-click]]
</body>
</html>