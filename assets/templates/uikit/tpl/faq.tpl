<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div id="content" class="product-page uk-container uk-container-center">
    <div id="content" class="uk-grid uk-margin-bottom">
      [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
      <div class="uk-width-1-1">
        <h1>[[*longtitle]]</h1>
        [[*content]]
        <div class="uk-accordion" data-uk-accordion="{showfirst: false}">
          [[!getPage@faq]]
        </div>
      </div>
    </div>
  </div>
  <div class="ask-question uk-block uk-block-muted uk-margin-bottom">
    <div class="uk-container uk-container-center ">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <p class="uk-text-large uk-text-center">Возможно у вас остались ещё какие-то вопросы?</p>
          <p class="uk-text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error fuga fugiat sint, quas eligendi ea dicta deserunt magnam ducimus corporis maxime, maiores commodi beatae animi laboriosam ab! Aliquid vitae, nihil.</p>
          <button class="uk-button uk-button-large uk-button-primary uk-width-medium-1-3 uk-width-small-1-1 uk-align-center" data-uk-toggle="{target:'#ask-question'}"><i class="uk-icon-question-circle-o"></i> &nbsp;Задать вопрос</button>
          [[!AjaxForm?
            &snippet=`FormIt`
            &form=`question_form`
            &hooks=`spam,email`
            &submitVar=`question`
            &emailTpl=`question_email`
            &emailSubject=`Новый вопрос с сайта [[++site_name]]`
            &emailTo=`[[++emailsender]]`
            &emailFrom=`[[++emailsender]]`
            &validate=`fullname:required,email:email:required,message:required`
            &errTpl=`<span class="error">[[+error]]</span>`
            &validationErrorMessage=`В форме содержатся ошибки!`
            &successMessage=`Сообщение успешно отправлено`
          ]]
        </div>
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$table_sizes]]
  [[$order-one-click]]
</body>
</html>