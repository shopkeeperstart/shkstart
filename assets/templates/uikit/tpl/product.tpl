<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div id="content" class="product-page uk-container uk-container-center">
    <div id="content" class="uk-grid uk-margin-bottom">
      [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
      <div class="uk-width-1-1">
        <h1>[[*longtitle:default=`[[*pagetitle]]`]]</h1>
        <article class="uk-article">
          <div class="uk-grid" data-uk-grid-margin>
            [[$gallery_product]]
            <div class="shk-item uk-width-medium-2-3">
              <form action="[[~[[*id]]? &scheme=`abs`]]" method="post">
              <input type="hidden" name="shk-id" value="[[*id]]" />
              <input type="hidden" name="shk-count" value="1" />
              <div class="uk-grid">
                <div class="uk-width-small-1-2 uk-width-1-1">
                  [[*inventory:is=`1`:then=`<div class="uk-badge uk-badge-notification uk-badge-success uk-margin-bottom">В налиичии</div>`:else=`<div class="uk-badge uk-badge-notification uk-badge-danger uk-margin-bottom">Нет в налиичии</div>`]]
                  
                  <div class="sku uk-margin-bottom"><strong>Артикул:</strong> [[*pagetitle]]</div>
                  <div class="sku uk-margin-bottom"><strong>Производитель:</strong> <a href="[[~[[*id:pdofield=`{"top":2,"field":"id"}`]]]]">[[*id:pdofield=`{"top":2,"field":"longtitle"}`]]</a></div>
                  <div class="uk-text-muted uk-h3">
                    <del>[[*old_price:notempty=`[[*old_price:num_format:shk_curr_rate]] [[!+shk_currency]]`]]</del>
                  </div>
                  <div class="uk-h1 uk-margin-bottom">
                    <strong>[[!*price:num_format:shk_curr_rate]] [[!+shk_currency]]</strong>
                  </div>
                  <div>
                    [[!ecThreadRating? &tpl=`ec_еhread_rating`]]
                  </div>
                </div>
                <div class="uk-width-small-1-2 uk-width-1-1 ">
                  <div class="uk-visible-small uk-margin-bottom"></div>
                  <input style="display: none" type="checkbox" name="compare" id="compare[[*id]]" value="1" onclick="return shkCompare.toCompare([[*id]],[[*parent]],this)" [[*id:in_compare=`checked="checked"`]] />
                  <label for="compare[[*id]]" class="[[*id:in_compare=`uk-active uk-text-danger`]] compare-button uk-button uk-button-large uk-align-medium-right uk-align-center uk-margin-small-right" data-uk-tooltip title="Добавить к сравнению"><i class="uk-icon-balance-scale"></i></label>
                  <div id="share-button" class="uk-button-dropdown uk-align-medium-right uk-align-center uk-margin-small-right" data-uk-dropdown="{pos:'bottom-center', mode:'click'}">
                      <div class="uk-button uk-button-large uk-align-medium-right uk-align-center" data-uk-tooltip title="Репост"><i class="uk-icon-share-alt"></i></div>
                      <div class="uk-dropdown-blank  uk-panel uk-panel-box">
                        <div id="social-like-buttons" class="social-likes">
                          <div data-service="facebook" title="Share link on Facebook"></div>
                          <div data-service="vkontakte" title="Share link on Vkontakte"></div>
                          <div data-service="odnoklassniki" title="Share link on Odnoklassniki"></div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="uk-grid uk-form">
                <div class="uk-width-1-3">
                  <label for="" class="uk-text-bold">Размер: </label>
                  [[*size:replace=`[[+id]]==[[*id]]`:replace=`#==`]]
                </div>
                <div class="uk-width-1-3">
                  <label for="" class="uk-text-bold">Рост: </label>
                  [[*height:replace=`[[+id]]==[[*id]]`:replace=`#==`]]
                </div>
                <div class="uk-width-1-3">
                  <label for="" class="uk-text-bold">Цвет: </label>
                  [[*color:replace=`[[+id]]==[[*id]]`:replace=`#==`]]
                </div>
                <div class="uk-align-center uk-margin"><i class="uk-icon-arrows"></i> <a href="#size-table" data-uk-modal>Таблица размеров</a></div>
                <div class="uk-cleanfix uk-margin-bottom"></div>
                <div class="uk-width-1-1" data-uk-margin>
                  <button type="submit"  class="shop-cart-button uk-button uk-button-success uk-button-large uk-width-1-1 uk-margin-bottom uk-text-large">Добавить в корзину</button>
                  <a href="#order-one-click" data-uk-modal id="order-one-click-button" class="uk-button uk-button-large uk-width-1-1 uk-text-large uk-margin-small-top">Заказать в один клик</a>
                </div>
              </div>
              </form>
            </div>
          </div>
        </article>
        <div class="uk-width-1-1 uk-margin-top">
          <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#shop-single-details'}">
            <li class="uk-width-1-2 uk-text-large uk-active"><a href="#">Описание</a></li>
            <li class="uk-width-1-2 uk-text-large"><a href="#">Отзывы ([[!ecThreadRating? &tpl=`ec_еhread_rating_count`]])</a></li>
          </ul>
          <ul id="shop-single-details" class="uk-switcher uk-margin">
            <li>
              [[*content]]
            </li>
            <li>
              <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                  <ul class="uk-comment-list uk-margin-top">
                  [[!ecMessages? &tpl=`ec_messages_row`]]
                  </ul>
                </div>
                <div class="uk-width-1-1">
                  [[!ecForm? &tplForm=`ec_form` &tplSuccess=`ec_form_success`]]
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="uk-block uk-block-muted uk-margin-bottom">
          <div class="uk-container">
            <div id="advantage" class="uk-grid">
              <div class="uk-width-large-1-3 uk-width-1-1">
                <div class="uk-grid">
                  <div class="uk-width-large-3-10 uk-width-medium-2-10 uk-width-small-2-10 uk-width-3-10">
                    <i class="uk-icon-truck"></i>
                  </div>
                  <div class="uk-width-7-10 uk-width-large-7-10 uk-width-medium-8-10 uk-width-small-8-10 uk-margin-bottom">
                    <h3>Доставка</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor pariatur vero, illum vitae magnam dolore optio est numquam. Modi, illum.</p>
                  </div>
                </div>
              </div>
              <div class="uk-width-large-1-3 uk-width-1-1">
                <div class="uk-grid">
                  <div class="uk-width-large-3-10 uk-width-medium-2-10 uk-width-small-2-10 uk-width-3-10">
                    <i class="uk-icon-credit-card"></i>
                  </div>
                  <div class="uk-width-7-10 uk-width-large-7-10 uk-width-medium-8-10 uk-width-small-8-10 uk-margin-bottom">
                    <h3>Оплата</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor pariatur vero, illum vitae magnam dolore optio est numquam. Modi, illum.</p>
                  </div>
                </div>
              </div>
              <div class="uk-width-large-1-3 uk-width-1-1">
                <div class="uk-grid">
                  <div class="uk-width-large-3-10 uk-width-medium-2-10 uk-width-small-2-10 uk-width-3-10">
                    <i class="uk-icon-thumbs-o-up"></i>
                  </div>
                  <div class="uk-width-7-10 uk-width-large-7-10 uk-width-medium-8-10 uk-width-small-8-10 uk-margin-bottom">
                    <h3>Гарантия</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor pariatur vero, illum vitae magnam dolore optio est numquam. Modi, illum.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="uk-width-1-1">
          <div class="uk-panel uk-panel-header">
            <div class="uk-panel-title">Возможно вас также заинтересуют</div>
            <div class="uk-slidenav-position" data-uk-slider>
              <div class="uk-slider-container">
                <ul class="uk-slider uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-small">
                  <li>
                    <div class="uk-panel">
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/356340.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                  <li>
                    <div class="uk-panel">
                      <div class="uk-panel-badge uk-badge uk-badge-danger">Акция</div>
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/356340.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                  <li>
                    <div class="uk-panel">
                      <div class="uk-panel-badge uk-badge">Топ продаж</div>
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/358720.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                  <li>
                    <div class="uk-panel">
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/356340.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                  <li>
                    <div class="uk-panel">
                      <div class="uk-panel-badge uk-badge uk-badge-danger">Акция</div>
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/356340.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                  <li>
                    <div class="uk-panel">
                      <div class="uk-panel-badge uk-badge">Топ продаж</div>
                      <figure class="uk-overlay uk-overlay-hover">
                        <img src="/assets/templates/uikit/img/catalog/358720.jpg" width="720" height="540" alt="Product Image">
                        <img class="uk-overlay-panel uk-overlay-image" src="/assets/templates/uikit/img/catalog/358140.jpg" width="720" height="540" alt="Product Image Overlay">
                        <figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom bh-padding-remove">
                          <div>
                            <a href="shop-cart.html" class="uk-align-center uk-button uk-button-primary uk-button-large uk-width-8-10"><i class="uk-icon-link"></i> &nbsp;Подробнее</a>
                          </div>
                        </figcaption>
                        <a class="uk-position-cover" href="shop-singleproduct.html"></a>
                      </figure>
                    </div>
                  </li>
                </ul>
              </div>
              <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a>
              <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$table_sizes]]
  [[$order_one_click]]
</body>
</html>
[[!getViewed? &tpls=`1`]]
<script>
  $('document').ready(function(){
    $('#order-one-click-button').click(function(){
      SHK.fillCart([[*id]],1);
    });
    $(document).on('submit', '#order-one-click.ajax_form', function() {
      SHK.emptyCart();
    });
  });
</script>