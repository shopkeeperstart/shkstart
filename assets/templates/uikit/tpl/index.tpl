<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div class="uk-container uk-container-center">
    <ul class="uk-tab" data-uk-switcher="{connect:'#products-set'}">
      <li class="uk-text-large uk-text-center uk-width-1-3 uk-active"><a href="#">Новинки</a></li>
      <li class="uk-text-large uk-text-center uk-width-1-3"><a href="#">Акция</a></li>
      <li class="uk-text-large uk-text-center uk-width-1-3"><a href="#">Популярные</a></li>
    </ul>
    <ul id="products-set" class="uk-switcher uk-margin">
      <li class="uk-active">
        <div class="uk-margin" data-uk-slideset="{small: 2, medium: 3, large: 5 , animation: 'scale', duration: 200}">
          <div class="uk-slidenav-position uk-margin">
            <div class="uk-slideset uk-grid uk-grid-small uk-flex-center">
              [[!getProducts?
              &depth=`4`
              &parents=`2`
              &tpl=`product_index`
              &limit=`18`
              &includeTVs=`1`
              &includeTVList=`old_price,price,image,size,status`
              &tvFilters=`{"status:LIKE":"%Новинка%"}`
              ]]
            </div>
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
          </div>
          <ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
        </div>
      </li>
      <li>
        <div class="uk-margin" data-uk-slideset="{small: 2, medium: 3, large: 5 , animation: 'scale', duration: 200}">
          <div class="uk-slidenav-position uk-margin">
            <div class="uk-slideset uk-grid uk-grid-small uk-flex-center">
              [[!getProducts?
              &depth=`4`
              &parents=`2`
              &tpl=`product_index`
              &limit=`18`
              &includeTVs=`1`
              &includeTVList=`old_price,price,image,size,status`
              &tvFilters=`{"status:LIKE":"%Акция%"}`
              ]]
            </div>
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
          </div>
          <ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
        </div>
      </li>
      <li>
        <div class="uk-margin" data-uk-slideset="{small: 2, medium: 3, large: 5 , animation: 'scale', duration: 200}">
          <div class="uk-slidenav-position uk-margin">
            <div class="uk-slideset uk-grid uk-grid-small uk-flex-center">
              [[!getProducts?
              &depth=`4`
              &parents=`2`
              &tpl=`product_index`
              &limit=`18`
              &includeTVs=`1`
              &includeTVList=`old_price,price,image,size,status`
              &tvFilters=`{"status:LIKE":"%Популярное%"}`
              ]]
            </div>
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
          </div>
          <ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
        </div>
      </li>
    </ul>
    <div id="content" class="uk-grid uk-margin-bottom">
      <div class="uk-width-1-1">
        <h1>[[*longtitle:default=`[[*pagetitle]]`]]</h1>
        [[*content]]
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$size_table]]
</body>
</html>