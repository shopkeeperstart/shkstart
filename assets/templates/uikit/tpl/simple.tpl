<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div id="content" class="product-page uk-container uk-container-center">
    <div id="content" class="uk-grid uk-margin-bottom">
      [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
      <div class="uk-width-1-1">
        <h1>[[*pagetitle]]</h1>
        [[*content]]
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$table_sizes]]
  [[$order-one-click]]
</body>
</html>