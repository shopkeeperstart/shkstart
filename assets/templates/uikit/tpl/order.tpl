<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div id="content" class="product-page uk-container uk-container-center">
    <div id="content" class="uk-grid uk-margin-bottom">
      [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
      <div class="uk-width-1-1">
        <h1>Оформление заказа</h1>
        <div class="uk-grid">
          <div class="uk-width-medium-4-6 uk-width-small-1-1">
            [[Shopkeeper3@cart_order]]
          </div>
          <div class="uk-width-medium-2-6 uk-width-small-1-1">
            [[+modx.user.id:userinfo=`username`:isnot=``:then=`[[$order_form_call]]`:else=`[[$welcome_new_user]]`]]
          </div>
        </div>
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$table_sizes]]
  [[$order-one-click]]
</body>
</html>