<!DOCTYPE html>
<html lang="en">
[[$head]]
<body id="top">
<script type="text/javascript" src="/assets/components/tag_manager2/js/web/view_switch.js"></script>
  <div id="page-preloader"><span class="spinner uk-icon-cog uk-icon-spin"></span></div>
  [[$toolbar]]
  [[$cart]]
  [[$header]]
  [[$top_nav]]
  [[$offcanvas_menu]]
  [[*id:is=`1`:then=`[[$main_slider]]`]]
  <div class="uk-container uk-container-center">
    <div class="uk-grid">
      <div class="uk-width-1-1">
        <button class="uk-button uk-button-large uk-width-1-1 uk-visible-small uk-margin-top uk-button-primary" data-uk-toggle="{target:'.show-hide', cls: 'uk-hidden-small', animation:'uk-animation-slide-left, uk-animation-slide-bottom'}">Показать/скрыть фильтры</button>
      </div>
      [[$menu_category]]
      <div class="uk-width-large-8-10 uk-width-medium-7-10 uk-width-small-1-1 uk-margin-bottom">
        [[*id:isnot=`1`:then=`[[$breadcrumbs]]`]]
        <h1>[[*longtitle:default=`[[*pagetitle]]`]]</h1>
        [[$sort]]
        <div id="products" class="uk-grid uk-margin-bottom">
          [[!tmCatalog]]
          <div class="clearfix"></div>
        </div>
        <ul class="uk-pagination" id="pages">
          [[!+page.nav]]
        </ul>
        [[*content]]
      </div>
    </div>
  </div>
  [[$footer]]
  [[$modal_login]]
  [[$size_table]]
</body>
</html>