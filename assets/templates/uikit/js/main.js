/**
 * Preloader
 */

$(window).on('load', function () {
  var $preloader = $('#page-preloader');
  $spinner = $preloader.find('.spinner');
  $spinner.fadeOut();
  $preloader.delay(350).fadeOut('slow');
});

/** 
 * Compare
 */

// Added active-class

$(document).ready(function(){
  $('.compare-button').click(function() {
    if($(this).hasClass("uk-active")){
      $(this).removeClass('uk-active uk-text-danger');
    }
    else{
      $(this).addClass('uk-active uk-text-danger');
    }
  });
});


/**
 * Alerts
 */

var cmpOnToCompareLinkMinimum = function () {
  siteLib.alert('В избранном пусто или только один товар.', 'danger');
};

var cmpOnToCompareAdded = function () {
  siteLib.alert('Товар добавлен в избранное.');
};

var cmpOnToCompareRemoved = function () {
  siteLib.alert('Товар убран из избранного.', 'danger');
};

var siteLib = (function ($) {

  this.init = function () {
    jQuery('body');
  };

  this.alert = function (msg, type, time) {

    type = type || 'success';
    time = time || 3000;
    var alertClass = 'uk-alert-' + type;
    $('.alert-fixed').remove();

    $('<div/>', {
      'class': 'uk-alert alert-fixed ' + alertClass,
      'html': '<p>' + msg + '</p>',
      on: {
        mouseover: function () {
          clearTimeout(window.alertTimer);
        }
      }
    })
      .css({
        position: 'fixed',
        zIndex: 999,
        top: 20,
        right: 20,
        opacity: 0.9
      })
      .prepend($('<a/>', {
          'class': 'uk-alert-close uk-close',
          on: {
            click: function (e) {
              e.preventDefault();
              clearTimeout(window.alertTimer);
              $(this).closest('.uk-alert').remove();
            }
          }
        }
      ))
      .appendTo('body');

    clearTimeout(window.alertTimer);
    window.alertTimer = setTimeout(function () {
      $('.alert-fixed').remove();
    }, time);

  };

  return this;

}).call({}, jQuery);

jQuery(document).bind('ready', function () {
  siteLib.init();
});