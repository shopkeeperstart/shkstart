<tr>
    <td>
        [[+id]]
    </td>
    <td>
        [[+date]]
    </td>
    <td>
        [[+price:num_format]] [[+currency]]
    </td>
    <td style="border-left:4px solid [[+status_color]];">
        <div class="badge">
            [[+status_name]]
        </div>
    </td>
    <td>
        <a href="[[~[[*id]]]]?ord_id=[[+id]]">
            Подробнее
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </td>
</tr>