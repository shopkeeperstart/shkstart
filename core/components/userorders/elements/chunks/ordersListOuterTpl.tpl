
<!--noindex-->

<table class="table table-bordered table-hover">
    <thead>
        <tr class="active">
            <th>ID</th>
            <th>Дата</th>
            <th>Цена</th>
            <th>Статус</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        [[+inner]]
    </tbody>
</table>

<ul class="pages">
[[+page.nav]]
</ul>

<!--/noindex-->
