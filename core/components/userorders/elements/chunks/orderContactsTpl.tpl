<tr>
    <td>[[+label]]</td>
    <td>
        [[+action:is=`edit`:then=`
        <input type="text" class="form-control input-sm" name="[[+name]]" value="[[+value]]" />
        `:else=`
        [[+value:default=`&mdash;`]]
        `]]
    </td>
</tr>