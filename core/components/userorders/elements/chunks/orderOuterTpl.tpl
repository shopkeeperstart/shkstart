
<!--noindex-->

<div class="pull-right text-right">
    <p>
        Статус:
        <span class="badge">[[+status_name]]</span>
    </p>
    <form action="[[~[[*id]]]]?ord_id=[[+order_id]]" method="post" onsubmit="return confirm('Вы уверены?');">
        <input type="hidden" name="ord_id" value="[[+order_id]]" />
        <input type="hidden" name="action" value="cancel" />
        [[+status:ne=`[[+statusCanceled]]`:then=`
        <button type="submit" class="btn btn-xs btn-danger">
            <span class="glyphicon glyphicon-remove"></span>
            Отменить заказ
        </button>
        `:else=``]]
    </form>
</div>

<p>Номер заказа: <b>[[+order_id]]</b></p>

<p>Дата: <b>[[+date]]</b></p>

<div class="panel panel-default">
    <div class="panel-heading">
        Состав заказа
    </div>
    <table class="table table-bordered table-hover">
        <thead>
            <tr class="active">
                <th>Наименование</th>
                <th>Параметры</th>
                <th>Кол-во, шт.</th>
                <th>Цена, [[+currency]]</th>
            </tr>
        </thead>
        <tbody>
            [[+purchases]]
        </tbody>
        <tfoot>
            <tr class="cart-order">
                <td colspan="3" style="text-align: right;">
                    <b>Итого:</b>
                </td>
                <td>
                    <b>[[+price:num_format]]</b>
                </td>
            </tr>
        </tfoot>
    </table>
</div>


<a name="contacts"></a>
<form action="[[~[[*id]]]]?ord_id=[[+order_id]]" method="post" >
    <input type="hidden" name="ord_id" value="[[+order_id]]" />
    <input type="hidden" name="action" value="update" />
    
    <div class="panel panel-default">
        <div class="panel-heading">
            Контактные данные
        </div>
        <table class="table table-bordered table-hover">
            <colgroup>
                <col width="50%" span="2">
            </colgroup>
            <tbody>
                [[+contacts]]
            </tbody>
        </table>
    </div>
    [[+action:is=`edit`:then=`
    <div class="pull-right">
        <button type="submit" class="btn btn-primary rounded">
            <span class="glyphicon glyphicon-ok"></span>
            Сохранить
        </button>
        <button type="button" class="btn btn-default rounded" onclick="window.location.href='[[~[[*id]]]]?ord_id=[[+order_id]]'">Отмена</button>
    </div>
    `:else=``]]
    
    <nav>
        <ul class="pager no-margin-top">
            <li class="previous">
                <a href="[[~[[*id]]]]">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    Назад
                </a>
            </li>
            [[+action:ne=`edit`:then=`
            <li class="next">
                <a href="[[~[[*id]]]]?ord_id=[[+order_id]]&amp;action=edit#contacts">
                    <span class="glyphicon glyphicon-pencil"></span>
                    Редактировать контактные данные
                </a>
            </li>
            `:else=``]]
        </ul>
    </nav>
    
</form>

<!--/noindex-->
