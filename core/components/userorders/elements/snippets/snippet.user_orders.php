<?php

/*

Выводит заказы пользоватля

[[!userOrders?
&usergroup=`Покупатели`
&statusCanceled=`5`
]]

---------------------------------

[[!getPage?
&elementClass=`modSnippet`
&element=`userOrders`
&usergroup=`Покупатели`
&limit=`10`
&statusCanceled=`5`
]]
<ul class="pages">
[[!+page.nav]]
</ul>

*/

//error_reporting(E_ALL);
//ini_set('display_errors',1);
//@date_default_timezone_set('Europe/Moscow');
@setlocale (LC_ALL, 'ru_RU.UTF-8');

$output = '';

$on_request = $modx->getOption('on_request',$scriptProperties,'');
if($on_request && !isset($_GET[$on_request])) return '';
$totalVar = $modx->getOption('totalVar', $scriptProperties, 'total');
$offset = $modx->getOption('offset', $scriptProperties, 0);
$limit = $modx->getOption('limit', $scriptProperties, 15);

$properties = array();
$properties['statusCanceled'] = $modx->getOption('statusCanceled', $scriptProperties, 5);
$properties['usergroup'] = $modx->getOption('usergroup',$scriptProperties,'Покупатели');
$properties['noResults'] = $modx->getOption('noResults',$scriptProperties,'Вы пока ничего у нас не купили.');
$properties['ordersListRowTpl'] = $modx->getOption('ordersListRowTpl',$scriptProperties,'ordersListRowTpl');
$properties['ordersListOuterTpl'] = $modx->getOption('ordersListOuterTpl',$scriptProperties,'ordersListOuterTpl');
$properties['crumbTpl'] = $modx->getOption('crumbTpl',$scriptProperties,'ordersCrumbTpl');
$properties['orderOuterTpl'] = $modx->getOption( 'orderOuterTpl', $scriptProperties, 'orderOuterTpl' );
$properties['orderContactsTpl'] = $modx->getOption( 'orderContactsTpl', $scriptProperties, 'orderContactsTpl' );
$properties['orderPurchaseRowTpl'] = $modx->getOption( 'orderPurchaseRowTpl', $scriptProperties, 'orderPurchaseRowTpl' );

$order_id = !empty( $_REQUEST['ord_id'] ) && is_numeric( $_REQUEST['ord_id'] ) ? $_REQUEST['ord_id'] : 0;
$action = !empty( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';

$user = $modx->user;
$user_id = $modx->user->get('id');
$profile = $user->getOne('Profile');

if(!$profile || !$user->isMember($usergroup)) return $output;

require_once MODX_CORE_PATH.'components/shopkeeper3/model/shopkeeper.class.php';
require_once MODX_CORE_PATH.'components/userorders/model/userorders.class.php';

$shk_conf = Shopkeeper::getConfig( 'statuses' );
$properties['statuses'] = array();
if( !empty( $shk_conf['statuses'] ) ){
    foreach( $shk_conf['statuses'] as $status ){
        $properties['statuses'][ $status['id'] ] = $status;
    }
}

$userOrders = new userUrders( $modx, $properties );

$total = $modx->getCount( 'shk_order', array( 'userid' => $user_id ) );
$modx->setPlaceholder( $totalVar, $total );
$modx->setPlaceholders( array( 'order_id' => $order_id, 'action' => $action ) );

//Order page
if( $order_id ){
    
    $output .= $userOrders->printOrder( $order_id );
    
}
//List orders
else{
    
    $output .= $userOrders->printList( $limit, $offset );
    
}

return $output;
