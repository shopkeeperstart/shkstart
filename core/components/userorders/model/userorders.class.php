<?php

/**
 * userOrders
 *
 * @version 1.0
 * @author Andchir <andchir@gmail.com>
 *
 */

class userUrders extends Shopkeeper {
    
    public $modx = null;
    public $config = array();
    public $user_id = 0;
    
    function __construct(modX &$modx, $config = array()){
        
        $this->user_id = $modx->user->get('id');
        
        parent::__construct($modx, $config);
        
    }
    
    /**
     * Применяет действие запроса, если есть соотв. метод
     *
     * @param string $action
     */
    public function applyRequest( $action = '' ){
        
        if( !$action ) {
            $action = isset($_POST['action']) ? $_POST['action'] : ( isset($_GET['action']) ? $_GET['action'] : '' );
        }
        
        if( $action ){
            
            if( method_exists( $this, 'request_' . $action ) ){
                
                call_user_func( array( $this, 'request_' . $action ) );
                
            }
            
        }
        
    }
    
    
    /**
     * Выводит список заказов
     *
     * @param integer $limit
     * @param integer $offset
     */
    public function printList( $limit, $offset ){
        
        $output = '';
        $inner_output = '';
        
        //Get orders
        $orders = array();
        $response = $this->modx->runProcessor('getorderslist',
            array(
                'count' => $limit,
                'offset' => $offset,
                'filters' => array( 'userid' => $this->user_id ),
                'date_format' => 'H:i:s d/m/Y'
            ),
            array('processors_path' => MODX_CORE_PATH . 'components/shopkeeper3/processors/mgr/')
        );
        if( !$response->isError() && $result = $response->getResponse()){
            
            $orders = $result['object'];
            
        }
        
        //Output HTML
        if( !empty( $orders ) ){
            
            foreach( $orders as $order ){
                
                $order['status_name'] = !empty( $this->config['statuses'][ $order['status'] ] ) ? $this->config['statuses'][ $order['status'] ]['label'] : '';
                $order['status_color'] = !empty( $this->config['statuses'][ $order['status'] ] ) ? $this->config['statuses'][ $order['status'] ]['color'] : '';
                
                $inner_output .= $this->modx->getChunk( $this->config['ordersListRowTpl'], $order );
                
            }
            
            $output .= $this->modx->getChunk( $this->config['ordersListOuterTpl'], array( 'inner' => $inner_output ) );
            
        }else{
            
            $output .= $this->config['noResults'];
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Выводит подробности заказа
     *
     * @param integer $order_id
     */
    public function printOrder( $order_id ){
        
        $output = '';
        
        if( $this->config['crumbTpl'] ){
            $this->modx->setPlaceholder( 'addcrumb', $this->modx->getChunk( $this->config['crumbTpl'], array( 'order_id' => $order_id ) ) );
        }
        
        $orderCnt = $this->modx->getCount( 'shk_order', array( 'id' => $order_id, 'userid' => $this->user_id ) );
        if( !$orderCnt ){ return $output; }
        
        //Get order
        $order_data = array();
        $response = $this->modx->runProcessor('getorder',
            array(
                'order_id' => $order_id,
                'date_format' => 'H:i:s d/m/Y'
            ),
            array('processors_path' => MODX_CORE_PATH . 'components/shopkeeper3/processors/mgr/')
        );
        if( !$response->isError() && $result = $response->getResponse()){
            $order_data = $result['object'];
        }
        
        //Render output
        if( !empty( $order_data ) ){
            
            $order_data['statusCanceled'] = $this->config['statusCanceled'];
            $response = $this->modx->runProcessor('renderorderdata',
                array(
                    'order_data' => $order_data,
                    'statuses' => $this->config['statuses'],
                    'orderOuterTpl' => $this->config['orderOuterTpl'],
                    'orderContactsTpl' => $this->config['orderContactsTpl'],
                    'orderPurchaseRowTpl' => $this->config['orderPurchaseRowTpl']
                ),
                array('processors_path' => MODX_CORE_PATH . 'components/shopkeeper3/processors/web/')
            );
            
            if( !$response->isError() && $result = $response->getResponse()){
                
                $output .= $result['object'];
                
            }
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Отмена заказа
     * 
     */
    public function request_cancel(){
        
        $order_id = !empty( $_REQUEST['ord_id'] ) && is_numeric( $_REQUEST['ord_id'] ) ? $_REQUEST['ord_id'] : 0;
        
        $order = $this->modx->getObject( 'shk_order', array( 'id' => $order_id, 'userid' => $this->user_id ) );
        
        if( $order ){
            $order->set( 'status', $this->config['statusCanceled'] );
            $order->save();
        }
        
        $this->modx->sendRedirect( $this->backUrl() );
        exit;
        
    }
    
    
    /**
     * Обновление контактных данных заказа
     *
     */
    public function request_update(){
        
        $order_id = !empty( $_REQUEST['ord_id'] ) && is_numeric( $_REQUEST['ord_id'] ) ? $_REQUEST['ord_id'] : 0;
        $order = $this->modx->getObject( 'shk_order', array( 'id' => $order_id, 'userid' => $this->user_id ) );
        
        if( $order ){
            
            $contacts = $order->contacts ? json_decode( $order->contacts, true ) : array();
            if( !empty( $contacts ) ){
                
                foreach( $contacts as &$contact ){
                    if( isset( $_POST[$contact['name']] ) ){
                        $contact['value'] = htmlspecialchars(trim($_POST[$contact['name']]));
                    }
                }
                
                $contacts = json_encode($contacts);
                $order->set( 'contacts', $contacts );
                $order->save();
                
            }
            
        }
        
        $back_url = $this->modx->makeURL($this->modx->resource->get('id'),'','','abs') . '&ord_id=' . $order_id;
        $this->modx->sendRedirect( $back_url );
        exit;
        
    }
    
}
