<?php
/**
 * Loads the home page.
 *
 * @package catalogfill
 * @subpackage controllers
 */

$modx->regClientStartupScript($catalogfill->config['jsUrl'].'mgr/widgets/home.panel.js');
$modx->regClientStartupScript($catalogfill->config['jsUrl'].'mgr/sections/index.js');

return $output;
