<?php
/**
 * Loads the header for mgr pages.
 *
 * @package catalogfill
 * @subpackage controllers
 */

$modx->regClientStartupScript($catalogfill->config['assetsUrl'].'js/mgr/catalogfill_mgr.js');
$modx->regClientCSS($catalogfill->config['assetsUrl'].'css/mgr.css');
$modx->regClientStartupHTMLBlock('<script type="text/javascript">
catalogfill.config = '.$modx->toJSON($catalogfill->config).';
catalogfill.config.connector_url = "'.$catalogfill->config['connectorUrl'].'";
catalogfill.request = '.$modx->toJSON($_GET).';
</script>');

/*echo '<pre>';
print_r($modx->config);
echo '</pre>';*/

return '';