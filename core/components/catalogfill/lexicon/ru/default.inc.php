<?php
/**
 * Catalogfill Russian language file
 *
 * @package catalogfill
 * @subpackage lexicon
 */

$_lang = array();

$_lang['catalogfill'] = 'Импорт / экспорт';
$_lang['catalogfill.menu_desc'] = 'Импорт и экспорт товаров';

$_lang['catalogfill.import'] = 'Импорт';
$_lang['catalogfill.export'] = 'Экспорт';

$_lang['catalogfill.parent_resource_help'] = 'Выберите родительский ресурс в дереве ресурсов';
$_lang['catalogfill.config'] = 'Конфигурация';
$_lang['catalogfill.upload_files'] = 'Загрузить файлы';
$_lang['catalogfill.imp_file'] = 'Файл для импорта';
$_lang['catalogfill.imp_type'] = 'Тип импорта';
$_lang['catalogfill.imp_type_refresh'] = 'Обновить';
$_lang['catalogfill.imp_type_add'] = 'Добавить';
$_lang['catalogfill.button_import'] = 'Импортировать';
$_lang['catalogfill.button_clean'] = 'Очистить категорию';
$_lang['catalogfill.button_export'] = 'Экспортировать';
$_lang['catalogfill.file_type'] = 'Тип файла';
$_lang['catalogfill.message'] = 'Сообщение';
$_lang['catalogfill.confirm'] = 'Подтверждение';
$_lang['catalogfill.confirm_clean'] = 'Вы уверены, что хотите очистить категорию?';
$_lang['catalogfill.confirm_delete_files'] = 'Вы уверены, что хотите удалить все файлы?';
$_lang['catalogfill.delete_files'] = 'Удалить все файлы';
$_lang['catalogfill.refreshfiles'] = 'Обновить список файлов';
$_lang['catalogfill.editconfig'] = 'Редактировать конфигурационный файл';
$_lang['catalogfill.stat_resources'] = 'Всего ресурсов';
$_lang['catalogfill.stat_tvs'] = 'Всего значений доп.полей';
$_lang['catalogfill.string_number'] = 'Номер строки';

$_lang['catalogfill.mess_no_conf'] = 'Выбирете конфигурацию.';
$_lang['catalogfill.mess_no_parent'] = 'Выбирете родительский ресурс.';
$_lang['catalogfill.mess_no_file'] = 'Выбирете файл для импорта.';
$_lang['catalogfill.mess_file_not_support'] = 'Данный тип файла не поддерживается.';


