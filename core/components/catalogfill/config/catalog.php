<?php

//разбивка по столбцам при импорте и экспорте (content|tv|category)
$cf_config['content_row'] = array(
    array('Заголовок',array('pagetitle','content')),
    array('Расширенный заголовок',array('longtitle','content')),
    array('Анотация',array('introtext','content')),
    array('Цена',array(1,'tv')),
    array('Старая цена',array(5,'tv')),
    array('Изображение',array(2,'tv')),
    array('Галерея',array(8,'tv')),
    array('Тип одежды',array(11,'tv')),
    array('Сезон',array(12,'tv')),
    array('Статус',array(9,'tv')),
    array('Цвет',array(4,'tv')),
    array('Размер',array(3,'tv')),
    array('Рост',array(6,'tv')),
    array('Дата создания', filter_export( array('publishedon','content') ) ),
    array('Ключевые слова для SEO',array(13,'tv')),
    array('Описание для SEO',array(14,'tv'))
);

//значения по умолчанию при импорте и проверка на соответствие при экспорте
$cf_config['imp_content_default'] = array(
    'content' => array(
        'deleted' => 0,
        'published' => 1,
        'template' => 1
        //'createdon' => strtotime("now")
        //'publishedon' => strtotime("now")
        //'pub_date' => strtotime("now")
        //'editedby' => 1
        //'editedon' => strtotime("now")
    ),
    'tv' => array(
        //7 => 0
    )
);

//первая строка - названия полей
$cf_config['include_captions'] = true;

//число товаров импортируемых за один раз (загрузка по группам). 0 - не ограничивать.
$cf_config['batch_import'] = 300;

//разбивать по категориям
$cf_config['include_categories'] = true; // изначально false

//удалять дочерние категории при очистке и обновлении каталога
$cf_config['delete_subcategories'] = true; //изначально true

//по какому полю проверять соответствие товара при обновлении. false - не проверять (очистка категории при обновлении).
$cf_config['imp_chk_field'] = 'pagetitle';

//проверять соответствие товара при обновлении по значению TV. Указать ID TV. false - не проверять (очистка категории при обновлении).
$cf_config['imp_chk_tvid_val'] = false;

//Добавлять товары, которые не найдены при обновлении по TV (imp_chk_tvid_val) или полю (imp_chk_field)
$cf_config['imp_if_not_exist'] = true;

//удалять HTML-теги при экспорте
$cf_config['exp_strip_tags'] = false;

//автоматически генерировать псевдоним (alias) при импорте
//false - выключено; true - генерировать с переводом в транслит; 'notranslit' - генерировать без перевода в транслит.
$cf_config['imp_autoalias'] = true;

//Изменить значения поля для всех вложенных товаров до начала импорта.
//Например можно отменить публикацию для всех товаров и публиковать только те, которые есть в новом прайс-листе.
//первый массив - какие поля и на какие значения менять, второй массив - условия которые нужно проверять (можно сделать пустым)
$cf_config['imp_before_change'] = false;//'[{"tv.inventory":0},{}]';//'[{"published":0},{"tv.pricename":"Поставщик1"}]';//false - для отмены

//удалить файл после экспорта (скачивания)
$cf_config['exp_delete_file'] = true;

//кодировка CSV-файла при экспорте
$cf_config['exp_csv_charset'] = 'windows-1251'; //'windows-1251'

//Импортировать (обновлять) пустые значения
$cf_config['imp_empty'] = true;

//Имя файла процессора, который использовать для импорта. Если пусто, используется стандартный процессор "import".
$cf_config['imp_custom_processor'] = '';

//путь (xpath) в XML структуре до товаров
$cf_config['imp_xml_itemsparent_path'] = '';//'/catalog/shop/offers';

//Структура XML файла для импорта
$cf_config['imp_xml_structure'] = <<<EOF
<Good>
    <Kod>tv8</Kod>
    <Name>pagetitle</Name>
    <Options>
        <Stock>tv5</Stock>
        <Image>tv2</Image>
    </Options>
    <Prices>
        <RegularPrice>tv1</RegularPrice>
        <OldPrice>tv20</OldPrice>
    </Prices>
</Good>
EOF;

//тестирование конфигурации (без записи в БД). Отчёты -> Журнал ошибок.
$cf_config['imp_testmode'] = false; // изначально false

//функция для фильтрации значений при ИМПОРТЕ
function filter_import($value_arr){
    $output_arr = $value_arr;
    
    if( isset($output_arr['content']['publishedon']) ) {
        $output_arr['content']['publishedon'] = strtotime($output_arr['content']['publishedon'] . "+1 days");
    }
    /*
    if(isset($output_arr['content']['pagetitle']))
        $output_arr['content']['pagetitle'] = mb_strtoupper($output_arr['content']['pagetitle'], 'UTF-8');
    */
    return $output_arr;
}


//функция для фильтрации значений при ЭКСПОРТЕ
function filter_export($value_arr,$doc_id=0) {
    $output_arr = $value_arr;
    if ( isset($output_arr['publishedon']) && $output_arr['publishedon'] != 0 ) {
        $output_arr['publishedon'] = date("d.m.Y", $output_arr['publishedon']);
    }
    return $output_arr;
}


?>