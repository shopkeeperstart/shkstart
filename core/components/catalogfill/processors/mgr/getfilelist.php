<?php
/**
 * Get a files list
 *
 * @package catalogfill
 * @subpackage processors
 */

require_once MODX_CORE_PATH."components/catalogfill/model/catalogfill.class.php";
$catalogfill = new Catalogfill($modx);
//$catalogfill->getModConfig();

$type = isset($scriptProperties['type']) ? $scriptProperties['type'] : 'import';
switch ($type){
    case 'import':
        $list = $catalogfill->filesList();
    break;
    case 'config':
        $list = $catalogfill->configList();
    break;
}

$count = count($list);

return $this->outputArray($list,$count);