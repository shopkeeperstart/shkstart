<?php
/**
 * Get a files list
 *
 * @package catalogfill
 * @subpackage processors
 */

$output = array();

$output['resources'] = $modx->getCount('modResource');
$output['resources_pub'] = $modx->getCount('modResource',array('published'=>1));
$output['tvs'] = $modx->getCount('modTemplateVarResource');

return $this->outputArray($output);