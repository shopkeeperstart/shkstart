<?php
/**
 * @package catalogfill
 * @subpackage processors
 */

@ini_set("upload_max_filesize","15M");
@ini_set("post_max_size","15M");
@ini_set("max_execution_time","1200"); //20 min.
@ini_set("max_input_time","1200"); //20 min.
@ini_set('memory_limit', '256M');
@ini_set('auto_detect_line_endings',1);
@set_time_limit(0);
@date_default_timezone_set('Europe/Moscow');
@setlocale (LC_ALL, 'ru_RU.UTF-8');

//ini_set('display_errors',1);
//error_reporting(E_ALL);

$modx->getService('lexicon','modLexicon');
$modx->lexicon->load($modx->config['manager_language'].':catalogfill:default');

$data = json_decode($scriptProperties['data'], true);

if (empty($data['config_file'])) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_conf'));

require MODX_CORE_PATH."components/catalogfill/config/".$data['config_file'].".php";
require_once MODX_CORE_PATH."components/catalogfill/model/catalogfill.class.php";
$catalogfill = new Catalogfill($modx,$cf_config);

$prepare = $catalogfill->importPrepareParent($data['parent_imp']);
if(!$prepare) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_parent'));

if($data['imp_type']=='delete_files'){
    
    //удаляем все файлы из папки /assets/components/catalogfill/files/
    $dir_path = '';
    $out = $catalogfill->cleanDir($catalogfill->config['files_import_dir']);
    
}else if($data['imp_type']=='clean_parent'){
    
    //очистка родительской категории
    if (strlen($catalogfill->config['parent_id'])==0) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_parent'));
    $out = $catalogfill->cleanParent($catalogfill->config['parent_id']);
    
}else{
    
    //импорт товаров
    if (strlen($catalogfill->config['parent_id'])==0) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_parent'));
    if (empty($data['imp_file'])) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_file'));
    $file_type_arr = explode('.',$data['imp_file']);
    $file_type = strtolower(end($file_type_arr));
    
    if (!in_array($file_type,array('csv','xls','xlsx','xml'))) return $modx->error->failure($modx->lexicon('catalogfill.mess_file_not_support'));
    
    $catalogfill->config['imp_update'] = $data['imp_type']=='update';
    $skip = $data['skip'];
    $is_first = isset($data['is_first']) ? $data['is_first'] : 0;
    $out = array();
    
    //Обновление кэша перед импортом
    if( $skip || $is_first ){
        
        /*
        $query = $modx->newQuery('modContext');
        $query->where(array('key:<>'=>'mgr'));
        $query->select($modx->escape('key'));
        if ($query->prepare() && $query->stmt->execute()) {
            $contextKeys = $query->stmt->fetchAll(PDO::FETCH_COLUMN);
        }
        */
        $modx->cacheManager->refresh(array(
            'context_settings' => array('contexts' => array( $catalogfill->config['context'] ) )
        ));
        
    }
    
    //Изменение значений полей перед импортом
    if($is_first){
        //Очищаем лог ошибок
        $log_file = $modx->getOption(xPDO::OPT_CACHE_PATH).'logs/error.log';
        if (file_exists($log_file)) {
            $cacheManager= $modx->getCacheManager();
            $cacheManager->writeFile($log_file,' ');
        }
        if($catalogfill->config['imp_before_change']){
            $catalogfill->fieldsValueChange($catalogfill->config['parent_id']);
        }
        $start_string = !empty($data['current_string']) && is_numeric($data['current_string']) ? $data['current_string'] : 0;
        return $modx->error->success('',array('pos'=>$start_string,'lines_count'=>$start_string+1));
        exit;
    }
    
    if( !empty( $cf_config['imp_custom_processor'] ) && file_exists( MODX_CORE_PATH.$cf_config['imp_custom_processor'] ) ){
        
        require MODX_CORE_PATH.$cf_config['imp_custom_processor'];
        
    }else{
        if($file_type  =='csv'){
            $out = $catalogfill->csv_import($data['imp_file'],$skip);
        }else if($file_type == 'xls'){
            $out = $catalogfill->xls_import($data['imp_file'],'Excel5',$skip);
        }else if($file_type == 'xlsx'){
            $out = $catalogfill->xls_import($data['imp_file'],'Excel2007',$skip);
        }else if($file_type == 'xml'){
            $out = $catalogfill->xml_import($data['imp_file'],$skip);
        }
    }
}

return $modx->error->success('',$out);
