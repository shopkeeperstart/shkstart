<?php
/**
 * @package catalogfill
 * @subpackage processors
 */

@ini_set("upload_max_filesize","15M");
@ini_set("post_max_size","15M");
@ini_set("max_execution_time","1200"); //20 min.
@ini_set("max_input_time","1200"); //20 min.
@ini_set('memory_limit', '256M');
@ini_set('auto_detect_line_endings',1);
@set_time_limit(0);
@date_default_timezone_set('Europe/Moscow');
@setlocale(LC_ALL, 'ru_RU.UTF-8');

$modx->getService('lexicon','modLexicon');
$modx->lexicon->load($modx->config['manager_language'].':catalogfill:default');

$data = json_decode($scriptProperties['data'], true);
if (empty($data['parent_exp'])) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_parent'));
if (empty($data['config_file'])) return $modx->error->failure($modx->lexicon('catalogfill.mess_no_conf'));

require MODX_CORE_PATH."components/catalogfill/config/".$data['config_file'].".php";
require_once MODX_CORE_PATH."components/catalogfill/model/catalogfill.class.php";
$catalogfill = new Catalogfill($modx,$cf_config);

$out = array();

switch($data['exp_type']){
    case 'csv':
        $out = $catalogfill->csv_export($data['parent_exp']);
    break;
    case 'xls':
        $out = $catalogfill->xls_export($data['parent_exp']);
    break;
    case 'xlsx':
        $out = $catalogfill->xls_export($data['parent_exp'],'Excel2007');
    break;
}

if($out===false)
    return $modx->error->failure($catalogfill->message);
else
    return $modx->error->success('',$out);
