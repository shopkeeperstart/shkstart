<div class="col-md-4 shk-item">
  <form action="[[~[[*id]]? &scheme=`abs`]]" method="post">
    <div class="panel panel-default">
      <div class="panel-body">
        <a href="[[~[[+id]]? &scheme=`abs`]]"><strong>[[+pagetitle]] - [[+longtitle]]</strong></a>
        <a href="[[~[[+id]]? &scheme=`abs`]]"><img  class="img-responsive" src="[[pthumb? &input=`assets/resourceimages/[[+id]]/[[+tv.image]]` &options=`w=250&h=350&zc=TC`]]" alt=""></a>
        <br>
        <div>Цена: <span class="shk-price">[[+tv.price:num_format]]</span> руб.</div>
      </div>
      <input type="hidden" name="shk-id" value="[[+id]]" />
      <input type="hidden" name="shk-name" value="[[+pagetitle]]" />
      <input type="hidden" name="shk-count" value="1" />
      
      <div class="panel-footer">
        <button type="submit" class="btn-block shk-but btn btn-default shk-but">В корзину</button>
        <a href="[[~[[+id]]? &scheme=`abs`]]" class="btn-block btn btn-primary">Подробнее</a>
      </div>
    </div>
  </form>
</div>