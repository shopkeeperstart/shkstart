<tr class="cart-order">
  <td class="uk-vertical-align"><img class="uk-vertical-align-middle uk-thumbnail" src="[[pthumb? &input=`assets/resourceimages/[[+id]]/[[+image]]` &options=`w=100&h=140&zc=TC`]]" alt="" ></td>
  <td class="uk-vertical-align">
    <div class="uk-vertical-align-middle"><a href="[[+url]]">[[+name]]</a>
      <br><span class="params">[[+addit_data:stripString=`#`:default=`Параметры товара не выбраны`]]</span></div>
  </td>
  <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><input class="shk-count" type="text" size="2" name="count[]" maxlength="3" title="изменить количество" value="[[+count]]" /></span></td>
  <td class="uk-vertical-align"><span class="uk-vertical-align-middle">[[+price]] [[+currency]]</span></td>
  <td class="uk-vertical-align"><span class="uk-vertical-align-middle"><a href="[[+url_del_item]]" class="shk-del uk-icon-trash uk-text-center uk-text-large"></a></span></td>
</tr>