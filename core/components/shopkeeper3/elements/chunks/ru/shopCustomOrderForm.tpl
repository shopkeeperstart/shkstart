<script type="text/javascript">
$(document).bind('ready',function(){
    if ( SHK.data.delivery_name ) {
        $('select[name="shk_delivery"]','#shopOrderForm').val( SHK.data.delivery_name );
    }
    $('select[name="shk_delivery"]','#shopOrderForm').bind('change',function(){
        SHK.selectDelivery( $(this).val() );
    });
});
</script>

[[!shkOptions?
&get=`delivery,payments`
&post_name=`shk_delivery,payment`
&toPlaceholders=`1`
&pl_prefix=`shkopt_`
&tpl=`select_option`
]]

<form method="post" action="[[~[[*id]]]]" id="shopOrderForm" class="ajax_form uk-form uk-form-stacked">
  <input type="text" name="nospam:blank" value="" style="display:none;" />
  <input type="hidden" name="order" value="1" />
  <div data-uk-margin="{cls:'uk-margin-top'}">
  <div class="uk-grid uk-grid-small" uk-text-center">
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Имя <span class="required uk-text-danger">*</span></label>
      <input type="text" name="fullname" placeholder="Владимир" class="uk-width-1-1" value="[[!+fi.fullname:default=`[[+modx.user.id:userinfo=`fullname`]]`:ne=`0`:show]]">
      <div class="error_fullname uk-text-small">[[!+fi.error.fullname]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Адрес <span class="required uk-text-danger">*</span></label>
      <input type="text" name="address" size="30" type="text" value="[[!+fi.address:default=`[[+modx.user.id:userinfo=`address`]]`:ne=`0`:show]]" placeholder="Украина, 35000, г. Киев, ул. Ленина, 50, кв. 71" class="uk-width-1-1">
      <div class="error_address uk-text-small">[[!+fi.error.address]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Телефон <span class="required uk-text-danger">*</span></label>
      <input name="phone" type="text" placeholder="+38 (099) 061-74-40" class="uk-width-1-1" value="[[!+fi.phone:default=`[[+modx.user.id:userinfo=`phone`]]`:ne=`0`:show]]">
      <div class="error_phone uk-text-small">[[!+fi.error.phone]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Email <span class="required uk-text-danger">*</span></label>
      <input name="email" type="text" placeholder="user@gmail.com" class="uk-width-1-1" value="[[!+fi.email:default=`[[+modx.user.id:userinfo=`email`]]`:ne=`0`:show]]">
      <div class="error_email uk-text-small">[[!+fi.error.email]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Вариант оплаты</label>
      <select class="uk-width-1-1" name="payment" >
        <option value=""></option>
        [[!+shkopt_payments]]
      </select>
      <div>[[!+fi.error.payment]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Вариант оплаты</label>
      <select class="uk-width-1-1" name="shk_delivery" >
        <option value=""></option>
        [[!+shkopt_delivery]]
      </select>
      <div>[[!+fi.error.shk_delivery]]</div>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <label class="uk-form-label" for="">Комментарий</label>
      <textarea name="message" class="uk-width-1-1" rows="4" cols="30">[[!+fi.message]]</textarea>
    </div>
    <div class="uk-width-1-1 uk-margin-bottom">
      <button type="submit" class="uk-button uk-button-large uk-button-success uk-width-1-1"><i class="uk-icon-paper-plane"></i> &nbsp;Отправить заказ</button>
    </div>
  </div>
  [[+fi.success:is=`1`:then=`
  <div class="alert alert-success">[[+fi.successMessage]]</div>
  `]]
  [[+fi.validation_error:is=`1`:then=`
  <div class="alert alert-danger">[[+fi.validation_error_message]]</div>
  `]]
</form>