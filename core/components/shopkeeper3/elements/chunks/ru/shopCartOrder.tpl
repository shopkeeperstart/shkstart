<div class="shop-cart" data-shopcart="3">
    <h3>Корзина</h3>
    <div class="empty">
        <div class="shop-cart-empty">Пусто</div>
    </div>
</div>
<!--tpl_separator-->
<div class="shop-cart" data-shopcart="3">
    <h3>Корзина</h3>
    <div class="full">
        <form action="[[+this_page_url]]#shopCart" method="post">
            <div  style="text-align:right;">
                <a href="[[+empty_url]]" id="shk_butEmptyCart">Очистить корзину</a>
            </div>
            <table class="uk-table uk-table-hover uk-table-striped">
                <tbody>
                    [[+inner]]
                </tbody>
            </table>
            <div  style="text-align:right;">
                Доставка: [[+delivery_name]] ([[+delivery_price]] [[+currency]])
            </div>
            <div  style="text-align:right;">
                Общая сумма: <b>[[+price_total]]</b> [[+currency]]
            </div>
            <noscript>
                <div><input type="submit" name="shk_recount" value="Пересчитать" /></div>
            </noscript>
        </form>
    </div>
</div>