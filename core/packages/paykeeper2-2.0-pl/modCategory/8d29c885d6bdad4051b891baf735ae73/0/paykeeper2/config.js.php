<?php

/**
 * @package modx
 * @var modX $modx
 */

define('MODX_CONNECTOR_INCLUDED',true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . "/config.core.php";
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
define('MODX_REQP', false);
require_once MODX_CONNECTORS_PATH . "index.php";

$cmp_config = array();

$cmp_config['auth_token'] = $modx->user->getUserToken($modx->context->get('key'));
$cmp_config['assets_url'] = $modx->getOption('assets_url') . 'components/paykeeper2/';
$cmp_config['manager_url'] = $modx->getOption('manager_url');
$cmp_config['manager_language'] = $modx->getOption('manager_language');
$cmp_config['connector_url'] = $cmp_config['assets_url'] . 'connector.php';

//lexicon
$modx->getService('lexicon', 'modLexicon');
$modx->lexicon->load($cmp_config['manager_language'] . ':paykeeper2:manager');
//$cmp_config['lang'] = $modx->lexicon->fetch('paykeeper2.');
/*
foreach($modx->config as $key => $val){
    
    if(substr($key, 0, 8) == 'paykeeper2'){
        $cmp_config[$key] = $val;
    }
    
}
*/

echo "
/* global config */
var cmp_config = ".(defined('JSON_PRETTY_PRINT') ? json_encode($cmp_config, JSON_PRETTY_PRINT) : json_encode($cmp_config)).";
";
