<?php

/**
 * Paykeeper get payment result - WebMoney
 *
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
require_once MODX_CORE_PATH . 'model/modx/modx.class.php';

$modx = new modX();
$modx->initialize('web');

$debug = $modx->getOption('paykeeper2.debug', null, false);

if( $debug ){
    ini_set('display_errors',1);
    error_reporting(E_ALL);
}

$paykeeper_path = $modx->getOption('core_path') . 'components/paykeeper2';
require_once $paykeeper_path . '/model/paykeeper.class.php';

$paykeeper = new Paykeeper( $modx, array(
    'gateway' => 'WebMoney'
));

if( $debug ) {
    array_map( 'urldecode', $_REQUEST );
    $paykeeper->logging( $_REQUEST, 'Request data' );
}

$orderData = array(
    'merchantPurse' => $paykeeper->getGatewayOption( 'merchantPurse' ),
    'webMoneyId' => $paykeeper->getGatewayOption( 'webMoneyId' ),
    'secretKey' => $paykeeper->getGatewayOption( 'secretKey' )
);

$request = $paykeeper->gateway->completePurchase($orderData);
$data = $request->getData();

if( $debug ) {
    $paykeeper->logging( $data );
}

if( !empty( $data['LMI_SECRET_KEY'] ) && $data['LMI_SECRET_KEY'] != $orderData['secretKey']){
    exit;
}

//Prerequest
if(isset($_POST['LMI_PREREQUEST']) && $_POST['LMI_PREREQUEST'] == 1){
    
    if( isset( $data['LMI_PAYMENT_NO'] ) && is_numeric( $data['LMI_PAYMENT_NO'] ) ){
        
        $orderSumAmount = isset( $data['LMI_PAYMENT_AMOUNT'] ) ? (float) str_replace( ',', '.', $data['LMI_PAYMENT_AMOUNT'] ) : 0;
        $requestPaymentId = isset( $data['LMI_PAYMENT_NO'] ) && is_numeric( $data['LMI_PAYMENT_NO'] ) ? (int) $data['LMI_PAYMENT_NO'] : 0;
        $paymentId = $paykeeper->getPaymentId( $orderSumAmount, '', 'initial', $requestPaymentId );
        
        if( $paymentId ){
            $paykeeper->setPaymentState( $paymentId, 'pending' );
            echo 'YES';
        }
    }
}
else if( !empty( $data['LMI_HASH'] ) ){
    
    $orderSumAmount = isset( $data['LMI_PAYMENT_AMOUNT'] ) ? (float) str_replace( ',', '.', $data['LMI_PAYMENT_AMOUNT'] ) : 0;
    $requestPaymentId = isset( $data['LMI_PAYMENT_NO'] ) && is_numeric( $data['LMI_PAYMENT_NO'] ) ? (int) $data['LMI_PAYMENT_NO'] : 0;
    $paymentId = $paykeeper->getPaymentId( $orderSumAmount, '', 'pending', $requestPaymentId );
    
    if( !$paymentId ){
        exit;
    }
    
    $response = $request->send();
    $responseData = $response->getData();
    
    if( !empty( $responseData ) ){
        
        $paykeeper->completePurchase( $paymentId );//Ставим успешные статусы и отправляем письма
        
        echo 'YES';
    }
    
} else {
    echo 'YES';// Next payment step
}
