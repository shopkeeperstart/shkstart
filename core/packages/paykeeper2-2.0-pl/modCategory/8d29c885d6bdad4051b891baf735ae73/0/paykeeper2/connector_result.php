<?php

/**
 * Paykeeper get payment result - Yandex.Kassa
 *
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
require_once MODX_CORE_PATH . 'model/modx/modx.class.php';

$modx = new modX();
$modx->initialize('web');

$debug = $modx->getOption('paykeeper2.debug', null, false);

if( $debug ){
    ini_set('display_errors',1);
    error_reporting(E_ALL);
}

$paykeeper_path = $modx->getOption('core_path') . 'components/paykeeper2';
require_once $paykeeper_path . '/model/paykeeper.class.php';

$paykeeper = new Paykeeper( $modx, array(
    'gateway' => 'YandexMoney'
));

if( $debug ) {
    array_map( 'urldecode', $_REQUEST );
    $paykeeper->logging( $_REQUEST, 'Request data' );
}
$action = isset( $_POST['action'] ) ? $_POST['action'] : '';

switch($action){
    case "checkOrder"://Проверка заказа
        
        $md5 = isset( $_POST['md5'] ) ? strtolower($_POST['md5']) : '';
        $hash = $_POST['action'].';'
            . $_POST['orderSumAmount'].';'
            . $_POST['orderSumCurrencyPaycash'].';'
            . $_POST['orderSumBankPaycash'].';'
            . $modx->getOption('paykeeper2.YandexMoney.shopid').';'
            . $_POST['invoiceId'].';'
            . $_POST['customerNumber'].';'
            . $modx->getOption('paykeeper2.YandexMoney.shopPassword');
        $hash_md5 = strtolower( md5( $hash ) );
        
        $paymentOrderId = isset( $_POST['orderNumber'] ) ? (int) $_POST['orderNumber'] : 0;
        $orderSumAmount = isset( $_POST['orderSumAmount'] ) ? (float) str_replace( ',', '.', $_POST['orderSumAmount'] ) : '';
        $user_email = isset( $_POST['customerNumber'] ) ? (string) trim( $_POST['customerNumber'] ) : '';
        $code = $hash_md5 != $md5 ? 1 : 0;//1 - error
        $paymentId = 0;
        
        if( !$code ){
            $paymentId = $paykeeper->getPaymentId( $orderSumAmount, $user_email, 'initial' );
            if( $paymentId ){
                $paykeeper->setPaymentState( $paymentId, 'pending' );
            } else {
                $code = 1;
            }
        }
        
        if( $debug ) {
            $paykeeper->logging( " - {$action} - code: {$code}, paymentId: {$paymentId}, md5: {$md5}, hash_md5: {$hash_md5}, hash: {$hash}" );
        }
        
        header('Content-type: text/xml; charset=utf-8');
        echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<checkOrderResponse performedDatetime="' . $_POST['requestDatetime'] . '" code="' . $code . '"'. ' invoiceId="' . $_POST['invoiceId'] . '" shopId="' . $modx->getOption('paykeeper2.YandexMoney.shopid') . '"/>';
        
    break;
    case "paymentAviso"://Уведомление
        
        $md5 = isset( $_POST['md5'] ) ? strtolower($_POST['md5']) : '';
        $hash = $_POST['action'].';'
            . $_POST['orderSumAmount'].';'
            . $_POST['orderSumCurrencyPaycash'].';'
            . $_POST['orderSumBankPaycash'].';'
            . $modx->getOption('paykeeper2.YandexMoney.shopid').';'
            . $_POST['invoiceId'].';'
            . $_POST['customerNumber'].';'
            . $modx->getOption('paykeeper2.YandexMoney.shopPassword');
        $hash_md5 = strtolower( md5( $hash ) );
        
        $paymentOrderId = isset( $_POST['orderNumber'] ) ? (int) $_POST['orderNumber'] : 0;
        $orderSumAmount = isset( $_POST['orderSumAmount'] ) ? (float) str_replace( ',', '.', $_POST['orderSumAmount'] ) : '';
        $user_email = isset( $_POST['customerNumber'] ) ? (string) trim( $_POST['customerNumber'] ) : '';
        $code = $hash_md5 != $md5 ? 1 : 0;//1 - error
        $paymentId = 0;
        
        if( !$code ){
            $paymentId = $paykeeper->getPaymentId( $orderSumAmount, $user_email, 'pending' );
            if( !$paymentId ){
                $code = 1;
            }
        }
        
        if( $paymentId ){
            $paymentState = !$code ? 'success' : 'fail';
            $paykeeper->setPaymentState( $paymentId, $paymentState );
            if( $paymentState == 'success' ){
		
		$payment = $paykeeper->getPayment( $paymentId );
		if( $payment !== false ){
		    
		    if( $payment['orderid'] ){
			
			//Переводим заказ в статус "Оплачен"
			$paykeeper->shkOrderPaid( $payment['orderid'] );
			
		    } else {
			//Отправляем письмо со ссылкой на скачивание электронного товара
			$paykeeper->createDownloadCode( $user_email );
		    }
		    
		}
                
                $paykeeper->sendAdminNotification( $paymentId );
            }
        }
        
        if( $debug ) {
            $paykeeper->logging( " - {$action} - code: {$code}, paymentId: {$paymentId}, md5: {$md5}, hash_md5: {$hash_md5}, hash: {$hash}" );
        }
        
        header('Content-type: text/xml; charset=utf-8');
        echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<paymentAvisoResponse performedDatetime="' . $_POST['requestDatetime'] . '" code="' . $code . '" invoiceId="' . $_POST['invoiceId'] . '" shopId="' . $modx->getOption('paykeeper2.YandexMoney.shopid') . '"/>';
        
    break;
}
