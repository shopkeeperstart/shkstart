<?php

define('MODX_REQP', false);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
require_once MODX_CONNECTORS_PATH . 'index.php';

$corePath = $modx->getOption('doodles.core_path', null, $modx->getOption('core_path').'components/paykeeper2/');
$packagePath = $corePath . 'model/';

$added = $modx->addPackage('paykeeper2', $packagePath);
$modx->lexicon->load('paykeeper2:default');

$path = $modx->getOption('processorsPath', null, $corePath . 'processors/');
$modx->request->handleRequest(array(
    'processors_path' => $path,
    'location' => '',
));
