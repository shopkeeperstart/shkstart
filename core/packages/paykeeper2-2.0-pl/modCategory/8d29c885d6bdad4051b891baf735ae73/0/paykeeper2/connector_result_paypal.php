<?php

/**
 * Paykeeper get payment result - PayPal
 *
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
require_once MODX_CORE_PATH . 'model/modx/modx.class.php';

$modx = new modX();
$modx->initialize('web');

$debug = $modx->getOption('paykeeper2.debug', null, false);

if( $debug ){
    ini_set('display_errors',1);
    error_reporting(E_ALL);
}

$paykeeper_path = $modx->getOption('core_path') . 'components/paykeeper2';
require_once $paykeeper_path . '/model/paykeeper.class.php';

$paykeeper = new Paykeeper( $modx, array(
    'gateway' => 'PayPal'
));

if( $debug ) {
    array_map( 'urldecode', $_REQUEST );
    $paykeeper->logging( $_REQUEST, 'Request data' );
}

$orderData = array(
    'username' => $paykeeper->getGatewayOption( 'username' ),
    'password' => $paykeeper->getGatewayOption( 'password' ),
    'signature' => $paykeeper->getGatewayOption( 'signature' ),
    'amount' => $paykeeper->sessionGet( 'paymentData', 'amount' ),
    'currency' => $paykeeper->sessionGet( 'paymentData', 'currency' )
);

$sessionData = $paykeeper->sessionGet( 'paymentData' );
if( $debug ) {
    $paykeeper->logging( $sessionData, 'Session data' );
}

$response = $paykeeper->gateway->completePurchase( $orderData )->send();
$responseData = $response->getData();

if( $debug ) {
    $paykeeper->logging( $responseData );
}

$successUrl = $paykeeper->getGatewayOption( 'successUrl' );
$failUrl =  $paykeeper->getGatewayOption( 'cancelUrl' );

if ( $response->isSuccessful() ){
    
    $orderSumAmount = (float) str_replace( ',', '.', $orderData['amount'] );
    $requestPaymentId = (int) $paykeeper->sessionGet( 'paymentData', 'transactionId' );
    $paymentId = $paykeeper->getPaymentId( $orderSumAmount, '', 'initial', $requestPaymentId );
    
    if( !$paymentId ){
        $paykeeper->redirect( $failUrl );
    }
    
    $paykeeper->completePurchase( $paymentId );//Ставим успешные статусы и отправляем письма
    $paykeeper->redirect( $successUrl );
    
} else {
    
    $paykeeper->redirect( $failUrl );
    
}
