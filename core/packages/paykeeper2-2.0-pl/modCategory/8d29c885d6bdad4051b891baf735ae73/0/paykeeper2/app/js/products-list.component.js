"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
var core_1 = require('angular2/core');
var common_1 = require('angular2/common');
var http_1 = require('angular2/http');
var router_1 = require('angular2/router');
var product_service_1 = require('./product.service');
var product_form_component_1 = require('./product-form.component');
var ProductsListComponent = (function () {
    function ProductsListComponent(_router, _routeParams, _service) {
        this._router = _router;
        this._routeParams = _routeParams;
        this._service = _service;
        this.selectedProductId = 0;
        this.currentPage = _routeParams.get('num')
            ? parseInt(_routeParams.get('num'), 10)
            : 1;
        this.totalPages = 1;
        this.pages = [];
        this.limit = localStorage.getItem('cmpPageLimit')
            ? parseInt(localStorage.getItem('cmpPageLimit'), 10)
            : 10;
        this.selectedProduct = { id: null, name: '', price: null, count: null, buyers: '' };
    }
    ProductsListComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    ProductsListComponent.prototype.getData = function () {
        var _this = this;
        this._service.getProducts(this.currentPage, this.limit)
            .subscribe(function (results) {
            _this.products = results[0];
            _this.total = results[1];
            _this.totalPages = Math.ceil(results[1] / _this.limit);
            _this.generatePages();
        }, function (error) { return _this.errorMessage = error; });
    };
    ProductsListComponent.prototype.generatePages = function () {
        this.pages = Array.from(new Array(this.totalPages), function (x, i) { return i + 1; });
    };
    ProductsListComponent.prototype.openPage = function (num) {
        this._router.navigate(['Pages', { num: num }]);
    };
    ProductsListComponent.prototype.isActivePage = function (num) {
        return num === this.currentPage;
    };
    ProductsListComponent.prototype.changeLimit = function (value) {
        localStorage.setItem('cmpPageLimit', value);
        this.reloadData();
    };
    ProductsListComponent.prototype.reloadData = function () {
        if (this.currentPage == 1) {
            this.getData();
        }
        else {
            this.openPage(1);
        }
    };
    ProductsListComponent.prototype.newProduct = function () {
        this.selectedProduct = { id: null, name: '', price: null, count: null };
        $('#editProductModal').modal();
    };
    ProductsListComponent.prototype.editProduct = function (product) {
        this.selectedProduct = JSON.parse(JSON.stringify(product));
        $('#editProductModal').modal();
    };
    ProductsListComponent.prototype.onSaved = function () {
        this.reloadData();
        $('#editProductModal').modal('hide');
    };
    ProductsListComponent.prototype.onCanceled = function () {
        $('#editProductModal').modal('hide');
    };
    ProductsListComponent.prototype.editBuyers = function (product) {
        var _this = this;
        this.selectedProduct = JSON.parse(JSON.stringify(product));
        this.selectedProduct.buyers = '';
        this._service.getBuyers(this.selectedProduct.id)
            .subscribe(function (res) {
            if (res && res.success && res.object) {
                _this.selectedProduct.buyers = res.object;
            }
        });
        $('#editBuyersModal').modal();
    };
    ProductsListComponent.prototype.updateBuyers = function () {
        this._service.updateBuyers(this.selectedProduct.id, this.selectedProduct.buyers)
            .subscribe(function (res) {
            if (res && res.success) {
                $('#editBuyersModal').modal('hide');
            }
        });
    };
    ProductsListComponent.prototype.deleteProduct = function (id) {
        var _this = this;
        if (window.confirm('Вы уверены?')) {
            this._service.deleteProduct(id)
                .subscribe(function (res) {
                _this.reloadData();
            });
        }
    };
    ProductsListComponent = __decorate([
        core_1.Component({
            selector: 'div.products-list.table-responsive',
            templateUrl: './templates/products-table.html',
            providers: [http_1.HTTP_PROVIDERS, router_1.ROUTER_DIRECTIVES, product_service_1.ProductService],
            directives: [router_1.ROUTER_DIRECTIVES, common_1.CORE_DIRECTIVES, product_form_component_1.ProductFormComponent]
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.RouteParams, product_service_1.ProductService])
    ], ProductsListComponent);
    return ProductsListComponent;
}());
exports.ProductsListComponent = ProductsListComponent;
//# sourceMappingURL=products-list.component.js.map