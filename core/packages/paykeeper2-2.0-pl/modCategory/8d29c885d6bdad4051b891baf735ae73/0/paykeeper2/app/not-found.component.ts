import {Component} from 'angular2/core';

@Component({
    template: `<div class="panel-body"><div class="alert alert-info">Page not found.</div></div>`
})
export class NotFoundComponent {


}