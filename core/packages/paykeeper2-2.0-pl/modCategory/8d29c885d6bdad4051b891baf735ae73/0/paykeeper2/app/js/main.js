"use strict";
var browser_1 = require('angular2/platform/browser');
var core_1 = require('angular2/core');
require('rxjs/Rx');
var router_1 = require('angular2/router');
var app_component_1 = require('./app.component');
core_1.enableProdMode();
browser_1.bootstrap(app_component_1.AppComponent, [
    router_1.ROUTER_PROVIDERS,
    core_1.provide(router_1.APP_BASE_HREF, { useValue: '/' }),
    //provide(APP_BASE_HREF, {useValue: window.location.pathname}),
    core_1.provide(router_1.LocationStrategy, { useClass: router_1.HashLocationStrategy })
]);
//# sourceMappingURL=main.js.map