import {Component, Input, Output, HostListener, EventEmitter} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_PROVIDERS, FORM_DIRECTIVES} from 'angular2/common';
import {Product} from './product';
import {ProductService}   from './product.service';

@Component({
    selector: 'product-form',
    templateUrl: cmp_config.assets_url + 'templates/product-form.component.html',
    providers: [ FORM_PROVIDERS ],
    directives: [ CORE_DIRECTIVES, FORM_DIRECTIVES ]
})
export class ProductFormComponent {
    @Output() canceled = new EventEmitter();
    @Output() saved = new EventEmitter();

    @Input('product-id') productId: number;
    @Input('product') product: Product;

    constructor( private _service: ProductService ){

    }

    ngOnInit() {
        this.product = <Product>{ id: null, name: '', price: null, count: null };
    }

    getProductId(){
        return this.productId;
    }

    onSubmit(){
        this._service.saveItem(this.product)
            .subscribe(res => {
                if( res && res.success ){
                    this.onSaved();
                }
            });
    }

    onSaved() {
        this.saved.next(this.product);
    }

    onCanceled () {
        this.canceled.next(this.product);
    }

}