///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
import {Component, Input, OnInit, OnChanges} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';
import {HTTP_PROVIDERS} from 'angular2/http';
import {Router, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router';
import {Product} from './product';
import {ProductService}   from './product.service';
import {ProductFormComponent} from './product-form.component';

@Component({
    selector: 'div.products-list.table-responsive',
    templateUrl: cmp_config.assets_url + 'templates/products-table.html',
    providers:  [ HTTP_PROVIDERS, ROUTER_DIRECTIVES, ProductService ],
    directives: [ ROUTER_DIRECTIVES, CORE_DIRECTIVES, ProductFormComponent ]
})
export class ProductsListComponent implements OnInit {
    errorMessage: string;
    products: Product[];
    pages: number[];
    currentPage: number;
    limit: number;
    totalPages: number;
    total: number;
    selectedProductId: number;
    selectedProduct: Product;

    constructor(
        private _router: Router,
        private _routeParams: RouteParams,
        private _service: ProductService) {

        this.selectedProductId = 0;
        this.currentPage = _routeParams.get('num')
            ? parseInt(_routeParams.get('num'), 10)
            : 1;
        this.totalPages = 1;
        this.pages = [];
        this.limit = localStorage.getItem('cmpPageLimit')
            ? parseInt(localStorage.getItem('cmpPageLimit'), 10)
            : 10;
        this.selectedProduct = <Product>{ id: null, name: '', price: null, count: null, buyers: '' };
    }

    ngOnInit() {
        this.getData();
    }

    getData(){
        this._service.getProducts(this.currentPage, this.limit)
            .subscribe(
                results => {
                    this.products = results[0];
                    this.total = results[1];
                    this.totalPages = Math.ceil( results[1] / this.limit );
                    this.generatePages();
                },
                error =>  this.errorMessage = <any>error
            );
    }

    generatePages(){
        this.pages = Array.from(new Array(this.totalPages), (x,i) => i+1);
    }

    openPage(num: number){
        this._router.navigate( ['Pages', { num: num }]  );
    }

    isActivePage(num: number){
        return num === this.currentPage;
    }

    changeLimit(value: string){
        localStorage.setItem('cmpPageLimit', value);
        this.reloadData();
    }

    reloadData(){
        if(this.currentPage == 1){
            this.getData();
        } else {
            this.openPage(1);
        }
    }

    newProduct(){
        this.selectedProduct = <Product>{ id: null, name: '', price: null, count: null };
        $('#editProductModal').modal();
    }

    editProduct(product: Product){
        this.selectedProduct = <Product>JSON.parse(JSON.stringify(product));
        $('#editProductModal').modal();
    }

    onSaved(){
        this.reloadData();
        $('#editProductModal').modal('hide');
    }

    onCanceled(){
        $('#editProductModal').modal('hide');
    }

    editBuyers(product: Product){
        this.selectedProduct = <Product>JSON.parse(JSON.stringify(product));
        this.selectedProduct.buyers = '';
        this._service.getBuyers(this.selectedProduct.id)
            .subscribe(
                res => {
                    if(res && res.success && res.object){
                        this.selectedProduct.buyers = res.object;
                    }
                }
            );
        $('#editBuyersModal').modal();
    }

    updateBuyers(){
        this._service.updateBuyers(this.selectedProduct.id, this.selectedProduct.buyers)
            .subscribe(
                res => {
                    if(res && res.success){
                        $('#editBuyersModal').modal('hide');
                    }
                }
            );
    }

    deleteProduct(id: number){
        if ( window.confirm( 'Вы уверены?' ) ){
            this._service.deleteProduct(id)
                .subscribe(
                    res => {
                        this.reloadData();
                    }
                );
        }
    }

}