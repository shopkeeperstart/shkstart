import {bootstrap} from 'angular2/platform/browser';
import {provide, enableProdMode} from 'angular2/core';
import 'rxjs/Rx';
import {APP_BASE_HREF, ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {AppComponent} from './app.component';

enableProdMode();
bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    provide(APP_BASE_HREF, {useValue: '/'}),
    //provide(APP_BASE_HREF, {useValue: window.location.pathname}),
    provide(LocationStrategy, {useClass: HashLocationStrategy})
]);
