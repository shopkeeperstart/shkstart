declare module config
{
    export interface ConfigObject
    {
        auth_token: string;
        assets_url: string;
        manager_url: string;
        manager_language: string;
        connector_url: string;
    }
}
declare var cmp_config:config.ConfigObject;