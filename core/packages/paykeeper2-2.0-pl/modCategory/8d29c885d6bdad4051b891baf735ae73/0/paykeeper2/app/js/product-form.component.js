"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var common_1 = require('angular2/common');
var product_service_1 = require('./product.service');
var ProductFormComponent = (function () {
    function ProductFormComponent(_service) {
        this._service = _service;
        this.canceled = new core_1.EventEmitter();
        this.saved = new core_1.EventEmitter();
    }
    ProductFormComponent.prototype.ngOnInit = function () {
        this.product = { id: null, name: '', price: null, count: null };
    };
    ProductFormComponent.prototype.getProductId = function () {
        return this.productId;
    };
    ProductFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this._service.saveItem(this.product)
            .subscribe(function (res) {
            if (res && res.success) {
                _this.onSaved();
            }
        });
    };
    ProductFormComponent.prototype.onSaved = function () {
        this.saved.next(this.product);
    };
    ProductFormComponent.prototype.onCanceled = function () {
        this.canceled.next(this.product);
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ProductFormComponent.prototype, "canceled", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ProductFormComponent.prototype, "saved", void 0);
    __decorate([
        core_1.Input('product-id'), 
        __metadata('design:type', Number)
    ], ProductFormComponent.prototype, "productId", void 0);
    __decorate([
        core_1.Input('product'), 
        __metadata('design:type', Object)
    ], ProductFormComponent.prototype, "product", void 0);
    ProductFormComponent = __decorate([
        core_1.Component({
            selector: 'product-form',
            templateUrl: './templates/product-form.component.html',
            providers: [common_1.FORM_PROVIDERS],
            directives: [common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [product_service_1.ProductService])
    ], ProductFormComponent);
    return ProductFormComponent;
}());
exports.ProductFormComponent = ProductFormComponent;
//# sourceMappingURL=product-form.component.js.map