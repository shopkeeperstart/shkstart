var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
System.register("product", [], function(exports_1) {
    "use strict";
    return {
        setters:[],
        execute: function() {
        }
    }
});
System.register("product.service", ['angular2/core', 'angular2/http', 'rxjs/Observable'], function(exports_2) {
    "use strict";
    var core_1, http_1, Observable_1;
    var ProductService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            }],
        execute: function() {
            ProductService = (function () {
                function ProductService(http) {
                    this.http = http;
                }
                ProductService.prototype.getProducts = function (page, limit) {
                    if (limit === void 0) { limit = 10; }
                    var searchParams = new http_1.URLSearchParams();
                    searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
                    searchParams.set('limit', limit.toString());
                    searchParams.set('start', ((page - 1) * limit).toString());
                    searchParams.set('action', 'mgr/products/getList');
                    return this.http.get(cmp_config.connector_url, {
                        search: searchParams
                    })
                        .map(function (res) { return [res.json().results, parseInt(res.json().total, 10)]; })
                        .catch(this.handleError);
                };
                ProductService.prototype.saveItem = function (product) {
                    var searchParams = new http_1.URLSearchParams();
                    searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
                    searchParams.set('action', product.id ? 'mgr/products/update' : 'mgr/products/create');
                    return this.http.post(cmp_config.connector_url, JSON.stringify(product), {
                        search: searchParams
                    })
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                ProductService.prototype.deleteProduct = function (id) {
                    var searchParams = new http_1.URLSearchParams();
                    searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
                    searchParams.set('action', 'mgr/products/remove');
                    searchParams.set('id', id.toString());
                    return this.http.delete(cmp_config.connector_url, {
                        search: searchParams
                    })
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                ProductService.prototype.getBuyers = function (id) {
                    var searchParams = new http_1.URLSearchParams();
                    searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
                    searchParams.set('action', 'mgr/products/getBuyers');
                    searchParams.set('query', id.toString());
                    return this.http.get(cmp_config.connector_url, {
                        search: searchParams
                    })
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                ProductService.prototype.updateBuyers = function (id, buyers) {
                    var searchParams = new http_1.URLSearchParams();
                    searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
                    searchParams.set('action', 'mgr/products/updateBuyers');
                    searchParams.set('id', id.toString());
                    var data = { data: buyers };
                    return this.http.post(cmp_config.connector_url, JSON.stringify(data), {
                        search: searchParams
                    })
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                ProductService.prototype.handleError = function (error) {
                    return Observable_1.Observable.throw(error.json().error || 'Server error');
                };
                ProductService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof http_1.Http !== 'undefined' && http_1.Http) === 'function' && _a) || Object])
                ], ProductService);
                return ProductService;
                var _a;
            }());
            exports_2("ProductService", ProductService);
        }
    }
});
System.register("product-form.component", ['angular2/core', 'angular2/common', "product.service"], function(exports_3) {
    "use strict";
    var core_2, common_1, product_service_1;
    var ProductFormComponent;
    return {
        setters:[
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (product_service_1_1) {
                product_service_1 = product_service_1_1;
            }],
        execute: function() {
            ProductFormComponent = (function () {
                function ProductFormComponent(_service) {
                    this._service = _service;
                    this.canceled = new core_2.EventEmitter();
                    this.saved = new core_2.EventEmitter();
                }
                ProductFormComponent.prototype.ngOnInit = function () {
                    this.product = { id: null, name: '', price: null, count: null };
                };
                ProductFormComponent.prototype.getProductId = function () {
                    return this.productId;
                };
                ProductFormComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this._service.saveItem(this.product)
                        .subscribe(function (res) {
                        if (res && res.success) {
                            _this.onSaved();
                        }
                    });
                };
                ProductFormComponent.prototype.onSaved = function () {
                    this.saved.next(this.product);
                };
                ProductFormComponent.prototype.onCanceled = function () {
                    this.canceled.next(this.product);
                };
                __decorate([
                    core_2.Output(), 
                    __metadata('design:type', Object)
                ], ProductFormComponent.prototype, "canceled", void 0);
                __decorate([
                    core_2.Output(), 
                    __metadata('design:type', Object)
                ], ProductFormComponent.prototype, "saved", void 0);
                __decorate([
                    core_2.Input('product-id'), 
                    __metadata('design:type', Number)
                ], ProductFormComponent.prototype, "productId", void 0);
                __decorate([
                    core_2.Input('product'), 
                    __metadata('design:type', Object)
                ], ProductFormComponent.prototype, "product", void 0);
                ProductFormComponent = __decorate([
                    core_2.Component({
                        selector: 'product-form',
                        templateUrl: cmp_config.assets_url + 'templates/product-form.component.html',
                        providers: [common_1.FORM_PROVIDERS],
                        directives: [common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [product_service_1.ProductService])
                ], ProductFormComponent);
                return ProductFormComponent;
            }());
            exports_3("ProductFormComponent", ProductFormComponent);
        }
    }
});
System.register("products-list.component", ['angular2/core', 'angular2/common', 'angular2/http', 'angular2/router', "product.service", "product-form.component"], function(exports_4) {
    "use strict";
    var core_3, common_2, http_2, router_1, product_service_2, product_form_component_1;
    var ProductsListComponent;
    return {
        setters:[
            function (core_3_1) {
                core_3 = core_3_1;
            },
            function (common_2_1) {
                common_2 = common_2_1;
            },
            function (http_2_1) {
                http_2 = http_2_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (product_service_2_1) {
                product_service_2 = product_service_2_1;
            },
            function (product_form_component_1_1) {
                product_form_component_1 = product_form_component_1_1;
            }],
        execute: function() {
            ProductsListComponent = (function () {
                function ProductsListComponent(_router, _routeParams, _service) {
                    this._router = _router;
                    this._routeParams = _routeParams;
                    this._service = _service;
                    this.selectedProductId = 0;
                    this.currentPage = _routeParams.get('num')
                        ? parseInt(_routeParams.get('num'), 10)
                        : 1;
                    this.totalPages = 1;
                    this.pages = [];
                    this.limit = localStorage.getItem('cmpPageLimit')
                        ? parseInt(localStorage.getItem('cmpPageLimit'), 10)
                        : 10;
                    this.selectedProduct = { id: null, name: '', price: null, count: null, buyers: '' };
                }
                ProductsListComponent.prototype.ngOnInit = function () {
                    this.getData();
                };
                ProductsListComponent.prototype.getData = function () {
                    var _this = this;
                    this._service.getProducts(this.currentPage, this.limit)
                        .subscribe(function (results) {
                        _this.products = results[0];
                        _this.total = results[1];
                        _this.totalPages = Math.ceil(results[1] / _this.limit);
                        _this.generatePages();
                    }, function (error) { return _this.errorMessage = error; });
                };
                ProductsListComponent.prototype.generatePages = function () {
                    this.pages = Array.from(new Array(this.totalPages), function (x, i) { return i + 1; });
                };
                ProductsListComponent.prototype.openPage = function (num) {
                    this._router.navigate(['Pages', { num: num }]);
                };
                ProductsListComponent.prototype.isActivePage = function (num) {
                    return num === this.currentPage;
                };
                ProductsListComponent.prototype.changeLimit = function (value) {
                    localStorage.setItem('cmpPageLimit', value);
                    this.reloadData();
                };
                ProductsListComponent.prototype.reloadData = function () {
                    if (this.currentPage == 1) {
                        this.getData();
                    }
                    else {
                        this.openPage(1);
                    }
                };
                ProductsListComponent.prototype.newProduct = function () {
                    this.selectedProduct = { id: null, name: '', price: null, count: null };
                    $('#editProductModal').modal();
                };
                ProductsListComponent.prototype.editProduct = function (product) {
                    this.selectedProduct = JSON.parse(JSON.stringify(product));
                    $('#editProductModal').modal();
                };
                ProductsListComponent.prototype.onSaved = function () {
                    this.reloadData();
                    $('#editProductModal').modal('hide');
                };
                ProductsListComponent.prototype.onCanceled = function () {
                    $('#editProductModal').modal('hide');
                };
                ProductsListComponent.prototype.editBuyers = function (product) {
                    var _this = this;
                    this.selectedProduct = JSON.parse(JSON.stringify(product));
                    this.selectedProduct.buyers = '';
                    this._service.getBuyers(this.selectedProduct.id)
                        .subscribe(function (res) {
                        if (res && res.success && res.object) {
                            _this.selectedProduct.buyers = res.object;
                        }
                    });
                    $('#editBuyersModal').modal();
                };
                ProductsListComponent.prototype.updateBuyers = function () {
                    this._service.updateBuyers(this.selectedProduct.id, this.selectedProduct.buyers)
                        .subscribe(function (res) {
                        if (res && res.success) {
                            $('#editBuyersModal').modal('hide');
                        }
                    });
                };
                ProductsListComponent.prototype.deleteProduct = function (id) {
                    var _this = this;
                    if (window.confirm('Вы уверены?')) {
                        this._service.deleteProduct(id)
                            .subscribe(function (res) {
                            _this.reloadData();
                        });
                    }
                };
                ProductsListComponent = __decorate([
                    core_3.Component({
                        selector: 'div.products-list.table-responsive',
                        templateUrl: cmp_config.assets_url + 'templates/products-table.html',
                        providers: [http_2.HTTP_PROVIDERS, router_1.ROUTER_DIRECTIVES, product_service_2.ProductService],
                        directives: [router_1.ROUTER_DIRECTIVES, common_2.CORE_DIRECTIVES, product_form_component_1.ProductFormComponent]
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof router_1.Router !== 'undefined' && router_1.Router) === 'function' && _a) || Object, (typeof (_b = typeof router_1.RouteParams !== 'undefined' && router_1.RouteParams) === 'function' && _b) || Object, product_service_2.ProductService])
                ], ProductsListComponent);
                return ProductsListComponent;
                var _a, _b;
            }());
            exports_4("ProductsListComponent", ProductsListComponent);
        }
    }
});
System.register("not-found.component", ['angular2/core'], function(exports_5) {
    "use strict";
    var core_4;
    var NotFoundComponent;
    return {
        setters:[
            function (core_4_1) {
                core_4 = core_4_1;
            }],
        execute: function() {
            NotFoundComponent = (function () {
                function NotFoundComponent() {
                }
                NotFoundComponent = __decorate([
                    core_4.Component({
                        template: "<div class=\"panel-body\"><div class=\"alert alert-info\">Page not found.</div></div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], NotFoundComponent);
                return NotFoundComponent;
            }());
            exports_5("NotFoundComponent", NotFoundComponent);
        }
    }
});
System.register("app.component", ['angular2/core', 'angular2/router', 'angular2/common', 'angular2/http', "products-list.component", "not-found.component"], function(exports_6) {
    "use strict";
    var core_5, router_2, common_3, http_3, products_list_component_1, not_found_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_5_1) {
                core_5 = core_5_1;
            },
            function (router_2_1) {
                router_2 = router_2_1;
            },
            function (common_3_1) {
                common_3 = common_3_1;
            },
            function (http_3_1) {
                http_3 = http_3_1;
            },
            function (products_list_component_1_1) {
                products_list_component_1 = products_list_component_1_1;
            },
            function (not_found_component_1_1) {
                not_found_component_1 = not_found_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(_router) {
                    this._router = _router;
                    this.title = 'Электронные товары';
                    this.isManagerArea = window.location.href.indexOf('namespace=paykeeper2') > -1;
                }
                AppComponent.prototype.ngOnInit = function () {
                    $('body').tooltip({
                        selector: '[data-toggle="tooltip"]',
                        container: 'body',
                        trigger: 'hover',
                        placement: function () {
                            return this.$element.data('placement')
                                ? this.$element.data('placement')
                                : 'bottom';
                        }
                    });
                    if (!cmp_config.auth_token) {
                        this._router.navigate(['NotFound']);
                    }
                    else {
                        this._router.navigate(['Pages', { num: 1 }]);
                    }
                };
                AppComponent.prototype.openPage = function (num) {
                    this._router.navigate(['Pages', { num: num }]);
                };
                AppComponent.prototype.openNewWindow = function () {
                    if (this.isManagerArea) {
                        window.location.href = cmp_config.assets_url + 'index.html';
                    }
                    else {
                        window.location.href = cmp_config.manager_url + '?a=index&namespace=paykeeper2';
                    }
                };
                AppComponent = __decorate([
                    core_5.Component({
                        selector: 'my-app',
                        templateUrl: cmp_config.assets_url + 'templates/index.html',
                        providers: [http_3.HTTP_PROVIDERS],
                        directives: [router_2.ROUTER_DIRECTIVES, common_3.CORE_DIRECTIVES]
                    }),
                    router_2.RouteConfig([
                        { path: '/', name: 'Home', component: products_list_component_1.ProductsListComponent, useAsDefault: true },
                        { path: '/not_found', name: 'NotFound', component: not_found_component_1.NotFoundComponent },
                        { path: '/page/:num', name: 'Pages', component: products_list_component_1.ProductsListComponent }
                    ]), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof router_2.Router !== 'undefined' && router_2.Router) === 'function' && _a) || Object])
                ], AppComponent);
                return AppComponent;
                var _a;
            }());
            exports_6("AppComponent", AppComponent);
        }
    }
});
System.register("main", ['angular2/platform/browser', 'angular2/core', 'rxjs/Rx', 'angular2/router', "app.component"], function(exports_7) {
    "use strict";
    var browser_1, core_6, router_3, app_component_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (core_6_1) {
                core_6 = core_6_1;
            },
            function (_1) {},
            function (router_3_1) {
                router_3 = router_3_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            }],
        execute: function() {
            core_6.enableProdMode();
            browser_1.bootstrap(app_component_1.AppComponent, [
                router_3.ROUTER_PROVIDERS,
                core_6.provide(router_3.APP_BASE_HREF, { useValue: '/' }),
                //provide(APP_BASE_HREF, {useValue: window.location.pathname}),
                core_6.provide(router_3.LocationStrategy, { useClass: router_3.HashLocationStrategy })
            ]);
        }
    }
});
System.register("product-model", [], function(exports_8) {
    "use strict";
    var ProductModel;
    return {
        setters:[],
        execute: function() {
            ProductModel = (function () {
                function ProductModel(id, name, price, content) {
                    this.id = id;
                    this.name = name;
                    this.price = price;
                    this.content = content;
                }
                return ProductModel;
            }());
            exports_8("ProductModel", ProductModel);
        }
    }
});
//# sourceMappingURL=bundle.js.map