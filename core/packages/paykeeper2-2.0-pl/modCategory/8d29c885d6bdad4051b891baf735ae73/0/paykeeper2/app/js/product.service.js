"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
///<reference path="typings/config.d.ts" />
var core_1 = require('angular2/core');
var http_1 = require('angular2/http');
var Observable_1 = require('rxjs/Observable');
var ProductService = (function () {
    function ProductService(http) {
        this.http = http;
    }
    ProductService.prototype.getProducts = function (page, limit) {
        if (limit === void 0) { limit = 10; }
        var searchParams = new http_1.URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('limit', limit.toString());
        searchParams.set('start', ((page - 1) * limit).toString());
        searchParams.set('action', 'mgr/products/getList');
        return this.http.get(cmp_config.connector_url, {
            search: searchParams
        })
            .map(function (res) { return [res.json().results, parseInt(res.json().total, 10)]; })
            .catch(this.handleError);
    };
    ProductService.prototype.saveItem = function (product) {
        var searchParams = new http_1.URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', product.id ? 'mgr/products/update' : 'mgr/products/create');
        return this.http.post(cmp_config.connector_url, JSON.stringify(product), {
            search: searchParams
        })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductService.prototype.deleteProduct = function (id) {
        var searchParams = new http_1.URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/remove');
        searchParams.set('id', id.toString());
        return this.http.delete(cmp_config.connector_url, {
            search: searchParams
        })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductService.prototype.getBuyers = function (id) {
        var searchParams = new http_1.URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/getBuyers');
        searchParams.set('query', id.toString());
        return this.http.get(cmp_config.connector_url, {
            search: searchParams
        })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductService.prototype.updateBuyers = function (id, buyers) {
        var searchParams = new http_1.URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/updateBuyers');
        searchParams.set('id', id.toString());
        var data = { data: buyers };
        return this.http.post(cmp_config.connector_url, JSON.stringify(data), {
            search: searchParams
        })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    ProductService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map