"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var common_1 = require('angular2/common');
var http_1 = require('angular2/http');
var products_list_component_1 = require('./products-list.component');
var not_found_component_1 = require('./not-found.component');
var AppComponent = (function () {
    function AppComponent(_router) {
        this._router = _router;
        this.title = 'Электронные товары';
    }
    AppComponent.prototype.ngOnInit = function () {
        $('body').tooltip({
            selector: '[data-toggle="tooltip"]',
            container: 'body',
            trigger: 'hover',
            placement: function () {
                return this.$element.data('placement')
                    ? this.$element.data('placement')
                    : 'bottom';
            }
        });
        if (!cmp_config.auth_token) {
            this._router.navigate(['NotFound']);
        }
        else {
            this._router.navigate(['Pages', { num: 1 }]);
        }
    };
    AppComponent.prototype.openPage = function (num) {
        this._router.navigate(['Pages', { num: num }]);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: './templates/index.html',
            providers: [http_1.HTTP_PROVIDERS],
            directives: [router_1.ROUTER_DIRECTIVES, common_1.CORE_DIRECTIVES]
        }),
        router_1.RouteConfig([
            { path: '/', name: 'Home', component: products_list_component_1.ProductsListComponent, useAsDefault: true },
            { path: '/not_found', name: 'NotFound', component: not_found_component_1.NotFoundComponent },
            { path: '/page/:num', name: 'Pages', component: products_list_component_1.ProductsListComponent }
        ]), 
        __metadata('design:paramtypes', [router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map