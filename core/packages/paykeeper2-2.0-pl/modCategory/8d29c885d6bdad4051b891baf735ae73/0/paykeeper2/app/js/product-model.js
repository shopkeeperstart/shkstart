"use strict";
var ProductModel = (function () {
    function ProductModel(id, name, price, content) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.content = content;
    }
    return ProductModel;
}());
exports.ProductModel = ProductModel;
//# sourceMappingURL=product-model.js.map