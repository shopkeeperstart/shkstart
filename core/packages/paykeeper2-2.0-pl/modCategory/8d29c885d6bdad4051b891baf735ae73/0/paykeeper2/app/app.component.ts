///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router, RouteParams} from 'angular2/router';
import {CORE_DIRECTIVES} from 'angular2/common';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ProductsListComponent} from './products-list.component';
import {NotFoundComponent} from './not-found.component';

@Component({
    selector: 'my-app',
    templateUrl: cmp_config.assets_url + 'templates/index.html',
    providers:  [ HTTP_PROVIDERS ],
    directives: [ ROUTER_DIRECTIVES, CORE_DIRECTIVES ]
})
@RouteConfig([
    { path: '/', name: 'Home', component: ProductsListComponent, useAsDefault: true },
    { path: '/not_found', name: 'NotFound', component: NotFoundComponent },
    { path: '/page/:num', name: 'Pages', component: ProductsListComponent }
])
export class AppComponent {

    public title = 'Электронные товары';
    public isManagerArea: boolean;

    constructor( private _router: Router ) {

        this.isManagerArea = window.location.href.indexOf('namespace=paykeeper2') > -1;

    }

    ngOnInit() {

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]',
            container: 'body',
            trigger: 'hover',
            placement: function(){
                return this.$element.data('placement')
                    ? this.$element.data('placement')
                    : 'bottom';
            }
        });

        if(!cmp_config.auth_token){
            this._router.navigate( ['NotFound'] );
        } else {
            this._router.navigate( ['Pages', { num: 1 }]  );
        }
    }

    openPage(num: number){
        this._router.navigate( ['Pages', { num: num }]  );
    }

    openNewWindow(){
        if( this.isManagerArea ){
            window.location.href = cmp_config.assets_url + 'index.html';
        } else {
            window.location.href = cmp_config.manager_url + '?a=index&namespace=paykeeper2';
        }
    }

}