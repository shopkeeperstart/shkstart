///<reference path="typings/config.d.ts" />
import {Injectable} from 'angular2/core';
import {Http, Response, URLSearchParams} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import {Product} from './product';

@Injectable()
export class ProductService {

    constructor (private http: Http) {

    }

    getProducts(page: number, limit: number = 10) {
        var searchParams = new URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('limit', limit.toString());
        searchParams.set('start', ((page - 1) * limit).toString());
        searchParams.set('action', 'mgr/products/getList');

        return this.http.get(
                cmp_config.connector_url,
                {
                    search: searchParams
                }
            )
            .map(
                res => <any[]> [<Product[]> res.json().results, parseInt(res.json().total, 10)]
            )
            .catch(this.handleError);
    }

    saveItem(product: Product){
        var searchParams = new URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', product.id ? 'mgr/products/update' : 'mgr/products/create');

        return this.http.post(
                cmp_config.connector_url,
                JSON.stringify( product ),
                {
                    search: searchParams
                }
            )
            .map(
                res => <any> res.json()
            )
            .catch(this.handleError);
    }

    deleteProduct(id: number){
        var searchParams = new URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/remove');
        searchParams.set('id', id.toString());

        return this.http.delete(
            cmp_config.connector_url,
            {
                search: searchParams
            })
            .map(
                res => <any> res.json()
            )
            .catch(this.handleError);
    }

    getBuyers(id: number){
        var searchParams = new URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/getBuyers');
        searchParams.set('query', id.toString());

        return this.http.get(
            cmp_config.connector_url,
            {
                search: searchParams
            })
            .map(
                res => <any> res.json()
            )
            .catch(this.handleError);
    }

    updateBuyers(id: number, buyers: string){
        var searchParams = new URLSearchParams();
        searchParams.set('HTTP_MODAUTH', cmp_config.auth_token);
        searchParams.set('action', 'mgr/products/updateBuyers');
        searchParams.set('id', id.toString());

        var data = {data: buyers};

        return this.http.post(
            cmp_config.connector_url,
            JSON.stringify( data ),
            {
                search: searchParams
            }
            )
            .map(
                res => <any> res.json()
            )
            .catch(this.handleError);
    }

    private handleError (error: Response) {
        return Observable.throw(error.json().error || 'Server error');
    }
}