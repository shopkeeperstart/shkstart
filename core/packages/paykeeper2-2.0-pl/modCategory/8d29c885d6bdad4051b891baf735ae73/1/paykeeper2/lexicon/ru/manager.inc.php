<?php
/**
 * Paykeeper2 Russian language file
 *
 * @package paykeeper2
 * @subpackage lexicon
 * @language ru
 */

$_lang = array();

$_lang['paykeeper2'] = 'Электронные товары и онлайн оплата';
$_lang['paykeeper2.name'] = 'Электронные товары и онлайн оплата';
