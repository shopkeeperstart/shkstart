<?php
/**
 * Paykeeper2 Russian language file
 *
 * @package paykeeper2
 * @subpackage lexicon
 * @language ru
 */

$_lang = array();

$_lang['paykeeper2'] = 'Электронные товары и онлайн оплата';
$_lang['paykeeper2.name'] = 'Электронные товары и онлайн оплата';
$_lang['paykeeper2.debug'] = 'Режим отладки';
$_lang['paykeeper2.email_validation_error'] = 'Пожалуйста, проверьте корректность, указанного Вами, адреса эл. почты.';
$_lang['paykeeper2.purchases_not_found'] = 'Вы не делали у нас покупок.';
$_lang['paykeeper2.file_not_found'] = 'Файл не найден на сервере.';
$_lang['paykeeper2.your_purchases'] = 'Ваши покупки';
$_lang['paykeeper2.new_purchase'] = 'Новая покупка';
$_lang['paykeeper2.order_number'] = 'Заказ №[[+orderId]]';
$_lang['paykeeper2.new_payment'] = 'В Вашем магазине сделана новая покупка';
$_lang['paykeeper2.email_sent'] = 'На Ваш адрес электронной почты отправлено письмо с дальнейшими инструкциями.';

$_lang['setting_paykeeper2.debug'] = 'Режим отладки';
$_lang['setting_paykeeper2.YandexMoney.cancelUrl'] = 'URL для перехода при отмене оплаты';
$_lang['setting_paykeeper2.YandexMoney.returnUrl'] = 'URL для перехода после оплаты';
$_lang['setting_paykeeper2.YandexMoney.currencyNum'] = 'Валюта';
$_lang['setting_paykeeper2.YandexMoney.method'] = 'Метод оплаты';
$_lang['setting_paykeeper2.YandexMoney.scid'] = 'Номер витрины';
$_lang['setting_paykeeper2.YandexMoney.shopPassword'] = 'Пароль';
$_lang['setting_paykeeper2.YandexMoney.shopid'] = 'Идентификатор магазина';
$_lang['setting_paykeeper2.dateformat'] = 'Формат даты';
$_lang['setting_paykeeper2.downloadPageId'] = 'ID страницы скачивания эл. товаров';
$_lang['setting_paykeeper2.downloadLimitTime'] = 'Время работы ссылок на скачивание, мин.';
$_lang['setting_paykeeper2.payLimitTime'] = 'Время проведения оплаты, мин.';
$_lang['setting_paykeeper2.payStatusIn'] = 'Номер статуса заказа перед оплатой';
$_lang['setting_paykeeper2.payStatusOut'] = 'Номер статуса оплаты после оплаты';
