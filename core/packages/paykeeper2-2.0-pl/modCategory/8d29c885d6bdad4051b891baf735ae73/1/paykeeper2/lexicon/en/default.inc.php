<?php
/**
 * Paykeeper2 English language file
 *
 * @package paykeeper2
 * @subpackage lexicon
 * @language en
 */

$_lang = array();

$_lang['paykeeper2'] = 'Online payments';
$_lang['paykeeper2.name'] = 'Electronic goods and online payment';
$_lang['paykeeper2.debug'] = 'Debug mode';
$_lang['paykeeper2.email_validation_error'] = 'Please check the correctness of the e-mail';
$_lang['paykeeper2.purchases_not_found'] = 'You did not do our shopping.';
$_lang['paykeeper2.file_not_found'] = 'File not found on the server.';
$_lang['paykeeper2.your_purchases'] = 'Your purchases';
$_lang['paykeeper2.new_purchase'] = 'New purchase';
$_lang['paykeeper2.order_number'] = 'Order #[[+orderId]]';
$_lang['paykeeper2.new_payment'] = 'In your store made a new purchase';
$_lang['paykeeper2.email_sent'] = 'In your e-mail was sent a letter with further instructions.';

$_lang['setting_paykeeper2.debug'] = 'Debug mode';
$_lang['setting_paykeeper2.YandexMoney.cancelUrl'] = 'URL to go to when you cancel payment';
$_lang['setting_paykeeper2.YandexMoney.returnUrl'] = 'URL to go to after payment';
$_lang['setting_paykeeper2.YandexMoney.currencyNum'] = 'Currency';
$_lang['setting_paykeeper2.YandexMoney.method'] = 'Payment method';
$_lang['setting_paykeeper2.YandexMoney.scid'] = 'Room showcases';
$_lang['setting_paykeeper2.YandexMoney.shopPassword'] = 'Password';
$_lang['setting_paykeeper2.YandexMoney.shopid'] = 'ID shop';
$_lang['setting_paykeeper2.dateformat'] = 'Date format';
$_lang['setting_paykeeper2.downloadPageId'] = 'ID download page goods';
$_lang['setting_paykeeper2.downloadLimitTime'] = 'Time links to download, min.';
$_lang['setting_paykeeper2.payLimitTime'] = 'Time of payment, min.';
$_lang['setting_paykeeper2.payStatusIn'] = 'Number of order status before paying';
$_lang['setting_paykeeper2.payStatusOut'] = 'Number of payment status after payment';