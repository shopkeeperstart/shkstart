<?php
$xpdo_meta_map['pkItems']= array (
  'package' => 'paykeeper2',
  'version' => NULL,
  'table' => 'paykeeper2_items',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'name' => '',
    'content' => '',
    'price' => 0,
    'userid' => 0,
    'state' => '0',
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
      'default' => '',
      'index' => 'index',
    ),
    'content' => 
    array (
      'dbtype' => 'text',
      'precision' => '512',
      'phptype' => 'string',
      'null' => false,
      'default' => '',
    ),
    'price' => 
    array (
      'dbtype' => 'float',
      'precision' => '11',
      'phptype' => 'float',
      'null' => false,
      'default' => 0,
    ),
    'userid' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'int',
      'null' => false,
      'default' => 0,
    ),
    'state' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '11',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
  ),
  'indexes' => 
  array (
    'name' => 
    array (
      'alias' => 'name',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'name' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'composites' => 
  array (
    'pkPayments' => 
    array (
      'class' => 'pkPayments',
      'local' => 'id',
      'foreign' => 'itemid',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
