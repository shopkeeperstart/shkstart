<?php
$xpdo_meta_map['pkDownloads']= array (
  'package' => 'paykeeper2',
  'version' => NULL,
  'table' => 'paykeeper2_downloads',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'email' => '0',
    'secret_code' => '0',
    'datetime' => NULL,
    'last_activity' => NULL,
  ),
  'fieldMeta' => 
  array (
    'email' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '64',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
    'secret_code' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
    'datetime' => 
    array (
      'dbtype' => 'datetime',
      'phptype' => 'string',
      'null' => false,
    ),
    'last_activity' => 
    array (
      'dbtype' => 'datetime',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
