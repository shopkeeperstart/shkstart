<?php

/**
 * 
 *
 */
class paykeeper2Manager {
    
    /**
     * A reference to the modX instance
     * @var modX $modx
     */
    public $modx;
    
    /**
     * A configuration array
     */
    public $config = array();
    
    /**
     * Paykeeper constructor
     * 
     * @param modX &$modx
     * @param array $config
     */
    function __construct(modX &$modx,array $config = array()) {
        
        $this->modx =& $modx;
        
        $basePath = $this->modx->getOption('paykeeper2.core_path', $config, $this->modx->getOption('core_path').'components/paykeeper2/');
        $assetsUrl = $this->modx->getOption('paykeeper2.assets_url', $config, $this->modx->getOption('assets_url').'components/paykeeper2/');
        $this->config = array_merge(array(
            'basePath' => $basePath,
            'corePath' => $basePath,
            'modelPath' => $basePath . 'model/',
            'processorsPath' => $basePath . 'processors/',
            'templatesPath' => $basePath . 'templates/',
            'chunksPath' => $basePath . 'elements/chunks/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
            'lang' => $this->modx->getOption('manager_language', null, 'ru'),
        ),$config);
        
        $this->modx->addPackage('paykeeper2', $this->config['corePath'].'model/');
        $this->modx->getService('lexicon','modLexicon');
        $this->modx->lexicon->load($this->config['lang'].':paykeeper2:manager');
        
    }

    
}
