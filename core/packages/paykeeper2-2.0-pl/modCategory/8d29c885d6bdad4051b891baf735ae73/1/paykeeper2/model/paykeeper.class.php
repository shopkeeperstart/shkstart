<?php

/**
 * Paykeeper for MODX Revo
 *
 * E-store for MODX Revo
 *
 * @author Andchir <andchir@gmail.com>
 * @package paykeeper2
 * @version 2.0
 */

use Omnipay\Omnipay;

class Paykeeper {
    
    /**
     * A reference to the modX instance
     * @var modX $modx
     */
    public $modx;
    
    /**
     * A configuration array
     */
    public $config = array();
    
    /**
     * Payment gateway instance
     */
    public $gateway;
    
    /**
     * Messages
     */
    private $message = array();
    
    /**
     * Paykeeper constructor
     * 
     * @param modX &$modx
     * @param array $config
     */
    function __construct(modX &$modx, $config = array())
    {
        $this->modx =& $modx;
		$this->config = array_merge(array(
            'corePath' => $this->modx->getOption('core_path') . 'components/paykeeper2/',
            'gateway' => '',
            'action' => 'payment',//payment, download_form
            'product' => '',
            'productName' => '',
            'productId' => '',
            'orderId' => '',
            'price' => 0,
            'currency' => 'RUB',
            'method' => '',
            'lang' => $this->modx->getOption('manager_language', null, 'ru'),
            'quickMode' => true,
            'autoLink' => true,
            'testMode' => false,
            'printParameters' => false,
            'tpl' => 'pk_form',
            'chunkEmailPurchases' => 'pk_chunkEmailPurchases',
            'chunkEmailNotification' => 'pk_chunkEmailNotification',
            'chunkDownloadsItem' => 'pk_chunkDownloadsItem',
            'chunkDownloadsOuter' => 'pk_chunkDownloadsOuter',
            'shop_className' => 'shk_order',
	    'shop_packageName' => 'shopkeeper3'
        ),$config);
        
        $this->modx->addPackage('paykeeper2', $this->config['corePath'].'model/');
        $this->modx->getService('lexicon','modLexicon');
        $this->modx->lexicon->load($this->config['lang'].':paykeeper2:default');
        
        $this->config['price'] = str_replace( ',', '.', $this->config['price'] );
        $this->config['debug'] = $this->modx->getOption('paykeeper2.debug', null, false);
        
        //Set instance index
        $this->modx->placeholders['pkIndex'] = isset($this->modx->placeholders['pkIndex'])
            ? $this->modx->placeholders['pkIndex']+1
            : 1;
        $pkIndex = $this->modx->placeholders['pkIndex'];
        $this->config['sid'] = $pkIndex;
        
        require_once $this->config['corePath'] . 'vendor/autoload.php';
			
		//prepare gateway
		if(!empty( $_POST['method'] ) && strpos($_POST['method'], '__') !== false){
			$temp_arr = explode( '__', $_POST['method'] );
			$this->config['gateway'] = trim($temp_arr[0]);
		}
		if( empty( $this->config['currency'] ) && !empty( $_POST['currency'] ) ){
			$this->config['currency'] = trim( (string) $_POST['currency'] );
		}
        if( $this->config['gateway'] ){
			$namespace = $this->getGatewayOption('namespace');
			if( $namespace  ){
				$this->gateway = Omnipay::create( $namespace );
			}
        }
    }
    
    /**
     * Validate E-mail
     *
     * @param string $email
     */
    public static function validateEmail( $email )
    {
        $regexp = '/^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/';
        return preg_match($regexp, $email) ? true : false;
    }
    
    /**
     * Start payment
     *
     * @param string $user_email
     */
    public function startPayment( $user_email = '' )
    {
		if( empty( $this->gateway ) ){
			$this->logging( 'Gateway "' . $this->config['gateway'] . '" not found.', 'ERROR' );
			return false;
		}
        
        if( !$this->config['productId'] && !$this->config['orderId'] ){
            $this->config['productId'] = $this->getItemId();
        } else if( $this->config['productId'] ) {
            $product = $this->getItem( $this->config['productId'] );
            if( $product === false ){
                return false;
            }
            $this->config['price'] = $product['price'];
            $this->config['productName'] = $product['name'];
        } else if( $this->config['orderId'] && is_numeric( $this->config['orderId'] ) ){
			$this->config['price'] = $this->getShkOrderPrice( $this->config['orderId'] );
		}
        
        $paymentId = $this->createPayment( $user_email );
        
        $gatewayOptions = $this->getGatewayOptions( $user_email, $paymentId );
        
        $this->setGatewayParameters( $gatewayOptions );
        
        if( $this->config['testMode'] ){
            $this->gateway->setTestMode(true);
        }
        
        $purchase = $this->gateway->purchase(array(
            'amount' => self::toDecimal( $this->config['price'] ),
            'currency' => $this->config['currency']
        ));
		
		$purchase->setTransactionId( $paymentId );
		$purchase->setDescription( $this->getDescription() );
		$purchase->setReturnUrl( $this->getGatewayOption('returnUrl') );
		$purchase->setCancelUrl( $this->getGatewayOption('cancelUrl') );
		$purchase->setNotifyUrl( $this->getGatewayOption('notifyUrl') );
		
		$response = $purchase->send();
        
        if( $this->config['debug'] ) {
            $this->logging( array_merge( $gatewayOptions, array( 'amount' => self::toDecimal( $this->config['price'] ), 'currency' => $this->config['currency'] ) ) );
        }
		
		//save user data to session
		$sessionData = array(
			'transactionId' => $paymentId,
			'email' => $user_email,
			'amount' => $purchase->getAmount(),
			'currency' => $this->config['currency']
		);
		$this->sessionSet( 'paymentData', $sessionData );
	
        if ($response->isSuccessful()) {
            $message = $response;
        } elseif ($response->isRedirect()) {
            $response->redirect();
        } else {
            $message = $response->getMessage();
        }
        
        if( !empty( $message ) && $this->config['debug'] ) {
            $this->logging( $message );
        }
        
        return true;
    }
    
    /**
     * Get product ID
     *
     */
    public function getItemId()
    {
        
        if( $this->config['autoLink'] ){
            $this->config['product'] = $this->autoLink( $this->config['product'] );
        }
        
        $c = $this->modx->newQuery('pkItems');
		$c->where(array(
			'name' => $this->config['productName'],
			'content' => $this->config['product'],
			'state' => 'available'
		));
        
        $c->where(array("CAST(`pkItems`.`price` AS DECIMAL) = CAST(" . $this->config['price'] . " AS DECIMAL)"));
        
        $product = $this->modx->getObject('pkItems', $c);
        
        if( !empty( $product ) ){
            $itemId = $product->get('id');
        }
        else {
            $itemId = $this->addItem();
        }
        
        return $itemId;
    }
    
    /**
     * Create payment
     *
     * @param string $user_email
     * @return int
     */
    public function createPayment( $user_email )
    {
        
        //Удаляем старые неоплаченные счета
        $query = "`email` = '{$user_email}' AND `state` = 'initial'";
        $limitTime = $this->modx->getOption('paykeeper2.payLimitTime');
        if( $limitTime ){
            //$query .= ' AND `datetime` < NOW() - INTERVAL ' . $this->modx->config['paykeeper2.payLimitTime'] . ' MINUTE';
        }
		$this->modx->removeCollection('pkPayments', $query);
        
        $paymentArr = array(
            'value' => $this->config['price'],
            'userid' => $this->modx->getLoginUserID() ? $this->modx->getLoginUserID() : 0,
            'email' => $user_email,
            'datetime' => strftime('%Y-%m-%d %H:%M:%S'),
            'state' => 'initial'
        );
        if($this->config['productId']){
            $paymentArr['itemid'] = $this->config['productId'];
        }
        if($this->config['orderId']){
            $paymentArr['orderid'] = $this->config['orderId'];
        }
        $payment = $this->modx->newObject('pkPayments');
        $payment->fromArray( $paymentArr );
        $payment->save();
        
		return $payment->get('id');
    }
    
    /**
     * Set payment state
     *
     * @param int $paymentId
     * @param string $stateName
     * @return boolean
     */
    public function setPaymentState( $paymentId, $stateName )
    {
        
        $payment = $this->modx->getObject('pkPayments', $paymentId);
        
        if( $payment ){
            $payment->set( 'state', $stateName );
            $payment->save();
            return true;
        }
        
        return false;
    }
    
    /**
     * Get payment ID, validate payment
     *
     * @param float $orderSumAmount
     * @param string $userEmail
     * @param string $stateName
     * @return int
     */
    public function getPaymentId( $orderSumAmount, $userEmail = '', $stateName = 'initial', $requestPaymentId = null )
    {
        
        $paymentId = 0;
        
        $c = $this->modx->newQuery('pkPayments');
        $c->where( array( 'state' => $stateName ) );
		if( $userEmail ){
			$c->where( array( 'email' => $userEmail ) );
		}
		if( $requestPaymentId ){
			$c->where( array( 'id' => $requestPaymentId ) );
		}
		$c->where("CAST(`pkPayments`.`value` AS DECIMAL) = CAST(" . str_replace(',', '.', strval($orderSumAmount)) . " AS DECIMAL)");
        
        //Set time limit
        $limitTime = $this->modx->getOption('paykeeper2.payLimitTime');
        if( $limitTime ){
            $c->where(array(
                "`pkPayments`.`datetime` BETWEEN NOW() - INTERVAL " . $limitTime . " MINUTE AND NOW()"
            ));
        }
        $c->sortby('pkPayments.datetime', 'DESC');
        
        $payments = $this->modx->getCollection('pkPayments', $c);
        
        if( $payments ){
            $payment = current( $payments );
            $paymentId = $payment->id;
            
            //Check item state
            if( !empty( $payment->itemid ) ){
                $countItems = $this->modx->getCount('pkItems', array(
                    'id' => $payment->itemid,
                    'state' => 'available'
                ));
                if($countItems == 0){
                    $paymentId = 0;
                }
            }
        }
        
        if( $this->config['debug'] ){
            $this->logging( $c->toSQL() . PHP_EOL . "Payment ID: {$paymentId}, Total: " . count( $payments ) );
        }
        
        return $paymentId;
    }
    
    /**
     * Add new product
     */
    public function addItem()
    {
        
        $item = $this->modx->newObject('pkItems');
        $item->fromArray(array(
            'name' => $this->config['productName'],
            'content' => $this->config['product'],
            'price' => $this->config['price'],
            'state' => 'available'
        ));
        $item->save();
        $itemId = $item->get('id');
        
        return $itemId;
    }
    
    /**
     * Get product data
     *
     * @param integer $itemId
     * @return array
     */
    public function getItem( $itemId )
    {
        $product = $this->modx->getObject('pkItems', $itemId);
        if(!$product){
            return false;
        }
        return $product->toArray();
    }
    
    /**
     * Get payment data
     *
     * @param integer $paymentId
     * @return array
     */
    public function getPayment( $paymentId )
    {
        $payment = $this->modx->getObject('pkPayments', $paymentId);
        if(!$payment){
            return false;
        }
        return $payment->toArray();
    }
    
    /**
     * Get order data
     *
     * @param integer $paymentId
     * @return array $output
     */
    public function getOrderData( $paymentId )
    {
        $output = array();
        $payment = $this->modx->getObject( 'pkPayments', $paymentId );
        if( !$payment ){
            if( $this->config['debug'] ){
                $this->logging( "Payment ID={$paymentId} not found." );
            }
            return $output;
        }
        $output = $payment->toArray();
        if( $payment->itemid ){
            $productData = $this->getItem( $payment->itemid );
            if( !empty( $productData ) ){
                $output['productName'] = $productData['name'];
                $output['productContent'] = $productData['content'];
            }
        }
		if(!empty($output['orderid'])){
			$this->config['orderId'] = (int) $output['orderid'];
		}
		if(!empty($output['itemid'])){
			$this->config['productId'] = (int) $output['itemid'];
		}
		if(!empty($output['productName'])){
			$this->config['productName'] = $output['productName'];
		}
        return $output;
    }
    
    /**
     * Get gateway options
     *
     */
    public function getGatewayOptions( $user_email = '', $paymentId = 0 )
    {
        $options = array();
        
        $name = $this->gateway->getName();
        $parameters = $this->gateway->getDefaultParameters();
        
        foreach( $parameters as $paramName => $value ){
            
            switch($paramName){
                case 'returnUrl':
                case 'cancelUrl':
                    
                    $site_url = $this->modx->getOption( 'site_url' );
                    $options[ $paramName ] = str_replace( '{site_url}', $site_url, $this->getGatewayOption( $paramName ) );
                    
                    break;
                case 'method':
                    
                    $method = !empty( $_POST['method'] ) ? (string) trim( $_POST['method'] ) : '';
					if( strpos( $method, '__' ) !== false ){
						$temp_arr = explode( '__', $_POST['method'] );
						$method = $temp_arr[1];
					}
                    if( $method ){
                        $options[ $paramName ] = $method;
                    } else {
                        $options[ $paramName ] = $this->getGatewayOption( $paramName, $this->config['method'] );
                    }
                    
                    break;
                case 'orderId':
                case 'orderNumber':
                    
                    if( $paymentId ){
                        $options[ $paramName ] = $paymentId;
                    }
                    
                    break;
                case 'customerNumber':
                    
                    $options[ $paramName ] = $user_email;
                    
                    break;
                default:
                    
                    $options[ $paramName ] = $this->getGatewayOption( $paramName );
                    
                    break;
            }
            
        }
        
        return $options;
    }
    
    /**
     * Get gateway option
     *
     */
    public function getGatewayOption( $paramName, $default = '' )
    {
		$name = $this->config['gateway'];
		$site_url = $this->modx->getOption( 'site_url' );
		$output = $this->modx->getOption( "paykeeper2.{$name}.{$paramName}", null, $default );
		return str_replace( '{site_url}', $site_url, $output );
    }
    
    /**
     * Get gateway parameters
     *
     */
    public function getGatewayParameters()
    {
		if( empty( $this->gateway ) ){
			$this->logging( 'Gateway "' . $this->config['gateway'] . '" not found.', 'ERROR' );
			return array();
		}
        
        $parameters = $this->gateway->getDefaultParameters();
        $name = $this->gateway->getName();
        
        $output = array();
        $output[ $name ] = $parameters;
        return $output;
        
    }
    
    /**
     * Set gateway parameters
     *
     * @param array $parameters
     */
    public function setGatewayParameters( $parameters )
    {
        
        if( !empty( $parameters ) ){
            
            foreach( $parameters as $paramName => $value ){
                
                $methodName = 'set' . $paramName;
                
                if( !empty( $value ) && method_exists( $this->gateway, $methodName ) ){//case-insensitive
                    call_user_func( array( $this->gateway, $methodName ), $value );
                }
                
            }
            
        }
        
    }
    
    /**
     * Create Download secret code and send from e-mail
     *
     * @param string $user_email
     * $param boolean $send_email
     */
    public function createDownloadCode( $user_email, $send_email = true )
    {
        
        $c = $this->modx->newQuery('pkPayments');
		$c->where(array(
				'email' => $user_email,
			'state' => 'success'
		));
        
        if( $this->modx->getCount( 'pkPayments', $c ) === 0 ){
            $this->setMessage( $this->modx->lexicon('paykeeper2.purchases_not_found'), true );
            return false;
        }
        else {
            
            $download = $this->modx->getObject('pkDownloads', array(
                'email' => $user_email
            ));
            
            if( !$download ){
                
                $download = $this->modx->newObject('pkDownloads');
                $download->fromArray(array(
                    'email' => $user_email,
                    'datetime' => strftime('%Y-%m-%d %H:%M:%S')
                ));
                
            }
            
            $secret_code = time() . uniqid();
            $download->set( 'secret_code', $secret_code );
            $download->set( 'last_activity', strftime('%Y-%m-%d %H:%M:%S') );
            
            $download->save();
            
            $resourceId = is_object( $this->modx->resource ) ? $this->modx->resource->id : 1;
            $downloadPageId = $this->modx->getOption('paykeeper2.downloadPageId', null, $resourceId);
            
            //send e-mail
            if( $send_email ){
                $chunk = $this->config['chunkEmailPurchases'];
                $chunkArr = array(
                    'user_email' => $user_email,
                    'page_url' => $this->modx->makeUrl( $downloadPageId, '', '', 'full' ),
                    'secret_code' => $secret_code
                );
                $emailBody = $this->modx->parseChunk($chunk, $chunkArr);
                
                $this->sendMail( $this->modx->lexicon('paykeeper2.your_purchases'), $emailBody, $user_email );
                
                $this->setMessage( $this->modx->lexicon('paykeeper2.email_sent'), false );
            }
            
            return $secret_code;
        }
        
    }
	
	/**
	 * Get SHK order price
	 *
	 * @param int $orderId
	 * @return float
	 */
	public function getShkOrderPrice( $orderId ){
		$output = 0;
		
		$modelpath = $this->modx->getOption('core_path') . 'components/shopkeeper3/model/';
        $this->modx->addPackage( 'shopkeeper3', $modelpath );
		
		$order = $this->modx->getObject('shk_order', $orderId);
		
		if( $order ){
			$output = $order->price;
		}
		
		return $output;
	}
    
    /**
     * Downloads list output
     *
     * @param string $secret_code
     * @return string
     */
    public function downloadsListOutput( $secret_code )
    {
        $output = '';
        
        $download = $this->getDownloadData( $secret_code );
        
        if( empty( $download ) ){
            return '';
        }
        
        $user_email = $download['email'];
        
        $c = $this->modx->newQuery('pkItems');
        $c->select('pkItems.*, pkPayments.datetime');
        $c->leftJoin('pkPayments', 'pkPayments', 'pkItems.id = pkPayments.itemid');
        $c->where(array(
            'pkPayments.email' => $user_email,
            'pkPayments.state' => 'success'
        ));
        $c->sortby('pkPayments.datetime','DESC');
        
        $collection = $this->modx->getCollection('pkItems', $c);
        
        if( $this->config['debug'] ){
            $this->logging( $c->toSQL() );
        }
        
        if( $collection ){
            
            $inner = '';
            
            foreach ($collection as $obj) {
                
                $dateformat = $this->modx->getOption('paykeeper2.dateformat', null, 'd/m/Y H:i');
                
                $chunkArr = array(
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'link' => $this->autoLink( $obj->content ),
                    'download_link' => $this->makeDownloadUrl( $obj->content,  $obj->id, $secret_code ),
                    'content' => $obj->content,
                    'datetime' => date( $dateformat, strtotime( $obj->datetime ) )
                );
                $inner .= PHP_EOL . $this->modx->parseChunk( $this->config['chunkDownloadsItem'], $chunkArr );
                
            }
            
            $chunkArr = array(
                'inner' => $inner,
                'user_email' => $user_email
            );
            
            $output = $this->modx->parseChunk( $this->config['chunkDownloadsOuter'], $chunkArr );
            
        }
        
        return $output;
    }
    
    /**
     * Send e-mail
     * 
     * @param string $subject
     * @param string $body
     * @param string $to
     * @param string $from
     * @return boolean
     */
    public function sendMail($subject, $body, $to, $from = '')
    {
		if(empty($to)) return false;
		
		if( !$from ){
			$from = $this->modx->getOption('emailsender');
		}
		$this->modx->getService('mail', 'mail.modPHPMailer');
		$this->modx->mail->set(modMail::MAIL_BODY, $body);
		$this->modx->mail->set(modMail::MAIL_FROM, $from);
		$this->modx->mail->set(modMail::MAIL_FROM_NAME, $this->modx->config['site_name']);
		$this->modx->mail->set(modMail::MAIL_SENDER, $this->modx->config['emailsender']);
		$this->modx->mail->set(modMail::MAIL_SUBJECT, $subject);
		$this->modx->mail->address('to', $to);
		$this->modx->mail->setHTML(true);
		if (!$this->modx->mail->send()) {
			$this->modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$this->modx->mail->mailer->ErrorInfo);
		}
		$this->modx->mail->reset();
		
		return true;
    }
    
    /**
     * Send notification to admin
     *
     * @param integer $paymentId
     */
    public function sendAdminNotification( $paymentId )
    {
        
        $orderData = $this->getOrderData( $paymentId );
        if(empty( $orderData )){
            return;
        }
        
        $chunk = $this->config['chunkEmailNotification'];
        $chunkArr = array(
            'user_email' => $orderData['email'],
            'productName' => $this->getDescription(),
            'price' => str_replace( ',', '.', $orderData['value'] ),
			'gateway' => $this->config['gateway']
        );
        $emailBody = $this->modx->parseChunk($chunk, $chunkArr);
        
        $adminEmail = $this->modx->getOption('emailsender');
        $this->sendMail( $this->modx->lexicon('paykeeper2.new_purchase'), $emailBody, $adminEmail );
    }
    
    /**
     * Get payment descripton
     *
     */
    public function getDescription(){
		return !empty( $this->config['productName'] )
			? $this->config['productName']
			: $this->modx->lexicon('paykeeper2.order_number', array('orderId' => $this->config['orderId']));
    }
    
    /**
     * Get download data
     *
     * @param string $secret_code
     * @return string
     */
    public function getDownloadData( $secret_code )
    {
        $output = array();
        
        $c = $this->modx->newQuery('pkDownloads');
        $c->where(array(
            'secret_code' => $secret_code
        ));
		
		$timeLimit = $this->modx->getOption('paykeeper2.downloadLimitTime');
		if( $timeLimit ){
			$c->where(array(
			'`pkDownloads`.`last_activity` BETWEEN DATE_SUB(NOW(), INTERVAL '
				. $this->modx->getOption('paykeeper2.downloadLimitTime')
				. ' MINUTE) AND NOW()'
			));
		}
        
        $download = $this->modx->getObject('pkDownloads', $c);
        
        if( $download ){
            $output = $download->toArray();
        }
        if( $this->config['debug'] ){
            $this->logging( $c->toSQL() );
        }
        
        return $output;
    }
    
    /**
     * Get Download link
     *
     * @param string $url
     * @param integer $item_id
     * @param string $secret_code
     * @return string
     */
    public function makeDownloadUrl( $url, $item_id = 0, $secret_code )
    {
        $output = '';
        
		if( defined( 'MODX_URL_SCHEME' ) ){
			$url = str_replace( 'http://', MODX_URL_SCHEME, $url );
		}
        $url = str_replace( $this->modx->getOption('site_url'), '', $url );
	
        $product_path = $this->modx->getOption('base_path') . $url;
        if( file_exists( $product_path ) ){
            
            $resourceId = is_object( $this->modx->resource ) ? $this->modx->resource->id : 0;
            $pageId = $this->modx->getOption( 'paykeeper2.downloadPageId', null, $resourceId );
            
            if( $resourceId ){
                $params = array(
                    'download' => $secret_code,
                    'item' => $item_id
                );
                $output = $this->modx->makeUrl( $pageId, '', $params, 'full' );
            }
            
        }
        
        return $output;
    }
    
    /**
     * Download item
     * 
     * @param integer $item_id
     * @param string $secret_code
     */
    public function downloadItem( $item_id, $secret_code = '', $user_email = '' )
    {
        
        if( !$user_email && $secret_code ){
            
            $download_data = $this->getDownloadData( $secret_code );
            if( !empty( $download_data ) ){
                $user_email = $download_data['email'];
            }
            
        }
        
        if( !$user_email ) return false;
        
        $c = $this->modx->newQuery('pkItems');
        $c->select('pkItems.*, pkPayments.datetime');
        $c->leftJoin('pkPayments', 'pkPayments', 'pkItems.id = pkPayments.itemid');
        $c->where(array(
            'pkItems.id' => $item_id,
            'pkPayments.email' => $user_email,
            'pkPayments.state' => 'success',
        ));
        
        $download = $this->modx->getObject( 'pkItems', $c );
        
        if( $download ){
				
			$url = $download->content;
			if( defined( 'MODX_URL_SCHEME' ) ){
				$url = str_replace( 'http://', MODX_URL_SCHEME, $url );
			}
			$url = str_replace( $this->modx->getOption('site_url'), '', $url );
	    
            $product_path = $this->modx->getOption('base_path') . $url;
            if( file_exists( $product_path ) ){
                
                self::downloadFile( $product_path );
                
            } else {
				$this->setMessage( $this->modx->lexicon('paykeeper2.file_not_found'), true );
			}
        }
        
    }
    
    /**
     * downloadFile
     *
     * @param string $file_path
     * @param string $file_name
     */
    public static function downloadFile( $file_path, $file_name = '' )
    {
        
        if( !file_exists( $file_path ) ) {
            header('HTTP/1.0 404 Not Found');
            echo 'Not Found';
            exit;
        }
        
        $ext = self::getFileExtension( $file_path );
        
        if( !$file_name ){
            $file_name = basename( $file_path );
        }
        if( strpos( $file_name, $ext ) === false ){
            $file_name .= '.' . $ext;
        }
        $file_size = filesize( $file_path );
        
        if (isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
            header('Content-Type: application/force-download');
        else
            header('Content-Type: application/octet-stream');
        
        header('Accept-Ranges: bytes');
        header('Content-Length: ' . $file_size);
        header('Content-disposition: attachment; filename="' . $file_name . '"');
        
        ob_clean();
        flush();
        readfile($file_path);
        
        exit;
        
    }
    
    /**
     * getFileExtension
     *
     * @param string $file_name
     */
    public static function getFileExtension( $file_name )
    {
        $temp_arr = explode( '.', $file_name );
        $ext = end( $temp_arr );
        return strtolower( $ext );
    }
    
    /**
     * String to link
     *
     * @param string $url
     * @return string
     */
    public function autoLink( $url, $returnInput = true )
    {
        $product_path = $this->modx->getOption('base_path') . $url;
        if( file_exists( $product_path ) ){
            $url = $this->modx->getOption('site_url') . $url;
        } else if( !$returnInput ) {
            return '';
        }
        return $url;
    }
    
    /**
     * Get message
     * @return array $message
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     *
     * @param string $msg
     * @param boolean $is_error
     */
    public function setMessage( $msg, $is_error = false )
    {
        
        $this->message = array(
            'msg' => $msg,
            'is_error' => $is_error
        );
        
    }
    
    /**
     * Set form placeholders
     *
     * @return void
     */
    public function setFormPlaceholders()
    {
		
		$this->modx->setPlaceholders(
			array(
			'price' => $this->config['price'],
			'productName' => $this->config['productName']
			),
			'paykeeper2.'
		);
		
    }
    
    /**
     * Get payment form
     *
     * @param string $user_email
     * @return string $output
     */
    public function getFormOutput( $user_email )
    {
		if( $this->config['productId'] ){
			$product = $this->getItem( $this->config['productId'] );
			if( $product !== false ){
			$this->config['price'] = $product['price'];
			$this->config['productName'] = $product['name'];
			}
		}
		
		$this->setFormPlaceholders();
		
		$chunkArr = array(
			'sid' => $this->config['sid'],
			'user_email' => $user_email
		);
		
		$output = $this->modx->parseChunk( $this->config['tpl'], $chunkArr );
		return $output;
    }
    
    /**
     * Set order status paid
     *
     * @param integer $order_id
     */
    public function shkOrderPaid( $order_id ){
		
		if($order_id){
			
			$this->modx->addPackage( $this->config['shop_packageName'], MODX_CORE_PATH."components/" . $this->config['shop_packageName'] . "/model/" );
			
			$order = $this->modx->getObject( $this->config['shop_className'], $order_id );
			if($order){
				
				$order->set('status', $this->modx->config['paykeeper2.payStatusOut']);
				$order->save();
				
				return true;
			}
			
		}
		
		return false;
    }
    
    /**
     * Complete purchase
     *
     * @param int $paymentId
     */
    public function completePurchase( $paymentId )
    {
		$payment = $this->getPayment( $paymentId );
		if( $payment !== false ){
			
			$this->setPaymentState( $paymentId, 'success' );
			
			if( $payment['orderid'] ){
			
			//Переводим заказ в статус "Оплачен"
			$this->shkOrderPaid( $payment['orderid'] );
			
			} else {
			//Отправляем письмо со ссылкой на скачивание электронного товара
			$this->createDownloadCode( $payment['email'] );
			}
			
		}
		$this->sendAdminNotification( $paymentId );
    }
    
    /**
     * Save data to session
     * 
     * @param string $key
     * @param array $data
     */
    public function sessionSet( $key, $data )
    {
		$_SESSION[ $key ] = $data;
    }
    
    /**
     * Redirect to new URL
     *
     * @param string $url
     */
    public function redirect( $url )
    {
		header( 'Location: ' . $url );
		exit;
    }
    
    /**
     * Get session data
     * 
     * @param string $key
     */
    public function sessionGet( $key, $subKey = '' )
    {
		if( $subKey ){
			return isset( $_SESSION[ $key ] ) && isset( $_SESSION[ $key ][ $subKey ] )
			? $_SESSION[ $key ][ $subKey ]
			: '';
		}
		return isset( $_SESSION[ $key ] ) ? $_SESSION[ $key ] : array();
    }
    
    /**
     * Message to placeholder
     *
     */
    public function messageToPlaceholder()
    {
        
        if( !empty( $this->message ) ){
            $this->modx->setPlaceholders(
                array(
                    'msg' => $this->message['msg'],
                    'is_error' => $this->message['is_error'] ? 1 : 0
                ),
                'paykeeper2.'
            );
        }
        
    }
    
    /**
     * String to CamelCase
     *
     * @param string $string
     * @param boolean $capitalizeFirstCharacter
     */
    public static function toCamelCase( $string, $capitalizeFirstCharacter = false ) 
    {
        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
        
        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }
        
        return $str;
    }
    
    /**
     * Number to decimal
     *
     * @param string $string
     */
    public static function toDecimal( $number )
    {
        $number = number_format($number, 2, '.', '');
        return $number;
    }
    
    /**
     * Logging to MODX error (debug) log
     *
     * @param mixed $msg
     */
    public function logging( $msg, $title = '' )
    {
        
        $str = is_array($msg) ? print_r( $msg, true ) : (string) $msg;
	if( $title ) $title .= ': ';
        $this->modx->log( $this->modx->getOption('log_level'), '[Paykeeper INFO] ' . $title . $str );
        
    }
    
}
