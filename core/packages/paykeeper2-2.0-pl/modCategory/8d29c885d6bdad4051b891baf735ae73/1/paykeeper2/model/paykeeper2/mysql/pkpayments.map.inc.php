<?php
$xpdo_meta_map['pkPayments']= array (
  'package' => 'paykeeper2',
  'version' => NULL,
  'table' => 'paykeeper2_payments',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'itemid' => 0,
    'orderid' => 0,
    'value' => 0,
    'userid' => 0,
    'email' => '0',
    'datetime' => NULL,
    'state' => '0',
  ),
  'fieldMeta' => 
  array (
    'itemid' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'int',
      'null' => false,
      'default' => 0,
    ),
    'orderid' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'int',
      'null' => false,
      'default' => 0,
    ),
    'value' => 
    array (
      'dbtype' => 'float',
      'precision' => '11',
      'phptype' => 'float',
      'null' => false,
      'default' => 0,
    ),
    'userid' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'int',
      'null' => false,
      'default' => 0,
    ),
    'email' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '64',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
    'datetime' => 
    array (
      'dbtype' => 'datetime',
      'phptype' => 'string',
      'null' => false,
    ),
    'state' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '11',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
  ),
  'aggregates' => 
  array (
    'pkItems' => 
    array (
      'class' => 'pkItems',
      'local' => 'itemid',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
