
Сниппет "Онлайн оплата" - Paykeeper2

http://modx-shopkeeper.ru/ - Бесплатный компонент для MODX Revolution для создания интернет-магазина.
http://e-store.modx-shopkeeper.ru/ - Электронные товары.

--------------------------------------------------

Paykeeper 2

Сниппет для продажи электронных товаров, а так же оплаты заказов в интернет-магазине на основе Shopkeeper 3.x.
Компонент для управления электронными товарами (Фронт-енд написан на Angular2 + TypeScript).
Используется библиотека Omnipay. Эта библиотека позволяет делать единый API для разных платежных систем.
Сайт проекта: http://omnipay.thephpleague.com/

В состав входят готовые обработчики для платежных систем:
Yandex.Kassa (агрегатор), WebMoney, PayPal.

Есть инструкция по подключению других платежных систем с помощью компонентов Omnipay.
Требуются базовые (начальные) знания PHP.

Агрегатор Яндекс.Касса имеет следующие методы оплаты:

Банковские карты - Visa (включая Electron), MasterCard и Maestro.
Яндекс.Деньги - От 25 млн пользователей электронных кошельков.
WebMoney - Из рублевых WM-кошельков.
QIWI Wallet - На сайте QIWI и через приложение QIWI Wallet.
Наличные - Через терминалы, банкоматы и салоны связи — это больше 250 000 пунктов в России и других странах.
Платежи через интернет-банки - Через «Альфа-клик», интернет-банк «Промсвязьбанка», Сбербанк Онлайн и MasterPass — всего около 30 млн пользователей.
Платежи со счета мобильного - От абонентов Билайна, МегаФона, МТС.
Платежи через кредитные сервисы - «КупиВкредит» («Тинькофф Банк»), доверительный платеж («Куппи.ру»).

--------------------------------------------------

Техническая поддержка:

http://modx-shopkeeper.ru/forum/

Если на форуме долго нет ответа, можно писать на почту автору с помощью форм:
http://modx-shopkeeper.ru/contacts/
или
http://e-store.modx-shopkeeper.ru/kontaktyi.html

--------------------------------------------------

Параметры сниппета Paykeeper2:

gateway - Название платежной системы (или агрегатора). Пример: &gateway=`YandexMoney`
action - Действие. payment - оплата, download_form - скачивание эл. товаров.
product - Ссылка на товар или информация, которая продается. Указывается вместо productId.
productName - Название товара.
productId - ID товара (не обязательно). Если не указано, создается новый ID для товара, указанного в параметре "product".
orderId - ID заказа в магазине. Используется для оплаты заказов в магазине, созданном на базе Shopkeeper 3.x.
price - Цена товара. Если указывается productId, то цену указывать не нужно.
currency - Валюта. По умолчанию: RUB
method - Метод оплаты (не обязательно).
lang - Язык. По умолчанию: ru
autoLink - Автоматически переводить значение параметра "product" в полную ссылку. По умолчанию: true
testMode - Тестовый режим оплаты. По умолчанию: false
printParameters - Показать список обязательный поле оплаты. По умолчанию: false
tpl - Чанк шаблона формы оплаты. По умолчанию: pk_form
chunkEmailPurchases - Чанк шаблона письма со ссылкой на скачивание эл. товаров. По умолчанию: pk_chunkEmailPurchases
chunkDownloadsItem - Чанк одного товара в списке. По умолчанию: pk_chunkDownloadsItem
chunkDownloadsOuter - Чанк обертки для списка товаров. По умолчанию: pk_chunkDownloadsOuter
shop_className - Имя класса таблицы заказов магазина. По умолчанию: shopkeeper3
shop_packageName - Имя пакета таблицы заказов магазина. По умолчанию: shk_order

Справка:
Вместо значения "true" нужно указывать "1" (еденица без кавычек).
Вместо значения "false" нужно указывать "0" (ноль без кавычек).

--------------------------------------------------

Примеры

Страница электронного товара

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

--------------------------------------------------

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&productId=`[[*product_id]]`
]]

--------------------------------------------------

Оплата заказа в интернет магазине на базе Shopkeeper 3.x:

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&orderId=`[[!+shk.id]]`
]]

--------------------------------------------------

Страница "Мои покупки"

[[!Paykeeper2?
&action=`download_form`
&tpl=`pk_downloadForm`
&toPlaceholder=`pkForm`
]]

[[!+paykeeper2.msg:wrap=`<div class="alert [[!+paykeeper2.is_error:eq=`1`:then=`alert-danger`:else=`alert-success`]]">|</div>`]]

[[!+pkForm]]

--------------------------------------------------

Параметр &gateway можно устанавливать динамически. Дабавьте в форму селект с выбором метода оплаты.

Пример:

select id="pkInputMethod[[+sid]]" name="method" class="form-control">
    <option value="YandexMoney__PC">Оплата из кошелька в Яндекс.Деньгах</option>
    <option value="YandexMoney__AC">Оплата с произвольной банковской карты</option>
    <option value="YandexMoney__MC">Платеж со счета мобильного телефона</option>
    <option value="YandexMoney__GP">Оплата наличными через кассы и терминалы</option>
    <option value="YandexMoney__WM">Оплата из кошелька в системе WebMoney</option>
    <option value="YandexMoney__SB">Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн</option>
    <option value="YandexMoney__MP">Оплата через мобильный терминал (mPOS)</option>
    <option value="YandexMoney__AB">Оплата через Альфа-Клик</option>
    <option value="YandexMoney__МА">Оплата через MasterPass</option>
    <option value="YandexMoney__PB">Оплата через Промсвязьбанк</option>
    <option value="YandexMoney__QW">Оплата через QIWI Wallet</option>
    <option value="YandexMoney__KV">Оплата через КупиВкредит (Тинькофф Банк)</option>
    
    <option value="WebMoney__R">WebMoney</option>
</select>

В данном значение до разделителя "__" это название платежной системы, а то что после - метод оплаты.
Если в значении нет разделителя "__", то используется платежная система, которая указана в параметре &gateway.

--------------------------------------------------
--------------------------------------------------

Плейсхолдеры на странице вызова сниппета

На странице где вызывается сниппет Paykeeper2 доступны плейсхолдеры:

[[!+paykeeper2.msq]] - Текст сообщения.
[[!+paykeeper2.is_error]] - Сообщение - ошибка (1) или нет (0).
[[!+paykeeper2.price]] - Цена товара.
[[!+paykeeper2.productName]] - Название товара.

"!" - значит, что значение плейсхолдера не кэшируется.

Пример использования:

[[!+paykeeper2.msg:wrap=`<div class="alert [[!+paykeeper2.is_error:eq=`1`:then=`alert-danger`:else=`alert-success`]]">|</div>`]]

Код сниппета "wrap" можно взять в файле:
/core/components/paykeeper2/elements/snippets/snippet.wrap.php
или здесь:
https://modx.today/posts/2015/11/the-story-of-my-super-snippet

--------------------------------------------------
--------------------------------------------------

Яндекс.Касса

https://kassa.yandex.ru/

Зарегистрироваться на сайте системы Яндекс.Касса.

Для сheckURL и paymentAvisoURL указать:
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result.php

successURL и failURL - динамические

В админке сайта перейти в "Настройки системы" -> "paykeeper2" (пространство имен в фильтре).
Заполнить все необходимые поля (есть комментарии).

Включить тестовый режим (см. параметры сниппета) и протестировать оплату.

Справка:
Для подключения сайта к системе "Яндекс.Касса" нужен SSL сертификат (https).

Тестовые данные
https://tech.yandex.ru/money/doc/payment-solution/examples/examples-test-data-docpage/

Документация
https://money.yandex.ru/doc.xml?id=527069

Примеры на PHP
https://github.com/YandexMoney/yandexmoney/tree/master/Yandex.Kassa/example%20integration/php

Пример вызова сниппета:

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

--------------------------------------------------

WebMoney

https://wiki.webmoney.ru/projects/webmoney/wiki/Merchant_WebMoney_Transfer

Описание интерфейса сервиса Web Merchant Interface:
https://merchant.webmoney.ru/conf/guide.asp

Result URL
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result_wm.php

[[!Paykeeper2?
&gateway=`WebMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

notifyUrl - URL, на которы платежная система будет отправлять запросы для проведения платежа (Result URL в настройках WebMoney Merchant).
Должен быть таким:
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result_wm.php

В админке сайта перейти в "Настройки системы" -> "paykeeper2" (пространство имен в фильтре).
Заполнить все необходимые поля (есть комментарии).

Cкриншоты настроек платежной системы:
/core/components/paykeeper2/docs/screenshots/wm_001.png
/core/components/paykeeper2/docs/screenshots/wm_002.png

--------------------------------------------------

Подключение платежных систем с помощью компонентов OmniPay

Сайт проекта:
http://omnipay.thephpleague.com/

Репозиторий:
https://github.com/thephpleague/omnipay

На примере PayPal

Документация платежной системы PayPal:
https://developer.paypal.com/webapps/developer/docs/classic/products/#pfg
https://developer.paypal.com/docs/classic/payflow/integration-guide/#configuring-hosted-pages-using-paypal-manager
https://developer.paypal.com/docs/classic/express-checkout/integration-guide/ECGettingStarted/#id0832BA0605Z

Список валют:
https://developer.paypal.com/docs/classic/api/currency_codes/#paypal

1. Устанавливаем composer:

cd /var/www/html/my_site/core/components/paykeeper2
curl -s http://getcomposer.org/installer | php
php composer.phar update

Установка в ОС Windows:
https://getcomposer.org/doc/00-intro.md#installation-windows

2. Загружаем код компонента:

composer require omnipay/paypal:~2.0

Код компонента будет загружен в папку vendor.

3. В админке переходим в "Системные настройки" -> "paykeeper2"

Создаем параметр:
paykeeper2.PayPal.namespace

Значение:
PayPal_Express

Namespace вы можете узнать в документации компонента.

4. На любой странице вызываем сниппет так:

[[!Paykeeper2?
&gateway=`PayPal`
&printParameters=`1`
]]

На выходе вы увидете массив необходимых параметров.

В данном случае такой:

Array
(
    [PayPal Express] => Array
        (
            [username] => 
            [password] => 
            [signature] => 
            [testMode] => 
            [solutionType] => Array
                (
                    [0] => Sole
                    [1] => Mark
                )
            [landingPage] => Array
                (
                    [0] => Billing
                    [1] => Login
                )
            [brandName] => 
            [headerImageUrl] => 
            [logoImageUrl] => 
            [borderColor] => 
        )

)

Это значит, что в системных настройках нужно создать дополнительные параметры:

paykeeper2.PayPal.username
paykeeper2.PayPal.password
paykeeper2.PayPal.signature

Остальные по желанию.
Пример данных:

username=sdk-three_api1.sdk.com
password=QFZCWN5HZM8VBG7Q
signature=A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU

Войдите в систему PayPal на сайте https://www.paypal.com/. Далее перейдите по ссылке "Настройки продавца" (слева внизу).
В строке "Доступ к API-интерфейсу" перейти по ссылке "Обновить". Далее ссылка "Просмотр подписи API" (Вариант 2).
См. скриншот core/components/paykeeper2/docs/screenshots/paypal_api.png
Здесь Вы можете взять данные для настройки платежей.

Кроме того, обязательными параметрами для всех платежных систем являются:

paykeeper2.PayPal.notifyUrl - URL обработчика платежных данных.
paykeeper2.PayPal.returnUrl - URL на который нужно вернуться после оплаты.
paykeeper2.PayPal.cancelUrl - URL на который нужно перенаправить в случае отказа от оплаты.

notifyUrl - URL по которому платежная система отправляет данные платежа в фоновом режиме.
Некоторые системы (как PayPal Express) не умеют отправлять данные в фоновом режиме (не заметно для пользователя),
для таких систем нужно добавить дополнительный параметр:

paykeeper2.PayPal.successUrl - URL на который нужно перенаправить после успешного платежа.
В этом случае в параметре "returnUrl" нужно указать такой же URL как "notifyUrl".

Примеры значений:
paykeeper2.PayPal.notifyUrl={site_url}assets/components/paykeeper2/connector_result_paypal.php
paykeeper2.PayPal.returnUrl={site_url}assets/components/paykeeper2/connector_result_paypal.php
paykeeper2.PayPal.cancelUrl={site_url}platezh-ne-vyipolnen.html
paykeeper2.PayPal.successUrl={site_url}spasibo-za-pokupku.html


5. Если у вас используются несколько платежных систем, в форму (чанк указанный в параметре "tpl") можно добавить выбор:

<select id="pkInputMethod[[+sid]]" name="method" class="form-control">
    <option value="YandexMoney__PC">Оплата из кошелька в Яндекс.Деньгах</option>
    <option value="YandexMoney__AC">Оплата с произвольной банковской карты</option>
    ...
    <option value="WebMoney__R">WebMoney</option>
    <option value="PayPal__Express">PayPal</option>
</select>

В этом случае платежная система будет браться из значения до разделителя "__". Второй параметр (после разделителя) - метод оплаты.
В вызове сниппета "Paykeeper2" параметр &gateway=`PayPal` можно не указывать.

6. Тестовый вызов сниппета:

[[!Paykeeper2?
&gateway=`PayPal`
&tpl=`pk_form`
&product=`http://test.com/files/file.zip`
&productName=`Тестовый товар`
&price=`1`
&currency=`RUB`
&testMode=`1`
]]

7. Теперь нужно написать обработчик прохождения платежа. Прилагаются примеры:

/assets/components/paykeeper2/connector_result.php - Yandex.Kassa
/assets/components/paykeeper2/connector_result_wm.php - WebMoney
/assets/components/paykeeper2/connector_result_paypal.php - PayPal

--------------------------------------------------

Для защиты от индексирования поисковыми роботами страницы "Мои покупки" создайте в корне сайта файл robots.txt.
Пример содержания:

User-agent: *
Disallow: /moi-pokupki.html

---

Если у вас не открывается "Журнал ошибок" в админке, удалите файл:
/core/cache/logs/error.log

Возможно этот файл слишком большой по размеру или в него попали не читаемые для MODX символы.

--------------------------------------------------

Важно!

На сервере должна быть правильно настроена временная зона.
Проверить можно в админке "Отчеты" -> "Информация о системе" -> "Серверное время"

Для PHP, если Москва: date.timezone = Europe/Moscow

Время mysql можно проверить выполнив запрос (можно в phpMyAdmin):
SELECT NOW();

Сверьте время с текущим.

Установить временную зону MySQL (Москва):
SET GLOBAL time_zone = '+3:00';

Также время можно проверить в терминале (консоли сервера). Выполните команду: date
Настройка временной зоны для сервера Ubuntu (Debian) производится через команду: dpkg-reconfigure tzdata

--------------------------------------------------

Временные зоны:

Europe/Moscow

Список временных зон
http://php.net/manual/ru/timezones.php

--------------------------------------------------

cd ***/core/components/paykeeper2
php composer.phar update

composer require yandexmoney/omnipay
composer require dercoder/omnipay-webmoney
composer require omnipay/paypal


Павел Романов 11.04.2016 14:03 #
Самое простое — заключить договор с каким-нибудь агрегатором платежей типа Платрона, Робокассы и т. д. На странице редиректа (туда, куда перекидывает пользователя после заказа) в этом случае просто ставится форма оплаты конкретного агрегатора (см. их API).
В случае с Платроном или PayAnyWay вообще есть готовые модули для SHK (там под 2 версию, но под третий переписать не сложно), в случае с каким-нибудь Хронопеем или Ассистом — просто ставится форма, поля которой заполняются стандартными плейсхолдерами SHK.
Если требуется по завершению оплаты переводить заказ в статус «Оплачен», то тут нужно создать страницу для callback и вызывать на ней соответствующий сниппет, который будет это делать (у Платрона и PayAnyWay уже такой функционал есть).