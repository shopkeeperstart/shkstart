<?php

/**
 * Paykeeper2GetProcessor
 *
 */
class Paykeeper2GetProcessor extends modObjectGetProcessor {
    public $classKey = 'pkItems';
    public $languageTopics = array('paykeeper2:default');
    public $objectType = 'paykeeper2.items';
}
return 'Paykeeper2GetProcessor';