<?php

/**
 * Paykeeper2GetBuyersProcessor
 *
 */
class Paykeeper2GetBuyersProcessor extends modProcessor {
    public $classKey = 'pkPayments';
    public $languageTopics = array('paykeeper2:default');
    public $defaultSortField = 'id';
    
    public function process() {
        
        $user = $this->modx->getUser('mgr');
        $itemId = $this->getProperty('query');
        $limit = intval($this->getProperty('limit'));
        $start = intval($this->getProperty('start'));
        
        //Check permission
        if(!$user->sudo && !$user->isMember('Administrator')){
            $itemCount = $this->modx->getCount('pkItems', array(
                'id' => $itemId,
                'userid' => $user->id
            ));
            if( !$itemCount ){
                return $this->failure($this->modx->lexicon('permission_denied'));
            }
        }
        
        $c = $this->modx->newQuery($this->classKey);
        $c->select($this->modx->getSelectColumns($this->classKey, $this->classKey));
        $c->where(array(
            'itemid' => $itemId,
            'state' => 'success'
        ));
        
        $sortKey = $this->getProperty('sort', 'id');
        $c->sortby($sortKey, $this->getProperty('dir', 'DESC'));
        if ($limit > 0) {
            $c->limit($limit,$start);
        }
        
        $collection = $this->modx->getCollection($this->classKey, $c);
        $data = '';
        
        if( !empty($collection) ){
            
            foreach($collection as $obj){
                $data .= $obj->email . PHP_EOL;
            }
            
        }
        
        $output = array(
            'success' => true,
            'message' => '',
            'object' => trim( $data )
        );
        
        return $output;
        
    }
    
}

return 'Paykeeper2GetBuyersProcessor';
