<?php

/**
 * Paykeeper2RemoveProcessor
 *
 */
class Paykeeper2RemoveProcessor extends modObjectRemoveProcessor {
    public $classKey = 'pkItems';
    public $languageTopics = array('paykeeper2:default');
    public $objectType = 'paykeeper2.items';
    
    public function beforeRemove(){
        
        $user = $this->modx->getUser('mgr');
        $properties = $this->getProperties();
        
        //Check permission
        if(!$user->sudo && !$user->isMember('Administrator')){
            $itemCount = $this->modx->getCount('pkItems', array(
                'id' => $properties['id'],
                'userid' => $user->id
            ));
            if( !$itemCount ){
                $this->modx->error->failure($this->modx->lexicon('permission_denied'));
            }
        }
        
        return parent::beforeRemove();
    }
}
return 'Paykeeper2RemoveProcessor';