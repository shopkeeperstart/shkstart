<?php

/**
 * Paykeeper2UpdateProcessor
 *
 */
class Paykeeper2UpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'pkItems';
    public $languageTopics = array('paykeeper2:default');
    public $objectType = 'paykeeper2.items';
    
    /**
     * Set the runtime properties for the processor
     * @param array $properties The properties, in array and key-value form, to run on this processor
     * @return void
     */
    public function setProperties() {
        
        $requestBody = file_get_contents('php://input');
        $properties = json_decode( $requestBody, true );
        $properties['state'] = 'available';
        
        $this->properties = array_merge($this->properties, $properties);
    }
    
    public function beforeSet(){
        
        $user = $this->modx->getUser('mgr');
        $properties = $this->getProperties();
        
        //Check permission
        if(!$user->sudo && !$user->isMember('Administrator')){
            $itemCount = $this->modx->getCount('pkItems', array(
                'id' => $properties['id'],
                'userid' => $user->id
            ));
            if( !$itemCount ){
                $this->modx->error->failure($this->modx->lexicon('permission_denied'));
            }
        }
        
        return parent::beforeSet();
    }
    
}
return 'Paykeeper2UpdateProcessor';