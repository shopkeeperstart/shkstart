<?php

/**
 * Paykeeper2CreateProcessor
 *
 */
class Paykeeper2CreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'pkItems';
    public $languageTopics = array('paykeeper2:default');
    public $objectType = 'paykeeper2.items';
    
    /**
     * Set the runtime properties for the processor
     * @param array $properties The properties, in array and key-value form, to run on this processor
     * @return void
     */
    public function setProperties() {
        
        $requestBody = file_get_contents('php://input');
        $properties = json_decode( $requestBody, true );
        $properties['state'] = 'available';
        
        $user = $this->modx->getUser('mgr');
        $properties['userid'] = $user->id;
        $this->properties = array_merge($this->properties, $properties);
    }
    
    public function beforeSave() {
        /*
        $name = $this->getProperty('name');
        if (empty($name)) {
            $this->addFieldError('name',$this->modx->lexicon('paykeeper2.err_ns_name'));
        }
        */
        return parent::beforeSave();
    }
}
return 'Paykeeper2CreateProcessor';