<?php

/**
 * Paykeeper2UpdateBuyersProcessor
 *
 */
class Paykeeper2UpdateBuyersProcessor extends modProcessor {
    public $classKey = 'pkPayments';
    public $languageTopics = array('paykeeper2:default');
    public $defaultSortField = 'id';
    
    /**
     * Set the runtime properties for the processor
     * @param array $properties The properties, in array and key-value form, to run on this processor
     * @return void
     */
    public function setProperties( $properties ) {
        unset($properties['HTTP_MODAUTH']);
        $requestBody = file_get_contents('php://input');
        if( $requestBody ){
            $properties = array_merge( $properties, json_decode( $requestBody, true ));
        }
        $this->properties = array_merge($this->properties, $properties);
    }
    
    public function process() {
        
        $canUpdate = $this->beforeSet();
        if ($canUpdate !== true) {
            return $this->failure($canUpdate);
        }
        
        $itemId = intval( $this->getProperty( 'id', 0 ) );
        $buyers = $this->getProperty( 'data', '' );
        
        if( $itemId ){
            
            $buyers_arr = $buyers ? explode( PHP_EOL, $buyers ) : array();
            array_map( 'trim', $buyers_arr );
            
            $product = $this->modx->getObject( 'pkItems', $itemId );
            
            if( !empty( $product ) ){
                
                $c = $this->modx->newQuery($this->classKey);
                $c->where(array(
                    'itemid' => $itemId,
                    'state' => 'success'
                ));
                $collection = $this->modx->getCollection($this->classKey, $c);
                
                //Remove payments
                if( !empty($collection) ){
                    
                    foreach($collection as $obj){
                        
                        if( !in_array( $obj->email, $buyers_arr ) ){
                            $obj->remove();
                        } else {
                            $index = array_search( $obj->email, $buyers_arr );
                            if( $index !== false ){
                                array_splice( $buyers_arr, $index, 1 );
                            }
                        }
                        
                    }
                    
                }
                
                //Add new payments
                $buyers_arr = array_unique( $buyers_arr );
                if( !empty( $buyers_arr ) ){
                    foreach( $buyers_arr as $buyer_email ){
                        
                        if( empty( $buyer_email ) ) continue;
                        
                        $payment = $this->modx->newObject($this->classKey);
                        $payment->set( 'itemid', $product->id );
                        $payment->set( 'email', $buyer_email );
                        $payment->set( 'state', 'success' );
                        $payment->set( 'datetime', strftime('%Y-%m-%d %H:%M:%S') );
                        $payment->set( 'value', $product->price );
                        $payment->save();
                        
                    }
                }
                
            }
            
        }
        
        $output = array(
            'success' => true,
            'message' => '',
            'object' => array()
        );
        
        return $output;
        
    }
    
    public function beforeSet(){
        
        $user = $this->modx->getUser('mgr');
        $properties = $this->getProperties();
        
        //Check permission
        if(!$user->sudo && !$user->isMember('Administrator')){
            $this->modx->error->failure($this->modx->lexicon('permission_denied'));
        }
        
        return !$this->hasErrors();
    }
    
}

return 'Paykeeper2UpdateBuyersProcessor';
