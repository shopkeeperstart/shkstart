<?php

/**
 * Paykeeper2GetListProcessor
 *
 */
class Paykeeper2GetListProcessor extends modObjectGetListProcessor {
    public $classKey = 'pkItems';
    public $languageTopics = array('paykeeper2:default');
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    public $objectType = 'paykeeper2.items';
    
    /**
     * Get the data of the query
     * @return array
     */
    public function getData() {
        $data = array();
        $user = $this->modx->getUser('mgr');
        $limit = intval($this->getProperty('limit'));
        $start = intval($this->getProperty('start'));
        
        $sub_query = "(SELECT COUNT(*) FROM {$this->modx->getTableName('pkPayments')}"
            . " WHERE {$this->modx->getTableName('pkPayments')}.`itemid` = `{$this->classKey}`.`id`"
            . " AND {$this->modx->getTableName('pkPayments')}.`state` = 'success'"
            . ") AS `count`";
        
        $c = $this->modx->newQuery($this->classKey);
        $c->select($this->modx->getSelectColumns($this->classKey, $this->classKey));
        $c->select( $sub_query );
        
        if(!$user->sudo && !$user->isMember('Administrator')){
            $c->where(array( 'userid' => $user->id ));
        }
        
        $c = $this->prepareQueryBeforeCount($c);
        $data['total'] = $this->modx->getCount($this->classKey,$c);
        $c = $this->prepareQueryAfterCount($c);
        
        $sortClassKey = $this->getSortClassKey();
        $sortKey = $this->modx->getSelectColumns($sortClassKey,$this->getProperty('sortAlias',$sortClassKey),'',array($this->getProperty('sort')));
        if (empty($sortKey)) $sortKey = $this->getProperty('sort');
        $c->sortby($sortKey,$this->getProperty('dir'));
        if ($limit > 0) {
            $c->limit($limit,$start);
        }
        
        $data['results'] = $this->modx->getCollection($this->classKey,$c);
        
        return $data;
    }
}

return 'Paykeeper2GetListProcessor';
