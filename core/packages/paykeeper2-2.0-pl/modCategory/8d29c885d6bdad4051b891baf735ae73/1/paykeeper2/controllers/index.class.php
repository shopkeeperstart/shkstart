<?php

//ini_set('display_errors',1);
//error_reporting(E_ALL);

require_once dirname(dirname(__FILE__)) . '/model/paykeeper2_manager.class.php';

class Paykeeper2IndexManagerController extends modExtraManagerController {
    public $pkManager;
    public function initialize() {
        $this->pkManager = new paykeeper2Manager( $this->modx );
        $headHTML = '<script type="text/javascript" src="' . $this->pkManager->config['assetsUrl'] . 'config.js.php' . '"></script>';
        $headHTML .= PHP_EOL . '<link rel="stylesheet" href="' . $this->pkManager->config['assetsUrl'] . 'lib/bootstrap/dist/css/bootstrap.min.css">';
        $headHTML .= PHP_EOL . '<link rel="stylesheet" href="' . $this->pkManager->config['assetsUrl'] . 'lib/todc-bootstrap/dist/css/todc-bootstrap.min.css">';
        $headHTML .= PHP_EOL . '<link rel="stylesheet" href="' . $this->pkManager->config['assetsUrl'] . 'css/manager-bootstrap-clear.css">';
        
        $headHTML .= PHP_EOL . '<script src="' . $this->pkManager->config['assetsUrl'] . 'node_modules/es6-shim/es6-shim.min.js"></script>';
        $headHTML .= PHP_EOL . '<script src="' . $this->pkManager->config['assetsUrl'] . 'node_modules/systemjs/dist/system-polyfills.js"></script>';
        $headHTML .= PHP_EOL . '<script src="' . $this->pkManager->config['assetsUrl'] . 'node_modules/systemjs/dist/system.js"></script>';
        
        $this->addHtml( $headHTML );
        return parent::initialize();
    }
    public function getLanguageTopics() {
        return array('paykeeper2:manager');
    }
    public function checkPermissions() { return true;}
    public function process(array $scriptProperties = array()) {}
    public function getPageTitle() { return $this->modx->lexicon('paykeeper2'); }
    public function loadCustomCssJs() {
        $this->addCss($this->pkManager->config['assetsUrl'] . 'css/styles.css' );
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'lib/jquery/dist/jquery.min.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'lib/bootstrap/dist/js/bootstrap.min.js');
        //$this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/es6-shim/es6-shim.min.js');
        //$this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/systemjs/dist/system-polyfills.js');

        //$this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/systemjs/dist/system.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/rxjs/bundles/Rx.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/angular2/bundles/angular2-polyfills.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/angular2/bundles/angular2.dev.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/angular2/bundles/router.dev.js');
        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'node_modules/angular2/bundles/http.min.js');

        $this->addLastJavascript($this->pkManager->config['assetsUrl'] . 'app/js/bundle.js');
    }
    public function getTemplateFile() {
        return $this->pkManager->config['templatesPath'].'home.tpl';
    }
}
