<p>
    Пожалуйста, введите ваш адрес. эл. почты.
    <br>
    Вам будет отправлено письмо со ссылкой на страницу, с которой вы сможете скачать ваши покупки.
</p>

<a name="form[[+sid]]"></a>
<form id="pkForm[[+sid]]" method="post" action="[[~[[*id]]?scheme=`abs`]]#form[[+sid]]" class="form-inline">
    <input type="hidden" name="sid" value="[[+sid]]">
    <input type="hidden" name="action" value="download">
    <label class="control-label" for="pkInputEmail[[+sid]]">Ваш адрес эл. почты:</label>
    <div>
        <div class="form-group">
            <input type="email" class="form-control" id="pkInputEmail[[+sid]]" name="email" value="[[+user_email]]" required>
        </div>
        <button type="submit" class="btn btn-primary">Продолжить</button>
    </div>
</form>