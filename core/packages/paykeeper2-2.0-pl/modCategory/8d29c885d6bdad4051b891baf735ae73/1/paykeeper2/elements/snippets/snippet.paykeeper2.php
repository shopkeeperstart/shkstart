<?php
if( $modx->getOption('paykeeper2.debug') ){
    ini_set('display_errors',1);
    error_reporting(E_ALL);
}

$output = '';

$paykeeper_path = $modx->getOption('core_path') . 'components/paykeeper2';
require_once $paykeeper_path . '/model/paykeeper.class.php';

$paykeeper = new Paykeeper($modx, $scriptProperties);

if( !empty($scriptProperties['printParameters']) ){
    return '<pre>' . print_r($paykeeper->getGatewayParameters(), true) . '</pre>';
}

$action = $modx->getOption( 'action', $scriptProperties, 'payment' );
if( $action == 'download_form' && !empty( $_GET['download'] ) ){
    $action = 'download';
}

$user_email = isset( $_POST['email'] ) && !is_array( $_POST['email'] ) ? trim($_POST['email']) : '';
if( $user_email && !$paykeeper::validateEmail( $user_email ) ){
    $paykeeper->setMessage( $modx->lexicon('paykeeper2.email_validation_error'), true );
    $action = '';
}

switch( $action ){
    case 'payment':
        
        if( $user_email && isset($_POST['sid']) && $_POST['sid'] == $paykeeper->config['sid'] ){
            
            $result = $paykeeper->startPayment( $user_email );
            if( $result !== false ){
                $user_email = '';
            }
            
        }
        
        break;
    case 'download_form':
        
        if( $user_email && isset($_POST['sid']) && $_POST['sid'] == $paykeeper->config['sid'] ){
            $result = $paykeeper->createDownloadCode( $user_email );
        }
        
        break;
    case 'download':
        
        $secret_code = !empty( $_GET['download'] ) ? (string) trim($_GET['download']) : '';
        $itemId = !empty( $_GET['item'] ) && is_numeric( $_GET['item'] ) ? intval( $_GET['item'] ) : 0;
        if( $secret_code && $itemId ){
            $paykeeper->downloadItem( $itemId, $secret_code );
        } else {
            $output = $paykeeper->downloadsListOutput( $secret_code );
        }
        
        break;
}

if( $action == 'payment'
    && ( !$paykeeper->config['product']
    && !$paykeeper->config['productId']
    && !$paykeeper->config['orderId'] ) ) {
    
    $paykeeper->setFormPlaceholders();
    return '';
}

if( !$output ){
    
    if( !$user_email && !empty( $modx->context ) && $modx->user->isAuthenticated( $modx->context->key ) ){
        $profile = $modx->user->getOne('Profile');
        $user_email = $profile ? $profile->get('email') : '';
    }
    
    //Вывод формы
    $output = $paykeeper->getFormOutput( $user_email );
    
}

$paykeeper->messageToPlaceholder();

if( !empty( $toPlaceholder ) ){
    $modx->setPlaceholder($toPlaceholder, $output);
    $output = '';
}

return $output;