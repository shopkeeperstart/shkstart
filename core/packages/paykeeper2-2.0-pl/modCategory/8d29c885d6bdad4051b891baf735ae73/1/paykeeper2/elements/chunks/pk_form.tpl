<div class="row">
    <div class="col-md-12">
        
        <a name="product[[+sid]]"></a>
        <form id="pkForm[[+sid]]" method="post" action="[[~[[*id]]?scheme=`abs`]]#product[[+sid]]">
            <input type="hidden" name="sid" value="[[+sid]]">
            
            <div class="form-group">
                <label class="control-label" for="pkInputEmail[[+sid]]">Ваш адрес эл. почты:</label>
                <input type="email" class="form-control" id="pkInputEmail[[+sid]]" name="email" value="[[+user_email]]" required>
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Оплатить</button>
                <button id="buttonCancel[[+sid]]" type="button" class="btn btn-default">Отмена</button>
            </div>
            
        </form>
        
        <button id="buttonStart[[+sid]]" type="button" class="btn btn-primary btn-lg" style="display:none;">
            Купить
        </button>
        
    </div>
</div>

<script type="text/javascript">
    document.getElementById('pkForm[[+sid]]').style.display = 'none';
    document.getElementById('buttonStart[[+sid]]').style.display = 'inline-block';
    document.getElementById('buttonStart[[+sid]]').addEventListener("click", function(e){
        e.preventDefault();
        document.getElementById('pkForm[[+sid]]').style.display = 'block';
        this.style.display = 'none'
    }, false);
    document.getElementById('buttonCancel[[+sid]]').addEventListener("click", function(e){
        e.preventDefault();
        document.getElementById('buttonStart[[+sid]]').style.display = 'inline-block';
        document.getElementById('pkForm[[+sid]]').style.display = 'none';
    }, false);
</script>