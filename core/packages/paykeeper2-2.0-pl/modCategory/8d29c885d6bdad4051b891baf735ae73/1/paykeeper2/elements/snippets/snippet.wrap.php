<?php

/*

https://modx.today/posts/2015/11/the-story-of-my-super-snippet

Example usage:

[[*introtext:wrap=`<p class="intro">|</p>`]]

*/

return ( empty($input) === false && strpos($options, '|') !== false )
    ? str_replace('|', $input, $options)
    : '';
