<!--link rel="stylesheet" href="[[++assets_url]]/components/paykeeper2/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="[[++assets_url]]/components/paykeeper2/lib/todc-bootstrap/dist/css/todc-bootstrap.min.css"-->
<link rel="stylesheet" href="[[++assets_url]]/components/paykeeper2/css/styles.css">

<script src="[[++assets_url]]/components/paykeeper2/lib/jquery/dist/jquery.min.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/lib/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- IE required polyfills, in this exact order -->
<script src="[[++assets_url]]/components/paykeeper2/node_modules/es6-shim/es6-shim.min.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/systemjs/dist/system-polyfills.js"></script>

<script src="[[++assets_url]]/components/paykeeper2/node_modules/systemjs/dist/system.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/rxjs/bundles/Rx.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/angular2/bundles/angular2-polyfills.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/angular2/bundles/angular2.dev.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/angular2/bundles/router.dev.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/node_modules/angular2/bundles/http.min.js"></script>
<script src="[[++assets_url]]/components/paykeeper2/config.js.php"></script>

<script src="[[++assets_url]]/components/paykeeper2/app/js/bundle.js"></script>

<div class="cmp-container">
    <my-app>Загрузка...</my-app>
</div>

<script>
    System.config({
        packages: {
            app: {
                format: 'register',
                defaultExtension: 'js'
            }
        }
    });
    System.import('main')
            .then(null, console.error.bind(console));
</script>