<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => '',
    'readme' => '
Сниппет "Онлайн оплата" - Paykeeper2

http://modx-shopkeeper.ru/ - Бесплатный компонент для MODX Revolution для создания интернет-магазина.
http://e-store.modx-shopkeeper.ru/ - Электронные товары.

--------------------------------------------------

Paykeeper 2

Сниппет для продажи электронных товаров, а так же оплаты заказов в интернет-магазине на основе Shopkeeper 3.x.
Компонент для управления электронными товарами (Фронт-енд написан на Angular2 + TypeScript).
Используется библиотека Omnipay. Эта библиотека позволяет делать единый API для разных платежных систем.
Сайт проекта: http://omnipay.thephpleague.com/

В состав входят готовые обработчики для платежных систем:
Yandex.Kassa (агрегатор), WebMoney, PayPal.

Есть инструкция по подключению других платежных систем с помощью компонентов Omnipay.
Требуются базовые (начальные) знания PHP.

Агрегатор Яндекс.Касса имеет следующие методы оплаты:

Банковские карты - Visa (включая Electron), MasterCard и Maestro.
Яндекс.Деньги - От 25 млн пользователей электронных кошельков.
WebMoney - Из рублевых WM-кошельков.
QIWI Wallet - На сайте QIWI и через приложение QIWI Wallet.
Наличные - Через терминалы, банкоматы и салоны связи — это больше 250 000 пунктов в России и других странах.
Платежи через интернет-банки - Через «Альфа-клик», интернет-банк «Промсвязьбанка», Сбербанк Онлайн и MasterPass — всего около 30 млн пользователей.
Платежи со счета мобильного - От абонентов Билайна, МегаФона, МТС.
Платежи через кредитные сервисы - «КупиВкредит» («Тинькофф Банк»), доверительный платеж («Куппи.ру»).

--------------------------------------------------

Техническая поддержка:

http://modx-shopkeeper.ru/forum/

Если на форуме долго нет ответа, можно писать на почту автору с помощью форм:
http://modx-shopkeeper.ru/contacts/
или
http://e-store.modx-shopkeeper.ru/kontaktyi.html

--------------------------------------------------

Параметры сниппета Paykeeper2:

gateway - Название платежной системы (или агрегатора). Пример: &gateway=`YandexMoney`
action - Действие. payment - оплата, download_form - скачивание эл. товаров.
product - Ссылка на товар или информация, которая продается. Указывается вместо productId.
productName - Название товара.
productId - ID товара (не обязательно). Если не указано, создается новый ID для товара, указанного в параметре "product".
orderId - ID заказа в магазине. Используется для оплаты заказов в магазине, созданном на базе Shopkeeper 3.x.
price - Цена товара. Если указывается productId, то цену указывать не нужно.
currency - Валюта. По умолчанию: RUB
method - Метод оплаты (не обязательно).
lang - Язык. По умолчанию: ru
autoLink - Автоматически переводить значение параметра "product" в полную ссылку. По умолчанию: true
testMode - Тестовый режим оплаты. По умолчанию: false
printParameters - Показать список обязательный поле оплаты. По умолчанию: false
tpl - Чанк шаблона формы оплаты. По умолчанию: pk_form
chunkEmailPurchases - Чанк шаблона письма со ссылкой на скачивание эл. товаров. По умолчанию: pk_chunkEmailPurchases
chunkDownloadsItem - Чанк одного товара в списке. По умолчанию: pk_chunkDownloadsItem
chunkDownloadsOuter - Чанк обертки для списка товаров. По умолчанию: pk_chunkDownloadsOuter
shop_className - Имя класса таблицы заказов магазина. По умолчанию: shopkeeper3
shop_packageName - Имя пакета таблицы заказов магазина. По умолчанию: shk_order

Справка:
Вместо значения "true" нужно указывать "1" (еденица без кавычек).
Вместо значения "false" нужно указывать "0" (ноль без кавычек).

--------------------------------------------------

Примеры

Страница электронного товара

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

--------------------------------------------------

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&productId=`[[*product_id]]`
]]

--------------------------------------------------

Оплата заказа в интернет магазине на базе Shopkeeper 3.x:

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&orderId=`[[!+shk.id]]`
]]

--------------------------------------------------

Страница "Мои покупки"

[[!Paykeeper2?
&action=`download_form`
&tpl=`pk_downloadForm`
&toPlaceholder=`pkForm`
]]

[[!+paykeeper2.msg:wrap=`<div class="alert [[!+paykeeper2.is_error:eq=`1`:then=`alert-danger`:else=`alert-success`]]">|</div>`]]

[[!+pkForm]]

--------------------------------------------------

Параметр &gateway можно устанавливать динамически. Дабавьте в форму селект с выбором метода оплаты.

Пример:

select id="pkInputMethod[[+sid]]" name="method" class="form-control">
    <option value="YandexMoney__PC">Оплата из кошелька в Яндекс.Деньгах</option>
    <option value="YandexMoney__AC">Оплата с произвольной банковской карты</option>
    <option value="YandexMoney__MC">Платеж со счета мобильного телефона</option>
    <option value="YandexMoney__GP">Оплата наличными через кассы и терминалы</option>
    <option value="YandexMoney__WM">Оплата из кошелька в системе WebMoney</option>
    <option value="YandexMoney__SB">Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн</option>
    <option value="YandexMoney__MP">Оплата через мобильный терминал (mPOS)</option>
    <option value="YandexMoney__AB">Оплата через Альфа-Клик</option>
    <option value="YandexMoney__МА">Оплата через MasterPass</option>
    <option value="YandexMoney__PB">Оплата через Промсвязьбанк</option>
    <option value="YandexMoney__QW">Оплата через QIWI Wallet</option>
    <option value="YandexMoney__KV">Оплата через КупиВкредит (Тинькофф Банк)</option>
    
    <option value="WebMoney__R">WebMoney</option>
</select>

В данном значение до разделителя "__" это название платежной системы, а то что после - метод оплаты.
Если в значении нет разделителя "__", то используется платежная система, которая указана в параметре &gateway.

--------------------------------------------------
--------------------------------------------------

Плейсхолдеры на странице вызова сниппета

На странице где вызывается сниппет Paykeeper2 доступны плейсхолдеры:

[[!+paykeeper2.msq]] - Текст сообщения.
[[!+paykeeper2.is_error]] - Сообщение - ошибка (1) или нет (0).
[[!+paykeeper2.price]] - Цена товара.
[[!+paykeeper2.productName]] - Название товара.

"!" - значит, что значение плейсхолдера не кэшируется.

Пример использования:

[[!+paykeeper2.msg:wrap=`<div class="alert [[!+paykeeper2.is_error:eq=`1`:then=`alert-danger`:else=`alert-success`]]">|</div>`]]

Код сниппета "wrap" можно взять в файле:
/core/components/paykeeper2/elements/snippets/snippet.wrap.php
или здесь:
https://modx.today/posts/2015/11/the-story-of-my-super-snippet

--------------------------------------------------
--------------------------------------------------

Яндекс.Касса

https://kassa.yandex.ru/

Зарегистрироваться на сайте системы Яндекс.Касса.

Для сheckURL и paymentAvisoURL указать:
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result.php

successURL и failURL - динамические

В админке сайта перейти в "Настройки системы" -> "paykeeper2" (пространство имен в фильтре).
Заполнить все необходимые поля (есть комментарии).

Включить тестовый режим (см. параметры сниппета) и протестировать оплату.

Справка:
Для подключения сайта к системе "Яндекс.Касса" нужен SSL сертификат (https).

Тестовые данные
https://tech.yandex.ru/money/doc/payment-solution/examples/examples-test-data-docpage/

Документация
https://money.yandex.ru/doc.xml?id=527069

Примеры на PHP
https://github.com/YandexMoney/yandexmoney/tree/master/Yandex.Kassa/example%20integration/php

Пример вызова сниппета:

[[!Paykeeper2?
&gateway=`YandexMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

--------------------------------------------------

WebMoney

https://wiki.webmoney.ru/projects/webmoney/wiki/Merchant_WebMoney_Transfer

Описание интерфейса сервиса Web Merchant Interface:
https://merchant.webmoney.ru/conf/guide.asp

Result URL
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result_wm.php

[[!Paykeeper2?
&gateway=`WebMoney`
&tpl=`pk_form`
&product=`[[*e_product]]`
&productName=`[[*pagetitle]]`
&price=`[[*price]]`
]]

notifyUrl - URL, на которы платежная система будет отправлять запросы для проведения платежа (Result URL в настройках WebMoney Merchant).
Должен быть таким:
https://[АДРЕС ВАШЕГО САЙТА]/assets/components/paykeeper2/connector_result_wm.php

В админке сайта перейти в "Настройки системы" -> "paykeeper2" (пространство имен в фильтре).
Заполнить все необходимые поля (есть комментарии).

Cкриншоты настроек платежной системы:
/core/components/paykeeper2/docs/screenshots/wm_001.png
/core/components/paykeeper2/docs/screenshots/wm_002.png

--------------------------------------------------

Подключение платежных систем с помощью компонентов OmniPay

Сайт проекта:
http://omnipay.thephpleague.com/

Репозиторий:
https://github.com/thephpleague/omnipay

На примере PayPal

Документация платежной системы PayPal:
https://developer.paypal.com/webapps/developer/docs/classic/products/#pfg
https://developer.paypal.com/docs/classic/payflow/integration-guide/#configuring-hosted-pages-using-paypal-manager
https://developer.paypal.com/docs/classic/express-checkout/integration-guide/ECGettingStarted/#id0832BA0605Z

Список валют:
https://developer.paypal.com/docs/classic/api/currency_codes/#paypal

1. Устанавливаем composer:

cd /var/www/html/my_site/core/components/paykeeper2
curl -s http://getcomposer.org/installer | php
php composer.phar update

Установка в ОС Windows:
https://getcomposer.org/doc/00-intro.md#installation-windows

2. Загружаем код компонента:

composer require omnipay/paypal:~2.0

Код компонента будет загружен в папку vendor.

3. В админке переходим в "Системные настройки" -> "paykeeper2"

Создаем параметр:
paykeeper2.PayPal.namespace

Значение:
PayPal_Express

Namespace вы можете узнать в документации компонента.

4. На любой странице вызываем сниппет так:

[[!Paykeeper2?
&gateway=`PayPal`
&printParameters=`1`
]]

На выходе вы увидете массив необходимых параметров.

В данном случае такой:

Array
(
    [PayPal Express] => Array
        (
            [username] => 
            [password] => 
            [signature] => 
            [testMode] => 
            [solutionType] => Array
                (
                    [0] => Sole
                    [1] => Mark
                )
            [landingPage] => Array
                (
                    [0] => Billing
                    [1] => Login
                )
            [brandName] => 
            [headerImageUrl] => 
            [logoImageUrl] => 
            [borderColor] => 
        )

)

Это значит, что в системных настройках нужно создать дополнительные параметры:

paykeeper2.PayPal.username
paykeeper2.PayPal.password
paykeeper2.PayPal.signature

Остальные по желанию.
Пример данных:

username=sdk-three_api1.sdk.com
password=QFZCWN5HZM8VBG7Q
signature=A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU

Войдите в систему PayPal на сайте https://www.paypal.com/. Далее перейдите по ссылке "Настройки продавца" (слева внизу).
В строке "Доступ к API-интерфейсу" перейти по ссылке "Обновить". Далее ссылка "Просмотр подписи API" (Вариант 2).
См. скриншот core/components/paykeeper2/docs/screenshots/paypal_api.png
Здесь Вы можете взять данные для настройки платежей.

Кроме того, обязательными параметрами для всех платежных систем являются:

paykeeper2.PayPal.notifyUrl - URL обработчика платежных данных.
paykeeper2.PayPal.returnUrl - URL на который нужно вернуться после оплаты.
paykeeper2.PayPal.cancelUrl - URL на который нужно перенаправить в случае отказа от оплаты.

notifyUrl - URL по которому платежная система отправляет данные платежа в фоновом режиме.
Некоторые системы (как PayPal Express) не умеют отправлять данные в фоновом режиме (не заметно для пользователя),
для таких систем нужно добавить дополнительный параметр:

paykeeper2.PayPal.successUrl - URL на который нужно перенаправить после успешного платежа.
В этом случае в параметре "returnUrl" нужно указать такой же URL как "notifyUrl".

Примеры значений:
paykeeper2.PayPal.notifyUrl={site_url}assets/components/paykeeper2/connector_result_paypal.php
paykeeper2.PayPal.returnUrl={site_url}assets/components/paykeeper2/connector_result_paypal.php
paykeeper2.PayPal.cancelUrl={site_url}platezh-ne-vyipolnen.html
paykeeper2.PayPal.successUrl={site_url}spasibo-za-pokupku.html


5. Если у вас используются несколько платежных систем, в форму (чанк указанный в параметре "tpl") можно добавить выбор:

<select id="pkInputMethod[[+sid]]" name="method" class="form-control">
    <option value="YandexMoney__PC">Оплата из кошелька в Яндекс.Деньгах</option>
    <option value="YandexMoney__AC">Оплата с произвольной банковской карты</option>
    ...
    <option value="WebMoney__R">WebMoney</option>
    <option value="PayPal__Express">PayPal</option>
</select>

В этом случае платежная система будет браться из значения до разделителя "__". Второй параметр (после разделителя) - метод оплаты.
В вызове сниппета "Paykeeper2" параметр &gateway=`PayPal` можно не указывать.

6. Тестовый вызов сниппета:

[[!Paykeeper2?
&gateway=`PayPal`
&tpl=`pk_form`
&product=`http://test.com/files/file.zip`
&productName=`Тестовый товар`
&price=`1`
&currency=`RUB`
&testMode=`1`
]]

7. Теперь нужно написать обработчик прохождения платежа. Прилагаются примеры:

/assets/components/paykeeper2/connector_result.php - Yandex.Kassa
/assets/components/paykeeper2/connector_result_wm.php - WebMoney
/assets/components/paykeeper2/connector_result_paypal.php - PayPal

--------------------------------------------------

Для защиты от индексирования поисковыми роботами страницы "Мои покупки" создайте в корне сайта файл robots.txt.
Пример содержания:

User-agent: *
Disallow: /moi-pokupki.html

---

Если у вас не открывается "Журнал ошибок" в админке, удалите файл:
/core/cache/logs/error.log

Возможно этот файл слишком большой по размеру или в него попали не читаемые для MODX символы.

--------------------------------------------------

Важно!

На сервере должна быть правильно настроена временная зона.
Проверить можно в админке "Отчеты" -> "Информация о системе" -> "Серверное время"

Для PHP, если Москва: date.timezone = Europe/Moscow

Время mysql можно проверить выполнив запрос (можно в phpMyAdmin):
SELECT NOW();

Сверьте время с текущим.

Установить временную зону MySQL (Москва):
SET GLOBAL time_zone = \'+3:00\';

Также время можно проверить в терминале (консоли сервера). Выполните команду: date
Настройка временной зоны для сервера Ubuntu (Debian) производится через команду: dpkg-reconfigure tzdata

--------------------------------------------------

Временные зоны:

Europe/Moscow

Список временных зон
http://php.net/manual/ru/timezones.php

--------------------------------------------------

cd ***/core/components/paykeeper2
php composer.phar update

composer require yandexmoney/omnipay
composer require dercoder/omnipay-webmoney
composer require omnipay/paypal
',
    'changelog' => '
Changelog

2.0pl

- Исправлена проблема с оплатой заказов интернет-магазина, созданного на базе Shopkeeper3.

2.0rc2

- Добавлен контроль прав доступа в компоненте управления электронными товарами. Все товары может видеть и редактировать только админ, юзер видит только свои товары.
- Добавлена возможность оплаты заказов в Shopkeeper3.
- Добавлены платежные системы WebMoney и PayPal.
- Добавлена возможность использовать несколько платежных систем на выбор покупателя.
- Дополнена документация.

2.0 rc1

- Переписан на Angular2 + TypeScript.
- Добавлена постраничная разбивка товаров.

2.0 beta3

- Исправлена проблема с протоколом HTTPS при скачивании электронного товара.
- Дополнена документация.

2.0beta

- Initial version.

',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'fa0c4e8f0ed32d802a280001c6474ca7',
      'native_key' => 'paykeeper2',
      'filename' => 'modNamespace/b5ffe966fb4a318bd073ca4eb4038cbd.vehicle',
      'namespace' => 'paykeeper2',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => '0cb154bd3bd042aa8604a35a243b0d5c',
      'native_key' => 'paykeeper2',
      'filename' => 'modMenu/f572fac71f659318ed2d2e5728139002.vehicle',
      'namespace' => 'paykeeper2',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '3a5b3ca687df4799111f276d7b82aafb',
      'native_key' => 1,
      'filename' => 'modCategory/8d29c885d6bdad4051b891baf735ae73.vehicle',
      'namespace' => 'paykeeper2',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '090b987b1e9c2ab4565bf7ec2fc457b2',
      'native_key' => 'paykeeper2.debug',
      'filename' => 'modSystemSetting/e3323ad08bf4cf50d1b04e383006bfe5.vehicle',
      'namespace' => 'paykeeper2',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '21c61cebdf6d9b289ecd927ddcd18ec3',
      'native_key' => 'paykeeper2.YandexMoney.namespace',
      'filename' => 'modSystemSetting/043fe2173a1607d7c3d6054c7b354790.vehicle',
      'namespace' => 'paykeeper2',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '43dc4b9fc36e5945b736aa20771905ad',
      'native_key' => 'paykeeper2.YandexMoney.cancelUrl',
      'filename' => 'modSystemSetting/7a5c3eef8694f954e1bc709ff55fe6c6.vehicle',
      'namespace' => 'paykeeper2',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '81c356807cf334d1116a7c2e57fdb4d9',
      'native_key' => 'paykeeper2.YandexMoney.returnUrl',
      'filename' => 'modSystemSetting/bebbe52ff188ef291c167b9281b563d6.vehicle',
      'namespace' => 'paykeeper2',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'a63c0041041c694ddf64415579918076',
      'native_key' => 'paykeeper2.YandexMoney.currencyNum',
      'filename' => 'modSystemSetting/6f7b83cbc2a1ba70f5a3ed7540716a3e.vehicle',
      'namespace' => 'paykeeper2',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd23ac3db8bf31970e176b06f869d4810',
      'native_key' => 'paykeeper2.YandexMoney.method',
      'filename' => 'modSystemSetting/e92f01466f7ed88f5f923ddf4661ab92.vehicle',
      'namespace' => 'paykeeper2',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'bf4c8a5cbb897f6f5ca76337da0dda0a',
      'native_key' => 'paykeeper2.YandexMoney.scid',
      'filename' => 'modSystemSetting/d3e58025c707d7e6c22f792d82f3fdf5.vehicle',
      'namespace' => 'paykeeper2',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'aa90d73333e16d6f6329dca45a6b67b9',
      'native_key' => 'paykeeper2.YandexMoney.shopPassword',
      'filename' => 'modSystemSetting/3e8d281271846d2aa69abc40dc61c1ac.vehicle',
      'namespace' => 'paykeeper2',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '21366580fa1361dbd169bdad89f2f35c',
      'native_key' => 'paykeeper2.YandexMoney.shopid',
      'filename' => 'modSystemSetting/1bda4c6d6e8a5ac6e1495274247de3b6.vehicle',
      'namespace' => 'paykeeper2',
    ),
    12 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'edf0731d6b16ff81ac5acf65de28d780',
      'native_key' => 'paykeeper2.dateformat',
      'filename' => 'modSystemSetting/ae096c5de1b77bdd3bd4338508018c6a.vehicle',
      'namespace' => 'paykeeper2',
    ),
    13 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd62e10369612fda0406ed103409ce352',
      'native_key' => 'paykeeper2.downloadPageId',
      'filename' => 'modSystemSetting/0a302f207b55ae0e24b69a8b35133c39.vehicle',
      'namespace' => 'paykeeper2',
    ),
    14 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd5a63236350b09cd31fb79c1032d8679',
      'native_key' => 'paykeeper2.downloadLimitTime',
      'filename' => 'modSystemSetting/6c90577bc9046d91b24a70ddcb6077bd.vehicle',
      'namespace' => 'paykeeper2',
    ),
    15 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'ae2fb130f96a791f7bbbeeabcb3614ca',
      'native_key' => 'paykeeper2.payLimitTime',
      'filename' => 'modSystemSetting/bddc65d658b999e536ab429e403d5e2c.vehicle',
      'namespace' => 'paykeeper2',
    ),
    16 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '5df44d3a875d20245a75c4346a810421',
      'native_key' => 'paykeeper2.payStatusIn',
      'filename' => 'modSystemSetting/b1136cf4754b79ff3b259b362843c631.vehicle',
      'namespace' => 'paykeeper2',
    ),
    17 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '76e5c4c9dc1f3f04c4bcbbaa52181709',
      'native_key' => 'paykeeper2.payStatusOut',
      'filename' => 'modSystemSetting/9c003d0db1c0c7365c15b13607101214.vehicle',
      'namespace' => 'paykeeper2',
    ),
    18 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'b01f18e38b9710ace1ebe5f6184e7e18',
      'native_key' => 'paykeeper2.WebMoney.cancelUrl',
      'filename' => 'modSystemSetting/b04d8646f3c18935f8c78a373526df5f.vehicle',
      'namespace' => 'paykeeper2',
    ),
    19 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd3f619122d3f66deef7c264699ee81bc',
      'native_key' => 'paykeeper2.WebMoney.merchantPurse',
      'filename' => 'modSystemSetting/bc7b96b100ae1f1b2e7b508c1c29c5a5.vehicle',
      'namespace' => 'paykeeper2',
    ),
    20 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'ef30770383b05e68f1bfa9e717f984de',
      'native_key' => 'paykeeper2.WebMoney.namespace',
      'filename' => 'modSystemSetting/6acfabb6eca72c281e38f00325f5983e.vehicle',
      'namespace' => 'paykeeper2',
    ),
    21 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '6676532da9d5eb16b2222c6c061ea4e3',
      'native_key' => 'paykeeper2.WebMoney.notifyUrl',
      'filename' => 'modSystemSetting/75c7b30967c6cbe6644831b2c182788a.vehicle',
      'namespace' => 'paykeeper2',
    ),
    22 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4287a6d6dd383bb6721e012af979ee7e',
      'native_key' => 'paykeeper2.WebMoney.returnUrl',
      'filename' => 'modSystemSetting/0a2a61d84228b91fa47dd7678b6899db.vehicle',
      'namespace' => 'paykeeper2',
    ),
    23 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f15209865dacd2568a7f850e049971d7',
      'native_key' => 'paykeeper2.WebMoney.secretKey',
      'filename' => 'modSystemSetting/c62757e64071d05d255fb1b10b6e29d1.vehicle',
      'namespace' => 'paykeeper2',
    ),
    24 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4a76057292f94c71793a3e9413062ee6',
      'native_key' => 'paykeeper2.WebMoney.webMoneyId',
      'filename' => 'modSystemSetting/d70036772f9ff363762dfe3541f78093.vehicle',
      'namespace' => 'paykeeper2',
    ),
  ),
);