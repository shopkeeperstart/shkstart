<?php

//if (!$modx->hasPermission('test_action')){
//  return $modx->error->failure($modx->lexicon('access_denied'));
//}

require_once dirname(dirname(__FILE__)).'/model/catalogfill.class.php';
$catalogfill = new catalogfill($modx);

$controllersPath = $modx->getOption('core_path').'components/catalogfill/controllers/';
$catalogfill->config['corePath'] = $modx->getOption('core_path').'components/catalogfill/';
$catalogfill->config['assetsUrl'] = $modx->getOption('assets_url').'components/catalogfill/';
$catalogfill->config['controllersPath'] = $catalogfill->config['corePath'].'controllers/';
$catalogfill->config['connectorUrl'] = $catalogfill->config['assetsUrl'].'connector.php';
$catalogfill->config['impFilesPath'] = 'assets/components/catalogfill/files/';//$modx->getOption('assets_url').'components/catalogfill/files/';
$catalogfill->config['cssUrl'] = $catalogfill->config['assetsUrl'].'css/';
$catalogfill->config['jsUrl'] = $catalogfill->config['assetsUrl'].'js/';

$modx->lexicon->load('catalogfill:default');

$output = '';
$output .= include $controllersPath.'mgr/header.php';
$output .= include $controllersPath.'mgr/index.php';
$output .= '<div id="catalogfill-panel-home-div"></div>';

return $output;

