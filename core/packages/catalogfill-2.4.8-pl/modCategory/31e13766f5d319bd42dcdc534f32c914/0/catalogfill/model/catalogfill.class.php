<?php

/**
 * Catalogfill
 *
 * Catalogfill cart class
 *
 * @author Andchir <andchir@gmail.com>
 * @package catalogfill
 * @version 2.4.8
 */

class Catalogfill {
    
    public $message = '';
    public $categories = array();
    public $articuls = array();
    public $keys = array(array(),array());
    public $values = array();
    public $table_fields = array('id');
    
    /**
     *
     * @param object $modx
     * @param array $config
     */
    public function __construct(modX &$modx, $config = array()){
        
        $this->modx =& $modx;
        
        $this->config = array_merge(array(
            "corePath"=>$this->modx->getOption('core_path').'components/catalogfill/',
            "content_template" => 1,
            "className" => "modResource",
            "packageName" => "",
            "table_name" => $this->modx->config['table_prefix']."site_content",
            "table_prefix" => $this->modx->config['table_prefix'],
            "parent_field" => "parent",
            "context" => "web",
            "parent_id" => 0,
            "imp_testmode" => false,
            "debug" => false,
            "files_import_dir" => $this->modx->getOption('assets_path')."components/catalogfill/files/",
            "files_export_dir" => $this->modx->getOption('assets_path')."components/catalogfill/files/",
            "files_export_dir_url" => $this->modx->getOption('assets_url')."components/catalogfill/files/",
            "files_config_dir" => $this->modx->getOption('core_path')."components/catalogfill/config/",
            "parent_paths" => array(),
            "extension" => false,
            "include_captions" => true,
            "exp_csv_charset" => "UTF-8",
            "exp_delete_file" => true,
            "delete_subcategories" => false,
            "imp_chk_field" => false,
            "imp_chk_tvid_val" => false,
            "imp_if_not_exist" => false,
            "imp_autoalias" => true,
            "imp_update" => true,
            "batch_import" => 0,
            "imp_xml_structure" => "",
            "imp_before_change" => false,
            "imp_empty" => true
        ),$config);
        
        $this->modx->addPackage('catalogfill', $this->config['corePath'].'model/');
        
        //Пакет класса объектов
        if($this->config['className'] == 'modResource'){
            $this->table_fields = array_merge($this->table_fields,array_keys($this->modx->map['modResource']['fields']));
            $this->config['table_name'] = $this->modx->config['table_prefix']."site_content";
        }else{
            $modelpath = $this->modx->getOption('core_path') . 'components/' . $this->config['packageName'] . '/model/';
            $loaded = $this->modx->loadClass($this->config['className'], $modelpath.$this->config['packageName'].'/');
            $added = $this->modx->addPackage($this->config['packageName'], $modelpath);
            if($added){
                $mapFile = $modelpath . $this->config['packageName'] .'/'. $this->modx->config['dbtype'] . '/' .strtolower($this->config['className']). '.map.inc.php';
                if(file_exists($mapFile)){
                    include $mapFile;
                    $metaMap = $xpdo_meta_map[ucfirst($this->config['className'])];
                    $this->config['table_name'] = $this->modx->config['table_prefix'].$metaMap['table'];
                    $this->table_fields = array_merge($this->table_fields,array_keys($metaMap['fields']));
                    
                    if(!isset($this->modx->map[$this->config['className']])){
                        $this->modx->map[$this->config['className']] = array();
                        $this->modx->map[$this->config['className']]['table'] = $metaMap['table'];
                    }
                    
                }
                
            }
            
        }
        
        //Раcширение для страниц
        if($this->config['extension']===false){
            $contentType = $this->modx->getObject('modContentType', 1);
            $this->config['extension'] = $contentType->getExtension();
        }
    
    }
    
    
    /**
    * Проверяет является ли строка сериализованным масивом
    *     
    * @param string $string
    */
    public function is_serialized($string){
        if (!is_string($string)) return false;
        return (@unserialize($string) !== false);
    }
    
    
    /**
    *
    * @param string $str
    * @return boolean
    */
    public function isUTF8($str){
        if($str === mb_convert_encoding(mb_convert_encoding($str, "UTF-32", "UTF-8"), "UTF-8", "UTF-32"))
          return true;
        else
          return false;
    }
    
    /**
     * Возвращает время работы PHP в секундах
     *
     */
    public function microtime_float(){
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    
    /**
     * Сбрасываем auto_increment ID
     * 
     */
    public function clearAutoIncrement(){
        
        $stmt = $this->modx->query("SELECT MAX(`id`) FROM `".$this->config['table_name']."`");
        $id_content_max = (integer) $stmt->fetch(PDO::FETCH_COLUMN);
        $stmt->closeCursor();
        if(!$id_content_max) $id_content_max = 0;
        $this->modx->query("ALTER TABLE `".$this->config['table_name']."` AUTO_INCREMENT = ".($id_content_max+1));
        
    }
    
    
    /**
    *
    * @param float $number
    * @return float
    */
    public function numberFormat($number){
        $output = str_repeat(array(' ',','),array('','.'),$number);
        if($this->config['excepDigitGroup']==true){
            $output = number_format($number,(floor($number) == $number ? 0 : 2),'.',' ');
        }
        return $output;
    }
    
    
    /**
     * Очищает строку от MODX-тегов
     *
     * @param string $string
     * @return string
     */
    public function stripModxTags($string){
        if(isset($this->modx->sanitizePatterns['tags'])) $string = preg_replace($this->modx->sanitizePatterns['tags'], '', $string);
        if(isset($this->modx->sanitizePatterns['tags1'])) $string = preg_replace($this->modx->sanitizePatterns['tags1'], '', $string);
        if(isset($this->modx->sanitizePatterns['tags2'])) $string = preg_replace($this->modx->sanitizePatterns['tags2'], '', $string);
        return $string;
    }
    
    
    
    /**
     * Формирует массив дочерних документов-контейнеров
     *
     * @param integer $id
     * @param integer $depth
     * @param array $options
     * @return array
     *
     */
    public function getChildParentsIds($id= null, $depth= 10,array $options = array()) {
        $children= array ();
        if ($id !== null && intval($depth) >= 1) {
            $id= is_int($id) ? $id : intval($id);
    
            $context = '';
            if (!empty($options['context'])) {
                $this->modx->getContext($options['context']);
                $context = $options['context'];
            }
            $resourceMap = !empty($context) && !empty($this->modx->contexts[$context]->resourceMap) ? $this->modx->contexts[$context]->resourceMap : $this->modx->resourceMap;
            
            if (isset ($resourceMap["{$id}"])) {
                $child_arr = array();
                
                foreach($resourceMap["{$id}"] as $child){
                    if(isset($resourceMap["{$child}"])){
                        array_push($child_arr,$child);
                    }
                }
                unset($child);
                
                $children = $child_arr;
                foreach ($children as $child) {
                    $processDepth = $depth - 1;
                    if ($c = $this->getChildParentsIds($child,$processDepth,$options)) {
                        $children = array_merge($children, $c);
                    }
                }
            }
        }
        return $children;
    }
    
    
    /**
     * Вытаскиваем данные товара
     * 
     * @param array $parent_id_arr
     * @return array
     */
    public function getProducts( $parent_id_arr ){
        
        if(!is_array($parent_id_arr) || count($parent_id_arr)==0) return array(array(),array());
        $products = array();
        $prodIds = array();
        
        $sql = "SELECT * FROM `".$this->config['table_name']."`
        WHERE `".$this->config['parent_field']."` IN (".implode(',',$parent_id_arr).")
        ";
        
        $sql .= $this->prepareWhere( $this->config['imp_content_default']['content'] );
        
        if( !isset( $this->config['imp_content_default']['content']['isfolder'] ) && in_array('isfolder',$this->table_fields) ){
            $sql .= " AND `isfolder` = 0";
        }
        
        $sql .= "\n ORDER BY `".$this->config['parent_field']."` ASC";
        
        $stmt = $this->modx->prepare($sql);
        
        if($stmt && $stmt->execute()){
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                array_push($products,$row);
                array_push($prodIds,$row['id']);
            }
            $stmt->closeCursor();
        }else{
            $this->modx->log(modX::LOG_LEVEL_ERROR, "catalogFill: " . $sql . "\n" . implode(', ',$stmt->errorInfo()));
            $stmt->closeCursor();
        }
        
        return array($products,$prodIds);
    
    }
    
    
    /**
     * Преобразует ассоциативный массив в строку SQL запроса
     *
     * @param array $where_arr
     * @return string
     */
    public function prepareWhere( $where_arr ){
        
        if( empty( $where_arr )  ) return "";
        
        $output = "\n";
        
        foreach( $where_arr as $key => $wh_val ){
            
            if( is_array( $wh_val ) ){
                $output .= " AND `{$key}` IN ('" . implode( "','", $wh_val ) . "')\n";
            }else{
                $output .= " AND `{$key}` = '{$wh_val}'\n";
            }
        }
        
        return $output;
        
    }
    
    
    /**
     * Вытаскиваем данные TV параметров товаров
     * 
     * @param array $idsArr
     * @return array
     */
    public function getTmplVars($idsArr){
        
        $templateVars = array();
        
        if($this->config['className']=='modResource' && count($idsArr)){
            
            $sql = "
            SELECT `tvc`.*, `tv`.`name` FROM ".$this->modx->getTableName('modTemplateVarResource')." `tvc`
            LEFT JOIN ".$this->modx->getTableName('modTemplateVar')." `tv` ON `tvc`.`tmplvarid` = `tv`.`id`
            WHERE `contentid` IN (".implode(',',$idsArr).")
            ";
            
            $stmt = $this->modx->prepare($sql);
            if($stmt && $stmt->execute()){
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $templateVars[$row['contentid']][$row['name']] = $row['value'];
                    $templateVars[$row['contentid']][$row['tmplvarid']] = $row['value'];
                }
            }
            $stmt->closeCursor();
            
        }
        
        return $templateVars;
    }
    
    
    /**
     * Массив названий категорий товара
     * 
     * @param integer $parent_id
     * @return array 
     */
    public function getCategoryNames($parent_id){
        $names = array();
        $count = 0;
        foreach($this->config['content_row'] as $key => $val){
            if($val[1][1] == 'category') $count++;
        }
        
        for($i=1;$i<=$count;$i++){
            //$this->modx->log(modX::LOG_LEVEL_INFO,"-----> $count - $i");
            $tempName = $this->setParentCategoryExport($parent_id,$count-$i);
            if($tempName) $names[] = $tempName;
        }
        
        return $names;
    }
    
    
    /**
     * Функция возвращает имя категории. Используется при экспорте товаров.
     * 
     * @param integer $parent_id
     * @param integer $index
     * @return string
     */
    public function setParentCategoryExport($parent_id, $index){
        $name = '';
        $count = 0;
        
        foreach($this->categories as $key => $val){
            if(in_array($parent_id, array_keys($val))){
                //$this->modx->log(modX::LOG_LEVEL_INFO,"++++++++ $parent_id -> ".implode(',',array_keys($val))." / $index > $count");
                $name = trim($val[$parent_id]);
                //$parent_id = $key;
                if($index > $count){
                    $name = $this->setParentCategoryExport($key,$index-1);//$this->setParentCategoryExport($parent_id,$index-1);
                }
                $count++;
            }
        }
        
    //    ob_start();
    //    var_dump($name);
    //    $debug_info = ob_get_contents();
    //    ob_end_clean();
    //    $this->modx->log(modX::LOG_LEVEL_INFO,$debug_info);
        
        return $name;
    }
    
    
    /**
     * Собираем массив дочерних категорий (ID + названия)
     * 
     * @param int $parent_id
     * @return array
     */
    public function getChildCategories($parent_id){
        $categories = array();
        $categories_id = array();
        $childrens = $this->modx->getChildIds($parent_id,10,array('context'=>$this->config['context']));
        $childrens = is_array($childrens) ? array_values($childrens) : array();
        
        if(count($childrens)>0){
            $c_query = $this->modx->newQuery('modResource');
            $c_query->where(array('id:IN' => $childrens));
            //$c_query->andCondition(array('isfolder'=>1));
            $c_query->select($this->modx->getSelectColumns('modResource','modResource','',array('id','parent','pagetitle')));
            $resources = $this->modx->getIterator('modResource',$c_query);
            
            foreach ($resources as $key => $resource) {
                $categories[$resource->get('parent')][$resource->get('id')] = trim($resource->get('pagetitle'));
            }
        }
        return $categories;
    }
    
    
    /**
     * Собираем массив ID дочерних категорий
     * 
     * @param int $parent_id
     * @return array
     */
    public function getChildCategoriesID( $parent_id ){
        $categories = array();
        $childrens = $this->modx->getChildIds($parent_id,10,array('context'=>$this->config['context']));
        $childrens = is_array($childrens) ? array_values($childrens) : array();
        
        if(count($childrens)>0){
            $c_query = $this->modx->newQuery('modResource');
            $c_query->where(array('id:IN' => $childrens));
            $c_query->andCondition(array('isfolder'=>1));
            $c_query->select($this->modx->getSelectColumns('modResource','modResource','',array('id')));
            $resources = $this->modx->getIterator('modResource',$c_query);
            foreach ($resources as $key => $resource) {
                $categories[] = $resource->get('id');
            }
        }
        
        array_push( $categories, $parent_id );
        
        return $categories;
    }
    
    
    /**
     * Экспорт товаров в CSV файл
     * 
     * @param int $parent_id
     */
    public function csv_export($parent_id){
        
        $out = array('count'=>0,'filepath'=>'','filename'=>'');
        
        //$time_start = $this->microtime_float();
        
        $parent_folder = $this->modx->getObject('modResource',$parent_id);
        $this->config['context'] = $parent_folder->get('context_key');
        
        if($this->config['include_categories']){
            $this->categories = $this->getChildCategories($parent_id);
            $parent_id_arr = $this->getChildCategoriesID($parent_id);
        }else{
            $this->categories = array();
            $parent_id_arr = array($parent_id);
        }
        
        list($products,$prodIds) = $this->getProducts( $parent_id_arr );
        if(count($products)==0){
            $this->message = "В выбранной категории нет товаров.";
            return false;
        }
        
        $tmplVars = $this->getTmplVars($prodIds);
        
        $filename = "exp_".date("d_m_y__H_i_s").".csv";
        $filepath = $this->config['files_export_dir'].$filename;
        $fp = fopen($filepath, 'x+');
        $out['filename'] = $filename;
        $out['filepath'] = $this->config['files_export_dir_url'].$filename;
    
        //write header row
        if($this->config['include_captions']){
            $line = array();
            
            if(strtoupper($this->config['exp_csv_charset']) != 'UTF-8'){
                foreach($this->config['content_row'] as $val) $line[] = iconv('UTF-8',$this->config['exp_csv_charset'],$val[0]);
            }else{
                foreach($this->config['content_row'] as $val) $line[] = $val[0];
            }
            
            fputcsv($fp, $line, ';', '"');
        }
        unset($val,$line);
    
        foreach($products as $key => $val){
            $line = array();
            $categoryNames = $this->getCategoryNames($val[$this->config['parent_field']]);
            
            $val = filter_export($val);
            $tv_arr = isset($tmplVars[$val['id']]) ? filter_export($tmplVars[$val['id']],$val['id']) : array();
            foreach($this->config['content_row'] as $k => $v){
                if($v[1][1]=='content'){
                    $c_value = isset($val[$v[1][0]]) ? trim($val[$v[1][0]]) : '';
                }else if($v[1][1]=='tv'){
                    $c_value = isset($tv_arr[$v[1][0]]) ? trim($tv_arr[$v[1][0]]) : '';
                }else if($v[1][1]=='category'){
                    $c_value = isset($categoryNames[$k]) ? $categoryNames[$k] : '';//isset($categories[$val['parent']]) ? $categories[$val['parent']] : '';
                }
                if(strtoupper($this->config['exp_csv_charset']) != 'UTF-8') $c_value = iconv('UTF-8',$this->config['exp_csv_charset'],$c_value);
                if($this->config['exp_strip_tags']) $c_value = strip_tags($c_value);
                
                $line[] = $c_value;
            }
            fputcsv($fp, $line, ';', '"');
            $out['count']++;
        }
        fclose($fp);
        unset($fp);
        
        //$time_end = $this->microtime_float();
        //$time = $time_end - $time_start;
        //var_dump($time);
        
        return $out;
    }
    
    
    /**
     * Экспорт товаров в XLS файл
     * 
     * @param int $parent_id
     */
    public function xls_export($parent_id, $xls_type='Excel5'){
        require_once realpath(dirname(__FILE__)).'/PHPExcel.php';
        
        $out = array('count'=>0,'filepath'=>'','filename'=>'');
        
        $parent_folder = $this->modx->getObject('modResource',$parent_id);
        $this->config['context'] = $parent_folder->get('context_key');
        
        if($this->config['include_categories']){
            $this->categories = $this->getChildCategories($parent_id);
            $parent_id_arr = $this->getChildCategoriesID($parent_id);
        }else{
            $this->categories = array();
            $parent_id_arr = array($parent_id);
        }
        
        list($products,$prodIds) = $this->getProducts($parent_id_arr);
        
        if(count($products)==0){
            $this->message = "В выбранной категории нет товаров.";
            return false;
        }
        $tmplVars = $this->getTmplVars($prodIds);
        
        $filename = "exp_".date("d_m_y__H_i_s").($xls_type == 'Excel2007' ? ".xlsx" : ".xls");
        $filepath = $this->config['files_export_dir'].$filename;
        $out['filename'] = $filename;
        $out['filepath'] = $this->config['files_export_dir_url'].$filename;
        
        $objPHPExcel = new PHPExcel();
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    
        //write header row
        if($this->config['include_captions']){
            foreach($this->config['content_row'] as $key => $val){
                $objWorksheet->setCellValueByColumnAndRow($key,1,$val[0]);
                $objWorksheet->getStyleByColumnAndRow($key,1,$val[0])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D3D3D3');
                //$objWorksheet->getColumnDimension($key)->setWidth(200);//setAutoSize(true);
            }
        }
        unset($key,$val);
        
        $start = $this->config['include_captions'] ? 2 : 1;
        foreach($products as $key => $val){
            
            $categoryNames = $this->getCategoryNames($val[$this->config['parent_field']]);
            
            $val = filter_export($val);
            $tv_arr = isset($tmplVars[$val['id']]) ? filter_export($tmplVars[$val['id']],$val['id']) : array();
            foreach($this->config['content_row'] as $k => $v){
                if($v[1][1]=='content'){
                    $cell = isset($val[$v[1][0]]) ? trim($val[$v[1][0]]) : '';
                }else if($v[1][1]=='tv'){
                    $cell = isset($tv_arr[$v[1][0]]) ? trim($tv_arr[$v[1][0]]) : '';
                }else if($v[1][1]=='category'){
                    $cell = isset($categoryNames[$k]) ? $categoryNames[$k] : '';// $this->setParentCategoryExport($val['parent'],$categoryCount-$k-1);
                }
                $cell = str_replace('=','\=',$cell);
                
                $objWorksheet->setCellValueByColumnAndRow($k,$key+$start,$cell);
                
                //$objWorksheet->getColumnDimension($k)->setAutoSize(true);
            }
            $out['count']++;
        }
        
        $objPHPExcel->setActiveSheetIndex(0);
    
        if($xls_type=='Excel2007'){
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        }else{
            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        }
        
        $objWriter->save($filepath);
        
        return $out;
        
    }
    
    
    /**
     * Импорт данных из CSV файла
     * 
     * @param string $file_name 
     */
    public function csv_import($file_name, $skip=0){
        
        $out = array('pos'=>0,'lines_count'=>0);
        
        //очистка категории
        if($skip==0 && !$this->config['imp_testmode'] && $this->config['imp_update'] && $this->config['imp_chk_field']===false && $this->config['imp_chk_tvid_val']===false){
            $this->cleanParent($this->config['parent_id']);
        }
        
        if($this->config['include_categories']){
            $this->categories = $this->getChildCategories($this->config['parent_id']);
        }
    
        $csv_file = $this->config['files_import_dir'].$file_name;
        $lines_count = count(file($csv_file));
        
        $out['lines_count'] =  $lines_count;
        if($this->config['include_captions']) $out['lines_count']--;
        
        if(file_exists($csv_file)){
            
            $fileHandler = fopen($csv_file, "r");
            $count = 0;
            
            while ($line = fgetcsv($fileHandler, filesize($csv_file), ";")){
                
                $count++;
                if($this->config['include_captions'] && $count == 1){ continue; }
                
                $tempArr = array();
                
                if($out['pos'] < $skip){ $out['pos']++; continue; }
                
                foreach($line as $key => $val){
                    if(isset($this->config['content_row'][$key][1])){
                        $c_type = $this->config['content_row'][$key][1];
                        $c_value = $this->isUTF8($val) ? trim($val) : trim(iconv('cp1251','UTF-8',$val));
                        if($c_type[1]=='category'){
                            $tempArr['category'][] = array($c_type[0],$c_value);
                        }else{
                            if(!empty($c_value) || (empty($c_value) && $this->config['imp_empty']))
                                $tempArr[$c_type[1]][$c_type[0]] = $c_value;
                        }
                    }
                }
                unset($key,$val);
                
                //alias
                if(!isset($tempArr['content']['alias']) && $this->config['imp_autoalias'] && isset($tempArr['content']['pagetitle'])){
                    $tempArr['content']['alias'] = $this->makeAlias($tempArr['content']['pagetitle']);
                }
                if(in_array('context_key',$this->table_fields)) $tempArr['content']['context_key'] = $this->config['context'];
                
                $tempArr['content'] = array_merge($this->config['imp_content_default']['content'],$tempArr['content']);
                
                if(!empty($tempArr['tv']))
                    $tempArr['tv'] = $this->config['imp_content_default']['tv'] + $tempArr['tv'];
                else
                    $tempArr['tv'] = array();
                
                //Пропускаем через функцию-фильтр
                $insertArr = filter_import($tempArr);
                
                $insert = '';
                if(is_array($insertArr) && ( !empty($insertArr['content']['pagetitle']) || ( empty($insertArr['content']['pagetitle']) && $this->config['imp_update'] ) )){
                    
                    //parent
                    if(!isset($insertArr['content'][$this->config['parent_field']])){
                        if(isset($insertArr['category'])){
                            $insertArr['content'][$this->config['parent_field']] = $this->setParentCategory($insertArr['category'], $this->config['parent_id']);
                        }else{
                            $insertArr['content'][$this->config['parent_field']] = $this->config['parent_id'];
                        }
                    }
                    
                    $insert = $this->insertToDB($insertArr);
                    
                }
                
                $out['pos']++;
                
                //Если есть ограничение, выходим из цикла
                if($this->config['batch_import'] && ($out['pos'] == $skip + $this->config['batch_import'])){
                    
                    break;
                    
                }
                
            }
            fclose($fileHandler);
    
        }
        
        return $out;
        
    }
    
    
    /**
     * Импорт данных из XLS файла
     * 
     * @param string $file_name
     */
    public function xls_import($file_name, $xls_type='Excel5', $skip=0){
        
        $out = array('pos'=>0,'lines_count'=>0);
        
        require_once realpath(dirname(__FILE__)).'/PHPExcel.php';
    
        $file_path = $this->config['files_import_dir'].$file_name;
        
        //очистка категории
        if($skip==0 && !$this->config['imp_testmode'] && $this->config['imp_update'] && $this->config['imp_chk_field']===false && $this->config['imp_chk_tvid_val']===false){
            $this->cleanParent($this->config['parent_id']);
        }
        
        if($this->config['include_categories']){
            $this->categories = $this->getChildCategories($this->config['parent_id']);
        }
        
        if(file_exists($file_path)){
    
            if($xls_type=='Excel2007'){
                $objReader = new PHPExcel_Reader_Excel2007();
            }else{
                $objReader = new PHPExcel_Reader_Excel5();
            }
            $objPHPExcel = $objReader->load($file_path);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            //$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            
            $out['lines_count'] = $highestRow;
            
            if($this->config['include_captions'] && $skip==0) $skip ++;
            $start = $skip+1;
            $out['pos'] = $skip;
            
            $count = 0;
            
            for ($row = $start; $row <= $highestRow; $row++) {
                
                $count++;
                
                $tempArr = array();
                
                foreach($this->config['content_row'] as $col => $v){
                    $c_type = $v[1];
                    $c_value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                    $c_value = trim(str_replace('\=','=',$c_value));
                    if(!$this->isUTF8($c_value)) $c_value = iconv('cp1251','UTF-8',$c_value);
                    if($c_type[1]=='category'){
                        $tempArr['category'][] = array($c_type[0],trim($c_value));
                    }else{
                        if(!empty($c_value) || (empty($c_value) && $this->config['imp_empty']))
                            $tempArr[$c_type[1]][$c_type[0]] = $c_value;
                    }
                }
                
                //alias
                if(!isset($tempArr['content']['alias']) && $this->config['imp_autoalias'] && isset($tempArr['content']['pagetitle'])){
                    $tempArr['content']['alias'] = $this->makeAlias($tempArr['content']['pagetitle']);
                }
                if(in_array('context_key',$this->table_fields)) $tempArr['content']['context_key'] = $this->config['context'];
                
                $tempArr['content'] = array_merge($this->config['imp_content_default']['content'],$tempArr['content']);
                
                if(!empty($tempArr['tv']))
                    $tempArr['tv'] = $this->config['imp_content_default']['tv'] + $tempArr['tv'];
                else
                    $tempArr['tv'] = array();
                
                //Пропускаем через функцию-фильтр
                $insertArr = filter_import($tempArr);
                
                if(is_array($insertArr) && ( !empty($insertArr['content']['pagetitle']) || ( empty($insertArr['content']['pagetitle']) && $this->config['imp_update'] ) )){
                    
                    //parent
                    if(!isset($insertArr['content'][$this->config['parent_field']])){
                        if(isset($insertArr['category'])){
                            $insertArr['content'][$this->config['parent_field']] = $this->setParentCategory($insertArr['category'], $this->config['parent_id']);
                        }else{
                            $insertArr['content'][$this->config['parent_field']] = $this->config['parent_id'];
                        }
                    }
                    
                    $insert = $this->insertToDB($insertArr);
                    
                }
                
                $out['pos']++;
                
                //$this->modx->log(modX::LOG_LEVEL_ERROR, "skip = $skip, pos = ".$out['pos']." ".$insertArr['content']['pagetitle']);
                
                //Если есть ограничение, выходим из цикла
                if($this->config['batch_import'] && ($out['pos'] == $skip + $this->config['batch_import'])){
                    
                    break;
                    
                }
                
            }
    
        }
        
        return $out;
        
    }
    
    
    /**
     * Подготавливает родительскую категорию для импорта в неё товаров
     * 
     */
    public function importPrepareParent($parent_id){
        
        $output = true;
        
        if($parent_id && is_numeric($parent_id)){
            
            //если главная категория не является контейнером, делаем её таковой
            $parent_folder = $this->modx->getObject('modResource',$parent_id);
            if(!$parent_folder->get('isfolder')){
                $parent_folder->set('isfolder',true);
                $parent_folder->save();
            }
            $this->config['context'] = $parent_folder->get('context_key');
            $this->config['parent_id'] = $parent_id;
            
        }else{
            
            $context_name = $parent_id;
            $is_context = $this->modx->getCount('modContext',array('key'=>$context_name));
            
            if($is_context){
                $this->config['context'] = $context_name;
                $this->config['parent_id'] = 0;
            }else{
                $output = false;
            }
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Записываем данные в таблицы БД
     * 
     * @param array $insertArr
     * @return boolean
     */
    public function insertToDB($insertArr){
        
        $output = false;
        $upd_id = 0;
        $is_update = false;
        
        if($this->config['imp_testmode']){
            
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($insertArr,true));
            return $output;
            
        }
        
        $insertArr['content'] = $this->prepareFields($insertArr['content']);
        
        //Обновление по значению TV. Ищем товар.
        if($this->config['imp_update'] && $this->config['imp_chk_tvid_val']){
            
            $is_update = true;
            
            if(isset($insertArr['tv'][$this->config['imp_chk_tvid_val']])){
                
                $sql = "SELECT `id`, `tmplvarid`, `contentid` FROM ".$this->modx->getTableName('modTemplateVarResource')."
                WHERE `tmplvarid` = '".intval($this->config['imp_chk_tvid_val'])."'
                AND `value` = :value
                LIMIT 1
                ";
                
                $stmt = $this->modx->prepare($sql);
                $stmt->bindValue(':value', $insertArr['tv'][$this->config['imp_chk_tvid_val']], PDO::PARAM_STR);
                
                if($stmt && $stmt->execute()){
                    
                    $result = $stmt->fetchAll();
                    $stmt->closeCursor();
                    
                    if(is_array($result[0])) $result = current($result);
                    if($result && !empty($result['contentid'])){
                        $upd_id = $result['contentid'];
                    }
                    
                }else{
                    $this->modx->log(modX::LOG_LEVEL_ERROR, "catalogFill: ".implode(', ',$stmt->errorInfo()));
                    $stmt->closeCursor();
                }
                
            }
            
        //Обновление по полю. Ищем товар.
        }else if($this->config['imp_update'] && $this->config['imp_chk_field']){
            
            $is_update = true;
            
            $sql = "SELECT `id` FROM `".$this->config['table_name']."`
            WHERE `".$this->config['imp_chk_field']."` = :".$this->config['imp_chk_field']."
            AND `".$this->config['parent_field']."` = '".$insertArr['content'][$this->config['parent_field']]."'
            LIMIT 1
            ";
            
            $stmt = $this->modx->prepare($sql);
            $stmt->bindValue(':'.$this->config['imp_chk_field'], $insertArr['content'][$this->config['imp_chk_field']], PDO::PARAM_STR);
            
            if($stmt && $stmt->execute()){
                
                $result = $stmt->fetchAll();
                $stmt->closeCursor();
                
                if(is_array($result[0])) $result = current($result);
                if($result && !empty($result['id'])){
                    $upd_id = $result['id'];
                }
                
            }
            
        }
        
        //Обновление товара
        if($is_update && $upd_id){
            
            $sql = "UPDATE `".$this->config['table_name']."` SET\n";
            
            //if(  ) ){
            //    unset( $insertArr['content'][$this->config['parent_field']] );
            //}
            
            foreach(array_keys($insertArr['content']) as $fname){
                $sql .= " `{$fname}` = ?,\n";
            }
            
            $sql = substr($sql,0,-2);
            
            $sql .= "\nWHERE `id` = '{$upd_id}' LIMIT 1";
            
            $stmt = $this->modx->prepare($sql);
            if($stmt && $stmt->execute(array_values($insertArr['content']))){
                
                $stmt->closeCursor();
                $this->updateTVs($insertArr['tv'],$upd_id);
                $output = true;
                
            }else{
                
                $this->modx->log(modX::LOG_LEVEL_ERROR, "catalogFill: ".implode(', ',$stmt->errorInfo()));
                $stmt->closeCursor();
                $output = false;
                
            }
            
            $output = true;
            
        //Добавление товара
        }else if(!$this->config['imp_update'] || ($is_update && $this->config['imp_if_not_exist'])){
            
            if(in_array('uri',$this->table_fields)) $insertArr['content']['uri'] = $this->getAliasPath($insertArr['content']);
            
            $sql = "
            INSERT INTO `".$this->config['table_name']."`
            (`".implode("`, `",array_keys($insertArr['content']))."`)
            VALUES (".implode(",",array_fill(0,count($insertArr['content']),'?')).")
            ";
            
            $stmt = $this->modx->prepare($sql);
            
            if($stmt && $stmt->execute(array_values($insertArr['content']))){
                
                $prod_id = $this->modx->lastInsertId();
                $stmt->closeCursor();
                
                $this->insertTVs($insertArr['tv'],$prod_id);
                
                $output = true;
                
            }else{
                
                $this->modx->log(modX::LOG_LEVEL_ERROR, "catalogFill: ".implode(', ',$stmt->errorInfo()));
                $stmt->closeCursor();
                $output = false;
                
            }
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Убирает лишние поля из массива
     *
     * @param array $fields
     */
    public function prepareFields($fields){
        
        $diff_fields = array_diff(array_keys($fields),$this->table_fields);
        
        if(!empty($diff_fields)){
            foreach($diff_fields as $f_key){
                unset($fields[$f_key]);
            }
        }
        
        return $fields;
        
    }
    
    
    /**
     * Возвращает полную URL страницы
     *
     * @param array $fields
     */
    public function getAliasPath($fields){
        
        $output = '';
        
        if(!in_array($fields['parent'],array_keys($this->config['parent_paths']))){
            
            $ctx = $this->modx->getContext($this->config['context']);
            $aliasPath = $ctx->getResourceURI($fields['parent']);
            if($aliasPath) $this->config['parent_paths'][$fields['parent']] = $aliasPath;
            
            if( empty( $aliasPath ) ){
                
                $resource = $this->modx->getObject('modResource',$fields['parent']);
                $aliasPath = $resource->getAliasPath();
                
                if($aliasPath) $this->config['parent_paths'][$fields['parent']] = $aliasPath;
                
                if( empty( $aliasPath ) ){
                    $this->modx->log( modX::LOG_LEVEL_ERROR, 'Catalogfill: alias error - ' . print_r($this->config['parent_paths'],true) . "\n\n" . print_r($fields,true) );
                    //exit;
                }
            }
            
        }
        
        $output = $this->config['parent_paths'][$fields['parent']].$fields['alias'].$this->config['extension'];
        
        return $output;
        
    }
    
    
    /**
     * Записывает значения TV в БД
     * 
     * @param array $tv_arr
     * @param integer $prod_id
     * @param boolean $replace
     * @return boolean
     */
    public function insertTVs($tv_arr, $prod_id){
        
        $output = false;
        
        foreach($tv_arr as $key => $val){
            if(!empty($val)){
                
                $sql = "INSERT INTO ".$this->modx->getTableName('modTemplateVarResource')."
                (`tmplvarid`,`contentid`,`value`)
                VALUES (?,?,?)
                ";
                
                $stmt = $this->modx->prepare($sql);
                
                if($stmt){
                    $output = $stmt->execute(array($key,$prod_id,$val));
                }
                $stmt->closeCursor();
                
            }
        }
        
        return $output;
        
    }
    
    
    /**
     * Обновляет значения TV
     * 
     * @param array $tv_arr
     * @param integer $prod_id
     * @return boolean
     */
    public function updateTVs($tvs, $prod_id){
        
        $output = false;
        
        foreach($tvs as $tmplvarid => $val){
            
            //Обновляем или добавляем запись (sql replace)
            if(strlen($val)>0){
                
                $sql = "SELECT * FROM ".$this->modx->getTableName('modTemplateVarResource')."
                WHERE `tmplvarid` = '{$tmplvarid}' AND `contentid` = '{$prod_id}'
                LIMIT 1
                ";
                
                $stmt = $this->modx->prepare($sql);
                if($stmt && $stmt->execute()){
                    
                    $result = $stmt->fetchAll();
                    $stmt->closeCursor();
                    
                    //Обновляем
                    if(!empty($result)){
                        
                        $u_sql = "UPDATE ".$this->modx->getTableName('modTemplateVarResource')."
                        SET `value` = :value
                        WHERE `tmplvarid` = '{$tmplvarid}' AND `contentid` = '{$prod_id}'
                        LIMIT 1
                        ";
                        
                        $stmt = $this->modx->prepare($u_sql);
                        $stmt->bindValue(':value', $val);
                        if($stmt && $stmt->execute()){
                            $output = true;
                        }
                        $stmt->closeCursor();
                        
                    }
                    //Добавляем новую запись
                    else{
                        
                        $this->insertTVs(array($tmplvarid => $val),$prod_id);
                        
                    }
                    
                }else{
                    $stmt->closeCursor();
                }
                
            //Удаляем запись
            }else{
                
                $del_sql = "DELETE FROM ".$this->modx->getTableName('modTemplateVarResource')."
                WHERE `tmplvarid` = '{$tmplvarid}' AND `contentid` = '{$prod_id}'
                ";
                
                $stmt = $this->modx->prepare($del_sql);
                if($stmt && $stmt->execute()){
                    $output = true;
                    $stmt->closeCursor();
                }
                
            }
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Устанавливаем ID категории для товара и создаем категории, если нужно
     * 
     * @param int $parent_id
     * @return int
     */
    public function setParentCategory($category_name,$parent_id){
        
        $categoryId = $parent_id;
        
        if(!$this->config['imp_testmode']){
            foreach($category_name as $key => $val){
                
                if(empty($val[1])) continue;
                if(!isset($this->categories[$parent_id])) $this->categories[$parent_id] = array();
                
                if(!in_array($val[1],$this->categories[$parent_id]) && (!$this->config['imp_update'] || $this->config['imp_if_not_exist'])){
                    
                    $alias = $this->config['imp_autoalias'] ? $this->makeAlias($val[1]) : '';
                    
                    $resource = $this->modx->newObject('modResource');
                    $resource->fromArray(array(
                        'context_key' => isset($this->config['imp_content_default']['content']['context_key']) ? $this->config['imp_content_default']['content']['context_key'] : $this->config['context'],
                        'pagetitle' => $val[1],
                        'alias' => $alias,
                        'template' => $val[0],
                        'parent' => $parent_id,
                        'isfolder' => 1,
                        'published' => 1
                    ));
                    $resource->save();
                    
                    /* empty cache */
                    /*
                    if( $this->modx->config['use_alias_path'] ){
                        $this->modx->cacheManager->refresh(array(
                            'db' => array(),
                            'auto_publish' => array('contexts' => array($this->config['context'])),
                            'context_settings' => array('contexts' => array($this->config['context'])),
                            'resource' => array('contexts' => array($this->config['context']))
                        ));
                    }
                    */
                    $categoryId = $resource->get('id');
                    
                    $this->categories[$parent_id][$categoryId] = trim($val[1]);
                    $parent_id = $categoryId;
                    
                }else{
                    $parent_id = array_search($val[1], $this->categories[$parent_id]);
                }
                
            }
        }
        
        return $parent_id;
    }
    
    
    /**
     * Очищаем категорию товаров
     * 
     * @param int $parent_id
     */
    public function cleanParent($parent_id){
        
        $out = array();
        $parentIds = array();
        
        if($this->config['include_categories']){
            $parentIds = $this->getChildCategoriesID($parent_id);
            
            //удаляем категории
            if(count($parentIds)>0 && $this->config['delete_subcategories']){
                $res_res = $this->modx->removeCollection('modResource', array("id:IN" => $parentIds));
                $res_tv = $this->modx->removeCollection('modTemplateVarResource', array("contentid:IN" => $parentIds));
            }
        }
        array_push($parentIds, $parent_id);
        
        $ids_arr = array();
        
        //собираем ID товаров   
        $sql = '
        SELECT `id` FROM `'.$this->config['table_name'].'`
        WHERE `'.$this->config['parent_field'].'` IN ('.implode(',',$parentIds).')
        ';
        if(isset($this->config['imp_content_default']['content']['template'])) $sql .= " AND `template` = '".$this->config['imp_content_default']['content']['template']."' ";
        if(!$this->config['delete_subcategories'] && in_array('isfolder',$this->table_fields)) $sql .= " AND `isfolder` = 0 ";
        
        $stmt = $this->modx->prepare($sql);
        if ($stmt && $stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $ids_arr[] = $row['id'];
            }
            $stmt->closeCursor();
        }
        
        if(count($ids_arr)>0){
            //удаляем товары
            $res_res = $this->modx->removeCollection($this->config['className'], array("id:IN" => $ids_arr));
            //удаляем их TV
            if($this->config['className'] == 'modResource') $res_tv = $this->modx->removeCollection('modTemplateVarResource', array("contentid:IN" => $ids_arr));
        }
        
        //clear auto_increment
        $this->clearAutoIncrement();
        
        return $out;
    
    }
    
    
    /**
     * Изменение значений полей ресурсов
     * 
     * @param int $parent_id
     *
     */
    public function fieldsValueChange($parent_id=false){
        
        if(!$parent_id) return false;
        $output = false;
        
        $options = $this->config['imp_before_change'];
        if($options){
            
            $options_arr = $this->modx->fromJSON($options);
            
            if(is_array($options_arr[0]) && count($options_arr[0])>0){
                
                if(empty($options_arr[1])) $options_arr[1] = array();
                
                $parentIds = $this->getChildCategoriesID($parent_id);
                array_push($parentIds, $parent_id);
                
                $udate_tv = strpos(implode(',',array_keys($options_arr[0])),'tv.') !== false;
                $where_tv = strpos(implode(',',array_keys($options_arr[1])),'tv.') !== false;
                
                //Если нужно изменить значение TV
                if($udate_tv){
                    
                    $tv_name = substr(current(array_keys($options_arr[0])),3);
                    
                    $sql = "
                    UPDATE ".$this->modx->getTableName('modTemplateVarResource')." `tvc`,
                    ".$this->modx->getTableName('modResource')." `sc`,
                    ".$this->modx->getTableName('modTemplateVar')." `tv`
                    SET `tvc`.`value` = '".current(array_values($options_arr[0]))."' ";
                    
                    $sql .= " WHERE `sc`.`id` = `tvc`.`contentid` AND `tv`.`id` = `tvc`.`tmplvarid` AND `tv`.`name` = '{$tv_name}' AND ";
                    
                //Изменение значения поля ресурса
                }else{
                    
                    $sql = "
                    UPDATE `".$this->config['table_name']."` `sc`
                    ";
                    
                    if($where_tv){
                        $sql .= ",\n".$this->modx->getTableName('modTemplateVarResource')." `tvc`,
                        ".$this->modx->getTableName('modTemplateVar')." `tv`
                        ";
                    }
                    
                    $sql .= "SET ";
                    foreach($options_arr[0] as $key => $val){
                        $sql .= "
                        `sc`.`{$key}` = '{$val}',";
                    }
                    $sql = substr($sql,0,-1)." WHERE ";
                    
                    if($where_tv){
                        $sql .= " `sc`.`id` = `tvc`.`contentid` AND `tv`.`id` = `tvc`.`tmplvarid` AND ";
                    }
                    
                }
                
                $sql .= "
                `sc`.`".$this->config['parent_field']."` IN (".implode(',',$parentIds).")";
                
                if(in_array('isfolder',$this->table_fields)) $sql .= " AND `sc`.`isfolder` = 0 ";
                
                foreach($options_arr[1] as $key => $val){
                    if(substr($key,0,3)=='tv.'){
                        $sql .= "
                        AND (`tv`.`name` = '".substr($key,3)."' AND `tvc`.`value` = '{$val}')
                        ";
                    }else{
                        $query->where(array('`modResource`.`'.$key.'`:='=>$val));
                        $sql .= "
                        AND `sc`.`{$key}` = '{$val}'
                        ";
                    }
                }
                
                $stmt = $this->modx->prepare($sql);
                $affected = $stmt->execute();
                $stmt->closeCursor();
                
                $output = true;
                
            }
            
        }
        
        return $output;
        
    }
    
    
    /**
     * Очищаем директорию от файлов
     * 
     * @param string $dir
     */
    public function cleanDir($dir_path){
        $out = array('count'=>0);
        $dir = opendir(realpath($dir_path));
        while($f = readdir($dir)){
            if(is_file($dir_path.$f)){
                unlink($dir_path.$f);
                $out['count']++;
            }
        }
        closedir($dir);
        return $out;
    }
    
    
    /**
     * Составляем список конфигурационных файлов
     * 
     * @return array
     */
    public function configList(){
        $output = array();
        $dir = opendir(realpath($this->config['files_config_dir']));
        while($f = readdir($dir)){
            if(is_file($this->config['files_config_dir'].$f)) $output[] = array('id'=>substr($f,0,-4),'name'=>substr($f,0,-4));
        }
        closedir($dir);
        return $output;
    }
    
    
    /**
     * Составляем список файлов для импорта
     * 
     * @return array
     */
    public function filesList(){
        $output = array();
        $dir = opendir(realpath($this->config['files_import_dir']));
        while($f = readdir($dir)){
            if(is_file($this->config['files_import_dir'].$f)) $output[] = array('id'=>$f,'name'=>$f);
        }
        closedir($dir);
        return $output;
    }
    
    
    /**
     * Создание псевдонима в транслите
     * 
     * @param string $str
     * @return string
     */
    public function makeAlias($str){
        $str = mb_strtolower($str, mb_detect_encoding($str));
        $str = strtr($str, 
            array(
                " "=>"-", "."=>"_", ","=>"", "$"=>"", "?"=>"", "!"=>"", "\""=>"", "'"=>"", "/"=>"",
                "\\"=>"", "("=>"", ")"=>"", "{"=>"", "}"=>"", "["=>"", "]"=>"", "+"=>"p", "&"=>"",
                "?"=>"", "!"=>"", "«"=>"", "»"=>"", "%"=>""
            )
        );
        if($this->config['imp_autoalias'] !== 'notranslit'){
          $str = strtr($str, 
              array(
                  "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z",
                  "и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
                  "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch",
                  "ь"=>"","ъ"=>"","ы"=>"y","э"=>"e","ю"=>"yu","я"=>"ya"
              )
          );
        }
        return $str;
    }
    
    
    #########################################
    # XML
    #########################################
    
    /**
     * Импорт по XML файлу
     * 
     * @param string $file_name
    */
    public function xml_import($file_name, $skip=0){
        
        $out = array('pos'=>0,'lines_count'=>0);
        
        $file_path = $this->config['files_import_dir'].$file_name;
        $insertArr = array();
        
        if($this->config['include_categories']){
            $this->categories = $this->getChildCategories($this->config['parent_id']);
        }
        
        if(file_exists($file_path)){
            
            $xml = @simplexml_load_file($file_path);
            if(!$xml) exit('Ошибка чтения файла.');
            
            $xml_conf = simplexml_load_string($this->config['imp_xml_structure']);
            
            $xml_group = !empty($this->config['imp_xml_itemsparent_path']) ? current($xml->xpath($this->config['imp_xml_itemsparent_path'])) : $xml->children();
            
            $out['lines_count'] = count($xml_group);
            
            foreach($xml_group as $g_key => $group){
                
                $tempArr = array();
                $item = $this->parceXML($group);
                
                //echo '<pre>'; print_r($this->keys); print_r($this->values); echo '</pre>';
                //exit;
                
                foreach($this->values as $k => $v){
                    
                    if(isset($this->keys[1][$k]) && substr($this->keys[1][$k],0,11)=='@attributes'){
                        
                        $p_name = substr($this->keys[1][$k],12);
                        $key = (string)$xml_conf[$p_name];
                        $tempArr[$key] = $v;
                        
                    }else{
                        
                        $xpath = $this->keys[1][$k];
                        
                        //ищем название параметра (поля) по конфиг. файлу
                        $key = (string) current($xml_conf->xpath($xpath));
                        
                        if(isset($tempArr[$key]) && is_array($tempArr[$key]))
                            $tempArr[$key][] = $v;
                        else if(isset($tempArr[$key]))
                            $tempArr[$key] = array($tempArr[$key],$v);
                        else
                            $tempArr[$key] = $v;
                        
                    }
                    
                }
                
                //Добавляем данные в массив артикулов
                //$this->articuls[$tempArr['tv39']] = array((is_array($tempArr['_temp2']) ? $tempArr['_temp2'] : array($tempArr['_temp2'])),(is_array($tempArr['tv4']) ? $tempArr['tv4'] : array($tempArr['tv4'])));
                
                $tempArr = filter_import(array($tempArr), $this);
                if(count($tempArr)>0) $insertArr = array_merge($insertArr,$tempArr);
                
                //print_r($tempArr);
                //echo '<pre>'; print_r($insertArr); echo '</pre>'; exit;
                
                //Обнуляем старые значения
                $this->keys = array(array(),array());
                $this->values = array();
                
                $out['pos']++;
                
            }
            
            //Добавляем или обновляем в БД
            $imp_count = $this->xml_groupInsertToDB($this->config['parent_id'], $insertArr);
            
            //echo '<pre>'; print_r($insertArr); echo '</pre>'; exit;
            
        }
        
        return $out;
        
    }
    
    
    /**
     * Формирует нужную структуру массива для отправки в БД
     * 
     * @param integer $parent_id
     * @param array $inputArr
     */
    public function xml_groupInsertToDB($parent_id, $inputArr){
        
        if($this->config['imp_testmode']){
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($inputArr,true));
            return false;
        }
        
        foreach($inputArr as $key => $data){
            
            $tempArr = array('tv'=>array(), 'content'=>array());
            
            foreach($data as $k => $v){
                
                if(substr($k,0,2)=='tv'){
                    $tempArr['tv'][substr($k,2)] = is_array($v) ? current($v) : $v;
                }else{
                    $tempArr['content'][$k] = $v;
                }
                
            }
            
            $tempArr['content']['parent'] = $parent_id;
            $tempArr['content'] = array_merge($this->config['imp_content_default']['content'], $tempArr['content']);
            if(in_array('context_key',$this->table_fields)) $tempArr['content']['context_key'] = $this->config['context'];
            //alias
            if(!isset($tempArr['content']['alias']) && $this->config['imp_autoalias'] && isset($tempArr['content']['pagetitle'])){
                $tempArr['content']['alias'] = $this->makeAlias($tempArr['content']['pagetitle']);
            }
            $tempArr['tv'] = $this->config['imp_content_default']['tv'] + $tempArr['tv'];
            
            //Пропускаем через функцию-фильтр
            $insertArr = filter_import($tempArr);
            
            //print_r($insertArr); exit;
            
            if(is_array($insertArr) && !empty($insertArr['content']['pagetitle'])) $insert = $this->insertToDB($insertArr);
            
        }
        
        return true;
        
    }
    
    
    /**
     * Переводит XML объект в массив
     * 
     */
    public function xml2array($xmlObject, $out = array ()){
        foreach ((array) $xmlObject as $index => $node)
            $out[$index] = is_object($node) ? $this->xml2array($node) : $node;
            
        return $out;
    }
    
    /**
     * Переводит XML объект в массив, сохраняет значения и их xpath
     * 
     */
    public function parceXML($xmlObject, $out=array(), $level=0){
        
        //foreach($xmlObject->attributes() as $a => $b) {
        //    echo $a,'="',$b,"\"\n";
        //}
        
        foreach ((array) $xmlObject as $key => $node){
            
            //Атрибуты элемента
            if($key=='@attributes'){
                foreach($node as $k => $v) {
                    $this->keys[1][] = '@attributes.'.$k;
                    $this->values[] = trim($v);
                }
                continue;
            }
            
            if(is_object($node) || is_array($node)){
                
                //var_dump(count($this->keys[0]),$level,$key,implode('/',$this->keys[0]));
                //var_dump(implode('/',$this->keys[0])); echo "\n\n";
                
                //убираем с конца массива ключей значения, которые не соответствуют текущему уровню вложенности
                if(/*$level > 0 && */count($this->keys[0]) > $level){
                    $this->keys[0] = $this->array_mpop($this->keys[0],(count($this->keys[0]) - $level));
                }else{
                    $level = count($this->keys[0]);
                }
                if(!is_numeric($key)) $this->keys[0][] = trim($key);
                
                $out[$key] = $this->parceXML($node,$out,$level+1);
                
            }else{
                $this->keys[1][] = implode('/',array_merge($this->keys[0],array($key)));
                $this->values[] = trim($node);
                $out[$key] = trim($node);
            }
        }
        
        return $out;
    }
    
    
    /**
     * Убирает с конца массива любое число значений
     *
     *
     */
    public function array_mpop($array, $iterate=1){
        if(!is_array($array) && is_int($iterate))
            return false;
    
        while(($iterate--)!=false)
            array_pop($array);
            
        return $array;
    }


}


?>