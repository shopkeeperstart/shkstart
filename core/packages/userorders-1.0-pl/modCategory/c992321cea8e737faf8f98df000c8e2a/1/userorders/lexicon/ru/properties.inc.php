<?php

$_lang['prop_userorders.usergroup'] = 'Название группы покупателей';
$_lang['prop_userorders.statusCanceled'] = 'Номер статуса заказа "Отменен"';
$_lang['prop_userorders.noResults'] = 'Текст, который будет выводиться при отсутствии заказов';
$_lang['prop_userorders.totalVar'] = 'Имя плейсхолдера с общим количеством заказов пользователя';
$_lang['prop_userorders.limit'] = 'Число заказов на одной странице';
$_lang['prop_userorders.ordersListOuterTpl'] = 'Чанк списка заказов';
$_lang['prop_userorders.ordersListRowTpl'] = 'Чанк одного заказа при выводе списка';
$_lang['prop_userorders.orderOuterTpl'] = 'Чанк вывода подробностей заказа';
$_lang['prop_userorders.orderContactsTpl'] = 'Чанк строки контактной информации';
$_lang['prop_userorders.orderPurchaseRowTpl'] = 'Чанк одного товара при выводе списка товаров';
$_lang['prop_userorders.crumbTpl'] = 'Чанк для вывода хлебной крошки на странице подробностей заказа';

