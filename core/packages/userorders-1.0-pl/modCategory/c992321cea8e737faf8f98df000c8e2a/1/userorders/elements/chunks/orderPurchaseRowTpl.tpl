<tr>
    <td>
        <a href="[[+url]]">[[+name]]</a>
    </td>
    <td>
        [[+addit_data:default=`&mdash;`]]
    </td>
    <td>
        [[+count]]
    </td>
    <td>
        [[+price:num_format]]
    </td>
</tr>