<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'commercial',
    'readme' => '
Сниппет userOrders

http://modx-shopkeeper.ru
Andchir <andchir@gmail.com>

==============================================

Описание

Сниппет для вывода истории заказов покупателя для MODX Shopkeeper 3.x.
Выводит список заказов и страницу подробностей заказа. Покупатель может отредактировать
контактные данные и отменить заказ.

Возможности и особенности

- Вывод списка заказов.
- Вывод подробностей заказа со свписком товаров.
- Покупатель может отредактировать контактные данные заказа.
- Покупатель может отменить заказ.
- Совместим со сниппетом постраничной разбивки getPage.
- Для регистрации и авторизации нужно использовать сниппет Login.

Параметры сниппета

usergroup - Название группы покупателей. По умолчанию - "Покупатели"
statusCanceled - Номер статуса заказа "Отменен" (от еденицы). По умолчанию - 5.
noResults - Текст, который будет выводиться при отсутствии заказов. По умолчанию - "Вы пока ничего у нас не купили.".
totalVar - Имя плейсхолдера с общим количеством заказов пользователя. По умолчанию - total.
limit - Число заказов на одной странице. По умолчанию - 15.
ordersListOuterTpl - Чанк списка заказов (оберточный).
ordersListRowTpl - Чанк одного заказа при выводе списка.
orderOuterTpl - Чанк вывода подробностей заказа.
orderContactsTpl - Чанк строки контактной информации.
orderPurchaseRowTpl - Чанк одного товара при выводе списка товаров.
crumbTpl - Чанк для вывода хлебной крошки на странице подробностей заказа. Хлебная крошка выводится плейсхолдером [[!+addcrumb]].

Все примеры чанков прилагаются (категория "userOrders").

------------------------------------------------------

Плейсхолдеры в чанках

ordersListOuterTpl

[[+inner]] - Список заказов.

ordersListRowTpl

[[+id]] - ID заказа.
[[+price]] - Цена заказа.
[[+currency]] - Валюта.
[[+date]] - Дата заказа.
[[+note]] - Заметка.
[[+email]] - Адрес эл. почты покупателя.
[[+delivery]] - Способ доставки.
[[+delivery_price]] - Цена способа доставки.
[[+payment]] - Способ оплаты.
[[+status]] - Номер статуса заказа.
[[+count_total]] - Число уникальных товаров в заказе.
[[+status_name]] - Название статуса заказа.
[[+status_color]] - Цвет статуса заказа.

+ контактные данные с префиксом "contacts.". Пример: [[+contacts.address]]

orderOuterTpl

Все плейсхолдеры чанка "ordersListRowTpl".
[[+purchases]] - Список товаров в заказе.
[[+contacts]] - Список контактных данных покупателя.
[[+action]] - Текущее действие.

orderContactsTpl

[[+label]] - Заголовок поля контактной информации.
[[+name]] - Имя поля.
[[+value]] - Значение поля.
[[+action]] - Текущее действие.

orderPurchaseRowTpl

[[+name]] - Наименование товара.
[[+url]] - Ссылка на страницу товара.
[[+addit_data]] - Дополнительные параметры товара.
[[+count]] - Количество.
[[+price]] - Цена товара.

crumbTpl

[[+order_id]] - ID заказа.

------------------------------------------------------

Примеры вызова сниппета

Вывод заказов пользователя без постраничной разбивки:

[[!userOrders?
&usergroup=`Покупатели`
&statusCanceled=`5`
]]

Вывод заказов с постраничной разбивкой с помощью сниппета getPage:

[[!getPage?
&elementClass=`modSnippet`
&element=`userOrders`
&usergroup=`Покупатели`
&limit=`10`
&statusCanceled=`5`
]]
<ul class="pages">
[[!+page.nav]]
</ul>

------------------------------------------------------
',
    'changelog' => '
1.0

- Первый релиз.


',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '4aa2137334e908a09b477124caf5114c',
      'native_key' => 'userorders',
      'filename' => 'modNamespace/9e36440ea2663408961cf754dc9a67cd.vehicle',
      'namespace' => 'userorders',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '7be62a6fe7b8dc25cadfad0accbef695',
      'native_key' => 1,
      'filename' => 'modCategory/c992321cea8e737faf8f98df000c8e2a.vehicle',
      'namespace' => 'userorders',
    ),
  ),
);