<?php return array (
  'bb1516f7ceae827514d24c7a1540fb1f' => 
  array (
    'criteria' => 
    array (
      'name' => 'compare',
    ),
    'object' => 
    array (
      'name' => 'compare',
      'path' => '{core_path}components/compare/',
      'assets_path' => '',
    ),
  ),
  'f2f895f1a77e67c85f51d87a72276cb3' => 
  array (
    'criteria' => 
    array (
      'category' => 'Compare',
    ),
    'object' => 
    array (
      'id' => 8,
      'parent' => 0,
      'category' => 'Compare',
      'rank' => 0,
    ),
  ),
  '003328403be93aac4acb0638a4f590e0' => 
  array (
    'criteria' => 
    array (
      'name' => 'toCompare',
    ),
    'object' => 
    array (
      'id' => 33,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'toCompare',
      'description' => '',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<p>
    Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
    / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
    <span id="sravnenie_otmena" style="display:[[+display_cancel]];">
    /
    <a href="[[+href_cancel]]">отменить</a></span>
</p>
<br clear="all" />',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => '',
      'content' => '<p>
    Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
    / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
    <span id="sravnenie_otmena" style="display:[[+display_cancel]];">
    /
    <a href="[[+href_cancel]]">отменить</a></span>
</p>
<br clear="all" />',
    ),
  ),
  '8d8ae255a0bd771db2327dc0cfa57f62' => 
  array (
    'criteria' => 
    array (
      'name' => 'compare_product',
    ),
    'object' => 
    array (
      'id' => 34,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'compare_product',
      'description' => '',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<table class="data-table">
    
<!--tpl_separator-->

<tr class="[[+classes]]">[[+inner]]</tr>

<!--tpl_separator-->

<td class="odd first">Параметры</td>

<!--tpl_separator-->

<td class="[[+classes]]">
    <a class="del-link" href="[[~[[*id]]]]?cmpr_action=del_product&amp;pid=[[+id]]" title="Убрать">X</a>
    <br clear="all" />
    <img src="[[+image]]" alt="" />
    <br />
    <b>[[+pagetitle]]</b>
</td>

<!--tpl_separator-->

<td class="odd first">[[+param_name]]</td>

<!--tpl_separator-->

<td class="[[+classes]]">[[+param_value]]</td>

<!--tpl_separator-->

</table>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => '',
      'content' => '<table class="data-table">
    
<!--tpl_separator-->

<tr class="[[+classes]]">[[+inner]]</tr>

<!--tpl_separator-->

<td class="odd first">Параметры</td>

<!--tpl_separator-->

<td class="[[+classes]]">
    <a class="del-link" href="[[~[[*id]]]]?cmpr_action=del_product&amp;pid=[[+id]]" title="Убрать">X</a>
    <br clear="all" />
    <img src="[[+image]]" alt="" />
    <br />
    <b>[[+pagetitle]]</b>
</td>

<!--tpl_separator-->

<td class="odd first">[[+param_name]]</td>

<!--tpl_separator-->

<td class="[[+classes]]">[[+param_value]]</td>

<!--tpl_separator-->

</table>',
    ),
  ),
  '0e14811db0a89c6e2d926a1e201dfc6e' => 
  array (
    'criteria' => 
    array (
      'name' => 'compare',
    ),
    'object' => 
    array (
      'id' => 32,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'compare',
      'description' => 'Compare Products in the parameters.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '/*

compare 1.1
Сравнение товаров и Избранное

Andchir - http://modx-shopkeeper.ru/

*/

$default_toCompareTpl = \'
    <p>
        Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
        / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
        <span id="sravnenie_otmena" style="display:[[+display_cancel]];"> / <a href="[[+href_cancel]]">отменить</a></span>
    </p>
    <br clear="all" />
\';

$action = isset($action) ? $action : \'to_compare\';

require_once MODX_CORE_PATH . \'components/compare/compare.class.php\';
$compare = new prodCompare( $modx, $scriptProperties );

//действия, переданные по $_GET
$cmpr_action = isset($_GET[\'cmpr_action\']) && !is_array($_GET[\'cmpr_action\']) ? $_GET[\'cmpr_action\'] : \'\';
if($cmpr_action == \'del_product\' && !in_array( $action, array(\'print_products\', \'print_id_list\') ) ) return;
switch($cmpr_action){
    //удаление одного товара из списка для сравнения
    case \'del_product\':
        if(!empty($_GET[\'pid\'])) $compare->deleteCompareProduct();
    break;
    //очистка списка товаров, выбранных для сравнения
    case \'empty\':
        $compare->emptyCompare();
    break;
}

//действия для вывода в месте вызова сниппета
switch($action){
    //вывод строки со ссылками на страницу сравнения
    case \'to_compare\':
        $output = $compare->toCompareContent();
    break;
    //вывод списка ID товаров, выбранных для сравнения
    case \'print_id_list\':
        $output = $compare->printIDList();
    break;
    //вывод списка товаров, выбранных для сравнения
    case \'print_products\':
        $output = $compare->printCompareProducts();
    break;
    //вывод ID категории товаров, выбранных для стравнения
    case \'print_parent_id\':
        $output = isset($_COOKIE[\'shkCompareParent\']) ? $_COOKIE[\'shkCompareParent\'] : \'\';
    break;
    default:
        $output = \'\';
    break;
}

return $output;',
      'locked' => 0,
      'properties' => NULL,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/*

compare 1.1
Сравнение товаров и Избранное

Andchir - http://modx-shopkeeper.ru/

*/

$default_toCompareTpl = \'
    <p>
        Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
        / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
        <span id="sravnenie_otmena" style="display:[[+display_cancel]];"> / <a href="[[+href_cancel]]">отменить</a></span>
    </p>
    <br clear="all" />
\';

$action = isset($action) ? $action : \'to_compare\';

require_once MODX_CORE_PATH . \'components/compare/compare.class.php\';
$compare = new prodCompare( $modx, $scriptProperties );

//действия, переданные по $_GET
$cmpr_action = isset($_GET[\'cmpr_action\']) && !is_array($_GET[\'cmpr_action\']) ? $_GET[\'cmpr_action\'] : \'\';
if($cmpr_action == \'del_product\' && !in_array( $action, array(\'print_products\', \'print_id_list\') ) ) return;
switch($cmpr_action){
    //удаление одного товара из списка для сравнения
    case \'del_product\':
        if(!empty($_GET[\'pid\'])) $compare->deleteCompareProduct();
    break;
    //очистка списка товаров, выбранных для сравнения
    case \'empty\':
        $compare->emptyCompare();
    break;
}

//действия для вывода в месте вызова сниппета
switch($action){
    //вывод строки со ссылками на страницу сравнения
    case \'to_compare\':
        $output = $compare->toCompareContent();
    break;
    //вывод списка ID товаров, выбранных для сравнения
    case \'print_id_list\':
        $output = $compare->printIDList();
    break;
    //вывод списка товаров, выбранных для сравнения
    case \'print_products\':
        $output = $compare->printCompareProducts();
    break;
    //вывод ID категории товаров, выбранных для стравнения
    case \'print_parent_id\':
        $output = isset($_COOKIE[\'shkCompareParent\']) ? $_COOKIE[\'shkCompareParent\'] : \'\';
    break;
    default:
        $output = \'\';
    break;
}

return $output;',
    ),
  ),
  '0af940bd6bc17efd885ebd306180c5bd' => 
  array (
    'criteria' => 
    array (
      'name' => 'in_compare',
    ),
    'object' => 
    array (
      'id' => 33,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'in_compare',
      'description' => 'Check if in compare list.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '/*

Проверка добавлен ли товар к сравнению

[[+id:in_compare=`checked="checked"`]]

*/

if( !isset( $options ) ) $options = \'checked\';
$opt_arr = explode(\'||\', $options);
if( count( $opt_arr ) < 2 ) $opt_arr[1] = \' \';

$compareIds_arr = !empty( $_COOKIE[\'shkCompareIds\'] )
    ? explode( \',\', str_replace(\' \', \'\', $_COOKIE[\'shkCompareIds\'] ) )
    : array();

return in_array( $input, $compareIds_arr ) ? $opt_arr[0] : $opt_arr[1];',
      'locked' => 0,
      'properties' => NULL,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/*

Проверка добавлен ли товар к сравнению

[[+id:in_compare=`checked="checked"`]]

*/

if( !isset( $options ) ) $options = \'checked\';
$opt_arr = explode(\'||\', $options);
if( count( $opt_arr ) < 2 ) $opt_arr[1] = \' \';

$compareIds_arr = !empty( $_COOKIE[\'shkCompareIds\'] )
    ? explode( \',\', str_replace(\' \', \'\', $_COOKIE[\'shkCompareIds\'] ) )
    : array();

return in_array( $input, $compareIds_arr ) ? $opt_arr[0] : $opt_arr[1];',
    ),
  ),
);