<?php return array (
  'bb0e74a1a9dc77d45406ec113c5eea2a' => 
  array (
    'criteria' => 
    array (
      'name' => 'getproducts',
    ),
    'object' => 
    array (
      'name' => 'getproducts',
      'path' => '{core_path}components/getproducts/',
      'assets_path' => '',
    ),
  ),
  '477b5f23cb505edc3b73c25d95affb00' => 
  array (
    'criteria' => 
    array (
      'category' => 'getProducts',
    ),
    'object' => 
    array (
      'id' => 27,
      'parent' => 0,
      'category' => 'getProducts',
      'rank' => 0,
    ),
  ),
  '375897c6d9a9d1d47c8903694af7068e' => 
  array (
    'criteria' => 
    array (
      'name' => 'getProducts',
    ),
    'object' => 
    array (
      'id' => 89,
      'source' => 1,
      'property_preprocess' => 1,
      'name' => 'getProducts',
      'description' => '',
      'editor_type' => 0,
      'category' => 27,
      'cache_type' => 0,
      'snippet' => '/**
 * getProducts 1.3.8
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Andchir
 * @copyright Copyright 2013 http://modx-shopkeeper.ru/
/

/*

[[!getPage?
&elementClass=`modSnippet`
&element=`getProducts`
&parents=`10`
&limit=`20`
&tvFilters=`{"param":"value"}`
&includeTVs=`1`
&includeTVList=`price,image,width,inventory`
&tpl=`product`
&pageFirstTpl=`<li class="control"><a [[+classes]] href="[[+href]]">Первая</a></li>`
&pageLastTpl=`<li class="control"><a [[+classes]] href="[[+href]]">Последняя</a></li>`
]]
<br class="clear" />
<ul class="pages">
[[!+page.nav]]
</ul>
[[+total]]

*/

//ini_set(\'display_errors\',1);
//error_reporting(E_ALL);

$cached = array();

//Настройки кэширования
if(!empty($scriptProperties[\'gp_cache\'])){
    
    if(empty($scriptProperties[\'cache_key\'])) $scriptProperties[\'cache_key\'] = $modx->getOption(\'cache_resource_key\', null, \'resource\');
    if(empty($scriptProperties[\'cache_checkURL\'])) $scriptProperties[\'cache_checkURL\'] = false;//Не рекомендуется менять значение
    if(empty($scriptProperties[\'cacheId\'])) $scriptProperties[\'cacheId\'] = \'gpCache\';
    if(empty($scriptProperties[\'cache_handler\'])) $scriptProperties[\'cache_handler\'] = $modx->getOption(\'cache_resource_handler\', null, \'xPDOFileCache\');
    if(empty($scriptProperties[\'cache_expires\'])) $scriptProperties[\'cache_expires\'] = 0;
    
    if($scriptProperties[\'cache_checkURL\']){
        $scriptProperties[\'cachePageKey\'] = $modx->resource->getCacheKey() . \'/\' . $scriptProperties[\'cacheId\'] . md5(http_build_query($modx->request->getParameters()));
    }else{
        $scriptProperties[\'cachePageKey\'] = $scriptProperties[\'cacheId\'];
    }
    $scriptProperties[\'cacheOptions\'] = array(
        xPDO::OPT_CACHE_KEY => $scriptProperties[\'cache_key\'],
        xPDO::OPT_CACHE_HANDLER => $scriptProperties[\'cache_handler\'],
        xPDO::OPT_CACHE_EXPIRES => $scriptProperties[\'cache_expires\'],
    );
    
    $cached = $modx->cacheManager->get($scriptProperties[\'cachePageKey\'], $scriptProperties[\'cacheOptions\']);
    
    //Если есть в кэше, выводим его содержимое
    if(!empty($cached) && isset($cached[\'placeholders\']) && isset($cached[\'output\'])){
        
        $output = $cached[\'output\'];
        $modx->setPlaceholders($cached[\'placeholders\']);
        
        if($toPlaceholder){
            $modx->setPlaceholder($toPlaceholder,$output);
            $output = \'\';
        }
        
        return $output;
        
    }else{
        
        $cached = array();
        
    }
    
}

$output = \'\';

$checkPlaceholders = $modx->placeholders;

require_once MODX_CORE_PATH.\'components/getproducts/model/getproducts.class.php\';
$getProducts = new getProducts($modx,$scriptProperties);

$noResults = $modx->getOption(\'noResults\',$scriptProperties,\'\');
$toPlaceholder = $modx->getOption(\'toPlaceholder\',$scriptProperties,\'\');
$returnIDs = $modx->getOption(\'returnIDs\',$scriptProperties,false);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$debug = $modx->getOption(\'debug\', $scriptProperties, false);

$parents_data = array();

//Поиск товаров по заданным параметрам
$getProducts->ids_arr = array();

$getProducts->getProductIds();

//Фильтрация
$getProducts->filterProducts();

$total = count($getProducts->ids_arr);
$modx->setPlaceholder($totalVar,$total);
if($total==0) return $noResults;
if($returnIDs) return implode(\',\',$getProducts->ids_arr);

//Вытаскиваем все данные товаров
$getProducts->products = array();
$getProducts->getProductsData();

//Собираем TV
$getProducts->appendTVs();

//Вытаскиваем данные от родителей, если нужно
$getProducts->appendFromParents();

//Создаём HTML код по шаблону
$output .= $getProducts->getHTMLOutput();

//Кэшируем
if(!empty($scriptProperties[\'gp_cache\'])){
    
    $cached = array(
        \'output\' => $output,
        \'placeholders\' => array_diff_assoc($modx->placeholders, $checkPlaceholders)
    );
    unset($checkPlaceholders);
    
    $modx->cacheManager->set($scriptProperties[\'cachePageKey\'], $cached, $scriptProperties[\'cache_expires\'], $scriptProperties[\'cacheOptions\']);
    
}

if($toPlaceholder){
    $modx->setPlaceholder($toPlaceholder,$output);
    $output = \'\';
}

return $output;',
      'locked' => 0,
      'properties' => 'a:28:{s:11:"activeClass";a:7:{s:4:"name";s:11:"activeClass";s:4:"desc";s:65:"Имя CSS-класса для активного ресурса.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:19:"activeParentSnippet";a:7:{s:4:"name";s:19:"activeParentSnippet";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:76:"Сниппет для активного контейнера-ресурса";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"addSubItemCount";a:7:{s:4:"name";s:15:"addSubItemCount";s:4:"desc";s:147:"В чанке сниппета будет доступен плейсхолдер [[+subitemcount]] - число дочерних ресурсов.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"className";a:7:{s:4:"name";s:9:"className";s:4:"desc";s:76:"Имя класса (объекта) элементов таблицы БД.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:11:"modResource";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:151:"Режим отладки (1|0). В журнал ошибок будут писаться SQL запросы, полученные в сниппете.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"depth";a:7:{s:4:"name";s:5:"depth";s:4:"desc";s:74:"Глубина поиска родителей. По умолчанию 1.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:14:"fromParentList";a:7:{s:4:"name";s:14:"fromParentList";s:4:"desc";s:133:"список полей через запятую, которые нужно добавить товарам от родителей.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:14:"includeContent";a:7:{s:4:"name";s:14:"includeContent";s:4:"desc";s:87:"Включать в выборку из БД значение поля "content" (1|0).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"includeTVList";a:7:{s:4:"name";s:13:"includeTVList";s:4:"desc";s:95:"Список имён TV, которые нужно добавить через запятую.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"includeTVs";a:7:{s:4:"name";s:10:"includeTVs";s:4:"desc";s:125:"Добавить плейсхолдеры значений TV для ресурсов (1|0). Префикс для TV: "tv.".";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"noResults";a:7:{s:4:"name";s:9:"noResults";s:4:"desc";s:122:"Текст, который будет выводиться, если по запросу ничего не найдено.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"orderby";a:7:{s:4:"name";s:7:"orderby";s:4:"desc";s:39:"JSON строка сортировки.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"packageName";a:7:{s:4:"name";s:11:"packageName";s:4:"desc";s:59:"Имя пакета элементов таблицы БД.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"processTVList";a:7:{s:4:"name";s:13:"processTVList";s:4:"desc";s:95:"Список TV через запятую, для которых применять processTVs.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"processTVs";a:7:{s:4:"name";s:10:"processTVs";s:4:"desc";s:78:"Применять параметр "Параметры ввода" для TV.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"resources";a:7:{s:4:"name";s:9:"resources";s:4:"desc";s:76:"Список ID ресурсов (товаров) через запятую.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"returnIDs";a:7:{s:4:"name";s:9:"returnIDs";s:4:"desc";s:62:"Возвращать только ID рессурсов (1|0).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"sortby";a:7:{s:4:"name";s:6:"sortby";s:4:"desc";s:89:"Поле для сортировки (только поля ресурсов, без TV).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:9:"menuindex";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"sortbyTV";a:7:{s:4:"name";s:8:"sortbyTV";s:4:"desc";s:55:"Сортировка по TV. Указать имя TV.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"sortbyTVType";a:7:{s:4:"name";s:12:"sortbyTVType";s:4:"desc";s:44:"Тип значения TV (string|integer).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:6:"string";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"sortdir";a:7:{s:4:"name";s:7:"sortdir";s:4:"desc";s:44:"Направление сортировки.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:3:"ASC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"sortdirTV";a:7:{s:4:"name";s:9:"sortdirTV";s:4:"desc";s:63:"Направление сортировки по TV (ASC|DESC).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:3:"ASC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:185:"Имя плейсхолдера, в который нужно отправить результат работы сниппета. По умолчанию не используется.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"totalVar";a:7:{s:4:"name";s:8:"totalVar";s:4:"desc";s:86:"Имя плейсхолдера с общим количеством ресурсов.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:5:"total";s:7:"lexicon";N;s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:68:"Имя чанка шаблона для вывода ресурса.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"tvFilters";a:7:{s:4:"name";s:9:"tvFilters";s:4:"desc";s:49:"JSON строка фильтрации по ТВ.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"useSmarty";a:7:{s:4:"name";s:9:"useSmarty";s:4:"desc";s:77:"Использовать в чанке шаблонизатор Smarty (1|0).";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:68:"JSON строка для условия WHERE в SQL запросе.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}}',
      'moduleguid' => '',
      'static' => 1,
      'static_file' => 'core/components/getproducts/snippet.getproducts.php',
      'content' => '/**
 * getProducts 1.3.8
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Andchir
 * @copyright Copyright 2013 http://modx-shopkeeper.ru/
/

/*

[[!getPage?
&elementClass=`modSnippet`
&element=`getProducts`
&parents=`10`
&limit=`20`
&tvFilters=`{"param":"value"}`
&includeTVs=`1`
&includeTVList=`price,image,width,inventory`
&tpl=`product`
&pageFirstTpl=`<li class="control"><a [[+classes]] href="[[+href]]">Первая</a></li>`
&pageLastTpl=`<li class="control"><a [[+classes]] href="[[+href]]">Последняя</a></li>`
]]
<br class="clear" />
<ul class="pages">
[[!+page.nav]]
</ul>
[[+total]]

*/

//ini_set(\'display_errors\',1);
//error_reporting(E_ALL);

$cached = array();

//Настройки кэширования
if(!empty($scriptProperties[\'gp_cache\'])){
    
    if(empty($scriptProperties[\'cache_key\'])) $scriptProperties[\'cache_key\'] = $modx->getOption(\'cache_resource_key\', null, \'resource\');
    if(empty($scriptProperties[\'cache_checkURL\'])) $scriptProperties[\'cache_checkURL\'] = false;//Не рекомендуется менять значение
    if(empty($scriptProperties[\'cacheId\'])) $scriptProperties[\'cacheId\'] = \'gpCache\';
    if(empty($scriptProperties[\'cache_handler\'])) $scriptProperties[\'cache_handler\'] = $modx->getOption(\'cache_resource_handler\', null, \'xPDOFileCache\');
    if(empty($scriptProperties[\'cache_expires\'])) $scriptProperties[\'cache_expires\'] = 0;
    
    if($scriptProperties[\'cache_checkURL\']){
        $scriptProperties[\'cachePageKey\'] = $modx->resource->getCacheKey() . \'/\' . $scriptProperties[\'cacheId\'] . md5(http_build_query($modx->request->getParameters()));
    }else{
        $scriptProperties[\'cachePageKey\'] = $scriptProperties[\'cacheId\'];
    }
    $scriptProperties[\'cacheOptions\'] = array(
        xPDO::OPT_CACHE_KEY => $scriptProperties[\'cache_key\'],
        xPDO::OPT_CACHE_HANDLER => $scriptProperties[\'cache_handler\'],
        xPDO::OPT_CACHE_EXPIRES => $scriptProperties[\'cache_expires\'],
    );
    
    $cached = $modx->cacheManager->get($scriptProperties[\'cachePageKey\'], $scriptProperties[\'cacheOptions\']);
    
    //Если есть в кэше, выводим его содержимое
    if(!empty($cached) && isset($cached[\'placeholders\']) && isset($cached[\'output\'])){
        
        $output = $cached[\'output\'];
        $modx->setPlaceholders($cached[\'placeholders\']);
        
        if($toPlaceholder){
            $modx->setPlaceholder($toPlaceholder,$output);
            $output = \'\';
        }
        
        return $output;
        
    }else{
        
        $cached = array();
        
    }
    
}

$output = \'\';

$checkPlaceholders = $modx->placeholders;

require_once MODX_CORE_PATH.\'components/getproducts/model/getproducts.class.php\';
$getProducts = new getProducts($modx,$scriptProperties);

$noResults = $modx->getOption(\'noResults\',$scriptProperties,\'\');
$toPlaceholder = $modx->getOption(\'toPlaceholder\',$scriptProperties,\'\');
$returnIDs = $modx->getOption(\'returnIDs\',$scriptProperties,false);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$debug = $modx->getOption(\'debug\', $scriptProperties, false);

$parents_data = array();

//Поиск товаров по заданным параметрам
$getProducts->ids_arr = array();

$getProducts->getProductIds();

//Фильтрация
$getProducts->filterProducts();

$total = count($getProducts->ids_arr);
$modx->setPlaceholder($totalVar,$total);
if($total==0) return $noResults;
if($returnIDs) return implode(\',\',$getProducts->ids_arr);

//Вытаскиваем все данные товаров
$getProducts->products = array();
$getProducts->getProductsData();

//Собираем TV
$getProducts->appendTVs();

//Вытаскиваем данные от родителей, если нужно
$getProducts->appendFromParents();

//Создаём HTML код по шаблону
$output .= $getProducts->getHTMLOutput();

//Кэшируем
if(!empty($scriptProperties[\'gp_cache\'])){
    
    $cached = array(
        \'output\' => $output,
        \'placeholders\' => array_diff_assoc($modx->placeholders, $checkPlaceholders)
    );
    unset($checkPlaceholders);
    
    $modx->cacheManager->set($scriptProperties[\'cachePageKey\'], $cached, $scriptProperties[\'cache_expires\'], $scriptProperties[\'cacheOptions\']);
    
}

if($toPlaceholder){
    $modx->setPlaceholder($toPlaceholder,$output);
    $output = \'\';
}

return $output;',
    ),
  ),
);