<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'readme' => '﻿QapTcha Snippets v2.5
 Оригинал статьи
Home Page QapTcha - http://www.myjqueryplugins.com/QapTcha
Online Demo QapTcha - http://www.myjqueryplugins.com/QapTcha/demo
 Исходя из вышеуказанной статьи, мы адаптировали под MODx Revo
Обсуждения на форуме modx-cms.ru - http://community.modx-cms.ru/blog/addons/2152.html
Ставим вызов капчи перед формой: 
 Простой вызов — 
[[!Qaptcha]]

 Полный вызов — 
[[!Qaptcha? &disabledSubmit=`false` &txtLock=`Заблокирована: форма не может быть отправлена` &txtUnlock=`Разблокирована: форма может быть отправлена`]]


Параметры:

 disabledSubmit — по умолчанию `true`, 

 txtLock — по умолчанию `Locked: form can\'t be submited`,

 txtUnlock — по умолчанию `Unlocked: form can be submited`

 Ставим плейсхолдер 
[[+btQaptcha]] 
перед кнопкой submit.

В вызове формы, ставим &preHooks=`validQaptcha`
Мs проверяем Валидность поля iQapTcha на пустоту ( изночально, когда ползунок не передвинут, в input[name:iQapTcha] value равно с генерированному случайному паролю. Когда ползунок мы передвинули, то value равно "").
И проверяем сесию iQaptcha которую при помощи ползунка мы открыли.

Пользуемся

Пример кода

[[!Qaptcha? &disabledSubmit=`false` &txtLock=`Заблокирована: форма не может быть отправлена` &txtUnlock=`Разблокирована: форма может быть отправлена`]]
[[!Register?
    &preHooks=`validQaptcha`
    &submitVar=`registerbtn`     
    &activationResourceId=`4`
    &activationEmailTpl=`myActivationEmailTpl`
    &activationEmailSubject=`Thanks for Registering!`
    &submittedResourceId=`4`
    &usergroups=`Marketing,Research`
    &validate=`nospam:blank,
  username:required:minLength=^6^,
  password:required:minLength=^6^,
  password_confirm:password_confirm=^password^,
  fullname:required,
  email:required:email`
    &placeholderPrefix=`reg.`
]]
<div class="register">
    <div class="registerMessage">[[!+reg.error.message]]</div>
 
    <form class="form" action="[[~[[*id]]]]" method="post">
        <input type="hidden" name="nospam" value="[[!+reg.nospam]]" />
 
        <label for="username">[[%register.username? &namespace=`login` &topic=`register`]]
            <span class="error">[[!+reg.error.username]]</span>
        </label>
        <input type="text" name="username" id="username" value="[[!+reg.username]]" />
 
        <label for="password">[[%register.password]]
            <span class="error">[[!+reg.error.password]]</span>
        </label>
        <input type="password" name="password" id="password" value="[[!+reg.password]]" />
 
        <label for="password_confirm">[[%register.password_confirm]]
            <span class="error">[[!+reg.error.password_confirm]]</span>
        </label>
        <input type="password" name="password_confirm" id="password_confirm" value="[[!+reg.password_confirm]]" />
 
        <label for="fullname">[[%register.fullname]]
            <span class="error">[[!+reg.error.fullname]]</span>
        </label>
        <input type="text" name="fullname" id="fullname" value="[[!+reg.fullname]]" />
 
        <label for="email">[[%register.email]]
            <span class="error">[[!+reg.error.email]]</span>
        </label>
        <input type="text" name="email" id="email" value="[[!+reg.email]]" />
 
        <br class="clear" />
 
        <div class="form-buttons">
[[+btQaptcha]]
            <input type="submit" name="registerbtn" value="Register" />
        </div>
    </form>',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'ad0a128d55bf52b840d5aee9c5789c90',
      'native_key' => 'qaptcha',
      'filename' => 'modNamespace/47b2afe2ac2f96e3b1bdd1f0e06c5612.vehicle',
      'namespace' => 'qaptcha',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '28bdbd0022557a85918fa10fe86f6562',
      'native_key' => 1,
      'filename' => 'modCategory/ac918f1d5e47273f4b65df5faa65c0b6.vehicle',
      'namespace' => 'qaptcha',
    ),
  ),
);