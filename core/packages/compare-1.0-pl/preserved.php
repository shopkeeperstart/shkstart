<?php return array (
  '96f9105d42aab42b56c5a787a8ae2309' => 
  array (
    'criteria' => 
    array (
      'name' => 'compare',
    ),
    'object' => 
    array (
      'name' => 'compare',
      'path' => '{core_path}components/compare/',
      'assets_path' => '',
    ),
  ),
  '22bbbe052b75b7ce9066fb98adf8e018' => 
  array (
    'criteria' => 
    array (
      'category' => 'compare',
    ),
    'object' => 
    array (
      'id' => 8,
      'parent' => 0,
      'category' => 'Compare',
      'rank' => 0,
    ),
  ),
  'a3bb821b26312fd81d9ae1539e814b39' => 
  array (
    'criteria' => 
    array (
      'name' => 'compare',
    ),
    'object' => 
    array (
      'id' => 32,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'compare',
      'description' => 'Compare Products in the parameters.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '/*

compare 1.1
Сравнение товаров и Избранное

Andchir - http://modx-shopkeeper.ru/

*/

$default_toCompareTpl = \'
    <p>
        Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
        / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
        <span id="sravnenie_otmena" style="display:[[+display_cancel]];"> / <a href="[[+href_cancel]]">отменить</a></span>
    </p>
    <br clear="all" />
\';

$action = isset($action) ? $action : \'to_compare\';

require_once MODX_CORE_PATH . \'components/compare/compare.class.php\';
$compare = new prodCompare( $modx, $scriptProperties );

//действия, переданные по $_GET
$cmpr_action = isset($_GET[\'cmpr_action\']) && !is_array($_GET[\'cmpr_action\']) ? $_GET[\'cmpr_action\'] : \'\';
if($cmpr_action == \'del_product\' && !in_array( $action, array(\'print_products\', \'print_id_list\') ) ) return;
switch($cmpr_action){
    //удаление одного товара из списка для сравнения
    case \'del_product\':
        if(!empty($_GET[\'pid\'])) $compare->deleteCompareProduct();
    break;
    //очистка списка товаров, выбранных для сравнения
    case \'empty\':
        $compare->emptyCompare();
    break;
}

//действия для вывода в месте вызова сниппета
switch($action){
    //вывод строки со ссылками на страницу сравнения
    case \'to_compare\':
        $output = $compare->toCompareContent();
    break;
    //вывод списка ID товаров, выбранных для сравнения
    case \'print_id_list\':
        $output = $compare->printIDList();
    break;
    //вывод списка товаров, выбранных для сравнения
    case \'print_products\':
        $output = $compare->printCompareProducts();
    break;
    //вывод ID категории товаров, выбранных для стравнения
    case \'print_parent_id\':
        $output = isset($_COOKIE[\'shkCompareParent\']) ? $_COOKIE[\'shkCompareParent\'] : \'\';
    break;
    default:
        $output = \'\';
    break;
}

return $output;',
      'locked' => 0,
      'properties' => NULL,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/*

compare 1.1
Сравнение товаров и Избранное

Andchir - http://modx-shopkeeper.ru/

*/

$default_toCompareTpl = \'
    <p>
        Выбрано <span id="skolko_vibrano">[[+count_current]]</span> из [[+count_max]]
        / <a href="[[+href_compare]]" onclick="return shkCompare.toCompareLink();">сравнить</a>
        <span id="sravnenie_otmena" style="display:[[+display_cancel]];"> / <a href="[[+href_cancel]]">отменить</a></span>
    </p>
    <br clear="all" />
\';

$action = isset($action) ? $action : \'to_compare\';

require_once MODX_CORE_PATH . \'components/compare/compare.class.php\';
$compare = new prodCompare( $modx, $scriptProperties );

//действия, переданные по $_GET
$cmpr_action = isset($_GET[\'cmpr_action\']) && !is_array($_GET[\'cmpr_action\']) ? $_GET[\'cmpr_action\'] : \'\';
if($cmpr_action == \'del_product\' && !in_array( $action, array(\'print_products\', \'print_id_list\') ) ) return;
switch($cmpr_action){
    //удаление одного товара из списка для сравнения
    case \'del_product\':
        if(!empty($_GET[\'pid\'])) $compare->deleteCompareProduct();
    break;
    //очистка списка товаров, выбранных для сравнения
    case \'empty\':
        $compare->emptyCompare();
    break;
}

//действия для вывода в месте вызова сниппета
switch($action){
    //вывод строки со ссылками на страницу сравнения
    case \'to_compare\':
        $output = $compare->toCompareContent();
    break;
    //вывод списка ID товаров, выбранных для сравнения
    case \'print_id_list\':
        $output = $compare->printIDList();
    break;
    //вывод списка товаров, выбранных для сравнения
    case \'print_products\':
        $output = $compare->printCompareProducts();
    break;
    //вывод ID категории товаров, выбранных для стравнения
    case \'print_parent_id\':
        $output = isset($_COOKIE[\'shkCompareParent\']) ? $_COOKIE[\'shkCompareParent\'] : \'\';
    break;
    default:
        $output = \'\';
    break;
}

return $output;',
    ),
  ),
  '4cc7771298b7332e4f6d6fbbd896e3a0' => 
  array (
    'criteria' => 
    array (
      'name' => 'in_compare',
    ),
    'object' => 
    array (
      'id' => 33,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'in_compare',
      'description' => 'Check if in compare list.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '/*

Проверка добавлен ли товар к сравнению

[[+id:in_compare=`checked="checked"`]]

*/

if( !isset( $options ) ) $options = \'checked\';
$opt_arr = explode(\'||\', $options);
if( count( $opt_arr ) < 2 ) $opt_arr[1] = \' \';

$compareIds_arr = !empty( $_COOKIE[\'shkCompareIds\'] )
    ? explode( \',\', str_replace(\' \', \'\', $_COOKIE[\'shkCompareIds\'] ) )
    : array();

return in_array( $input, $compareIds_arr ) ? $opt_arr[0] : $opt_arr[1];',
      'locked' => 0,
      'properties' => NULL,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/*

Проверка добавлен ли товар к сравнению

[[+id:in_compare=`checked="checked"`]]

*/

if( !isset( $options ) ) $options = \'checked\';
$opt_arr = explode(\'||\', $options);
if( count( $opt_arr ) < 2 ) $opt_arr[1] = \' \';

$compareIds_arr = !empty( $_COOKIE[\'shkCompareIds\'] )
    ? explode( \',\', str_replace(\' \', \'\', $_COOKIE[\'shkCompareIds\'] ) )
    : array();

return in_array( $input, $compareIds_arr ) ? $opt_arr[0] : $opt_arr[1];',
    ),
  ),
);